The product comes in the form of a zip file named 'njoin-analyzer.tar.gz'

Place the file in the root directory. Use the following command to unzip and extract the content of the file:

	sudo tar -xvzf njoin-analyzer.tar.gz

This will produce a directory named 'njoin-analyzer'. The directory contains the following:
	
	1. README.txt : This file
	2. config/application.properties : Replace this file with your custom propertt file
	3. datadump/analytics_manager.sql : the data file for database creation
	4. libs/ : directory containing the jar file
	5. log/ : directory for logs
	6. source/ : directory for the source code (including java files, static files, and target jars)

To create the database schema:

	mysql -u<username> -p analytics_manager < /njoin-analyzer/datadump/analytics_manager.sql

To start the program:

	sudo bash -c "nohup java -Dapp.root=/njoin-analyzer -Dserver.port=<port> -jar /njoin-analyzer/libs/analyzer<version>.jar >> /njoin-analyzer/log/logs 2>&1 &"

To stop the program:
	
	ps aux | grep java | grep <port>
	kill -9 <pid>
