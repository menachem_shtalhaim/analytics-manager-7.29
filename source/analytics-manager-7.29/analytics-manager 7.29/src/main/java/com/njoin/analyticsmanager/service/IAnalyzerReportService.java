package com.njoin.analyticsmanager.service;

import java.util.List;

import com.njoin.analyticsmanager.entity.Report;
import com.njoin.analyticsmanager.entity.analyzer.Analyzer;

/**
 * A Service that manages the training reports of analyzers
 * 
 * @author Henry Xing
 * @version 7.29
 * 
 * @param <T> the type of the analyzer
 *
 */
public interface IAnalyzerReportService<T extends Analyzer> {

	/**
	 * Request to the back-end server and extract all training reports of the
	 * specified analyzer
	 * 
	 * @param analyzer
	 * @param jid
	 * @param prepareSU
	 *            whether the buffer should prepare the semantic unit responses
	 * @return the extracted list of reports
	 * @throws Throwable
	 */
	public List<Report> fetchReports(T analyzer, String jid, boolean prepareSU) throws Throwable;

}
