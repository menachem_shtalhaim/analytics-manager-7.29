package com.njoin.analyticsmanager.service.impl;

import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.njoin.analyticsmanager.entity.DBInput;
import com.njoin.analyticsmanager.entity.analyzer.Analyzer;
import com.njoin.analyticsmanager.entity.analyzer.KNNAnalyzer;
import com.njoin.analyticsmanager.exception.AnalyzerCorruptedException;
import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;

/**
 * An Implementation of Abstract Analyzer Service for the KNN analyzer.
 * 
 * Particularly, the creation of the analyzer involves only the KNN anomaly
 * model
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Service
@Transactional
public class KNNServiceImpl extends AbstractAnalyzerService<KNNAnalyzer> {

	private void analyzerSpecificPrepareRequest(Map<String, Object> request, KNNAnalyzer analyzer) {
		request.put("type", "KnnAnomalyModel");
		analyzer.setType(Analyzer.KNN_TYPE);
		analyzer.setDisplayType(displayDao.getDisplayInfoByType(Analyzer.KNN_TYPE).getDisplayType());
	}

	@Override
	public KNNAnalyzer createRequestFromFile(Map<String, Object> payload, String fileContent, String jid)
			throws BackEndServerException, SessionExpiredException {
		KNNAnalyzer analyzer = new KNNAnalyzer();
		populateAnalyzer(analyzer, payload);

		Map<String, Object> request = prepairModelRequest(analyzer, "");
		analyzerSpecificPrepareRequest(request, analyzer);
		populateFileRequest(request, fileContent);
		String result = requestService.postJSON(createModelURL, jid, request);
		int modelId = processCreateResult(result);
		int aeId = aeService.createAE(payload, modelId, "", jid);
		analyzer.setModelId(modelId);
		analyzer.setAeId(aeId);
		return analyzer;
	};

	@Override
	public KNNAnalyzer createRequestFromDB(Map<String, Object> payload, DBInput input, String jid)
			throws BackEndServerException, SessionExpiredException {
		KNNAnalyzer analyzer = new KNNAnalyzer();
		populateAnalyzer(analyzer, payload);

		Map<String, Object> request = prepairModelRequest(analyzer, "");
		analyzerSpecificPrepareRequest(request, analyzer);
		populateDBRequest(request, input);
		String result = requestService.postJSON(createModelURL, jid, request);
		int modelId = processCreateResult(result);
		int aeId = aeService.createAE(payload, modelId, "", jid);
		analyzer.setModelId(modelId);
		analyzer.setAeId(aeId);
		return analyzer;
	};

	@Override
	public String delete(int id, String jid) throws BackEndServerException, SessionExpiredException {
		KNNAnalyzer analyzer = retriveExist(id);
		requestOperation(analyzer.getAeId(), deleteAEURL, jid);
		analyzer.setArchived(true);
		analyzerDao.saveOrUpdate(analyzer);
		requestOperation(analyzer.getModelId(), deleteModelURL, jid);

		return "Analyzer removed";
	}

	@Override
	public String train(int id, Integer batch, String jid) throws BackEndServerException, SessionExpiredException {

		KNNAnalyzer analyzer = retriveExist(id);
		trainOperation(analyzer.getModelId(), batch, jid);

		return "Model submitted for training";
	}

	@Override
	public String stop(int id, String jid) throws BackEndServerException, SessionExpiredException {

		KNNAnalyzer analyzer = retriveExist(id);
		requestOperation(analyzer.getModelId(), stopURL, jid);

		return "Model train session abort request submitted";
	}

	@Override
	public String removeSchedule(int id, String jid) throws BackEndServerException, SessionExpiredException {
		KNNAnalyzer analyzer = retriveExist(id);
		String result = requestOperation(analyzer.getModelId(), removeScheduleURL, jid);
		return parseScheduleResponse(result);
	}

	@Override
	public String suspendSchedule(int id, String jid) throws BackEndServerException, SessionExpiredException {
		KNNAnalyzer analyzer = retriveExist(id);
		String result = requestOperation(analyzer.getModelId(), suspendScheduleURL, jid);
		return parseScheduleResponse(result);
	}

	@Override
	public String resumeSchedule(int id, String jid) throws BackEndServerException, SessionExpiredException {
		KNNAnalyzer analyzer = retriveExist(id);
		String result = requestOperation(analyzer.getModelId(), resumeScheduleURL, jid);
		return parseScheduleResponse(result);
	}

	@Override
	public String createSchedule(int id, String cron, String jid)
			throws BackEndServerException, SessionExpiredException {
		KNNAnalyzer analyzer = retriveExist(id);
		String result = scheduleOperation(analyzer.getModelId(), cron, jid);
		return parseScheduleCreateResponse(result);
	}

	@Override
	public String toggleActive(int id, String jid)
			throws BackEndServerException, SessionExpiredException, AnalyzerCorruptedException {
		KNNAnalyzer analyzer = retriveExist(id);
		return aeService.toggleActive(analyzer.getAeId(), analyzer.getId(), jid);
	}

	@Override
	public String toggleVisible(int id, String jid)
			throws BackEndServerException, SessionExpiredException, AnalyzerCorruptedException {
		KNNAnalyzer analyzer = retriveExist(id);
		return aeService.toggleVisible(analyzer.getAeId(), analyzer.getId(), jid);
	}
}