package com.njoin.analyticsmanager.buffer;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import com.njoin.analyticsmanager.service.IRequestService;

/**
 * An implementation of IStateListBuffer. The buffer input comes in batches and
 * the buffer removes the record after it has been deleted or requested
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Service
public class StateListBuffer extends AbstractOperationBuffer<MultiValueMap<String, String>, String>
		implements IStateListBuffer {

	@Autowired
	private IRequestService requestService;
	@Value("${server.state.detect.url}")
	private String detectStateURL;

	@Override
	public void prepare(Map<Integer, MultiValueMap<String, String>> records, String jid) {
		for (Map.Entry<Integer, MultiValueMap<String, String>> entry : records.entrySet()) {
			MultiValueMap<String, String> map = entry.getValue();

			Future<String> future = threadPool.submit(new StateListCallable(jid, map, detectStateURL, requestService));
			results.put(entry.getKey(), future);
		}
	}

	/**
	 * Removes the future upon retrieval
	 */
	@Override
	public String getResponse(int id) throws Throwable {
		try {
			String response = super.getResponse(id);
			results.remove(id);
			return response;
		} catch (Throwable e) {
			throw e;
		} finally {
			results.remove(id);
		}
	}

	@Override
	public String remove(int id) {
		Future<String> future = results.get(id);
		if (future == null) {
			throw new IllegalArgumentException("Request with the given id not found");
		}
		future.cancel(true);
		results.remove(id);
		return "Detect state request removed";
	}

	@Override
	public Map<Integer, Boolean> getStatus() {
		Map<Integer, Boolean> map = new HashMap<>();
		for (Map.Entry<Integer, Future<String>> entry : results.entrySet()) {
			Future<String> future = entry.getValue();
			map.put(entry.getKey(), future.isDone());
		}
		return map;
	}
}
