package com.njoin.analyticsmanager.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * A training task entity that represents a task to be executed and managed by
 * the Training Task Management Service. If the task is running, it must
 * maintain the id of the analyzer it created. If it is idle, it must maintain a
 * single analyzer create data to create the analyzer. After the task finishes
 * training, it will be deleted automatically
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Entity
@Table(name = "training_task")
public class TrainingTask {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	@Column(name = "analyzer_type")
	private String analyzerType;
	@Column(name = "analyzer_id")
	private Integer analyzerId;
	@Column(name = "report_id")
	private Integer reportId;
	// The current status of the training task
	// 0: idle
	// 1: running
	@Column(name = "status")
	private int status;
	// Whether the task is temporary, i.e., whether the created analyzer should
	// be deleted alongside the training task when finishes
	@Column(name = "temporary")
	private boolean temporary;
	@OneToOne(mappedBy = "trainingTask", cascade = CascadeType.ALL, fetch = FetchType.EAGER, optional = true, orphanRemoval = true)
	private AnalyzerCreateData analyzerCreateData;

	@Transient
	private String displayType;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAnalyzerType() {
		return analyzerType;
	}

	public void setAnalyzerType(String analyzerType) {
		this.analyzerType = analyzerType;
	}

	public Integer getAnalyzerId() {
		return analyzerId;
	}

	public void setAnalyzerId(Integer analyzerId) {
		this.analyzerId = analyzerId;
	}

	public Integer getReportId() {
		return reportId;
	}

	public void setReportId(Integer reportId) {
		this.reportId = reportId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public boolean isTemporary() {
		return temporary;
	}

	public void setTemporary(boolean temporary) {
		this.temporary = temporary;
	}

	public AnalyzerCreateData getAnalyzerCreateData() {
		return analyzerCreateData;
	}

	public void setAnalyzerCreateData(AnalyzerCreateData analyzerCreateData) {
		this.analyzerCreateData = analyzerCreateData;
	}

	public String getDisplayType() {
		return displayType;
	}

	public void setDisplayType(String displayType) {
		this.displayType = displayType;
	}
}