package com.njoin.analyticsmanager.dao;

import com.njoin.analyticsmanager.entity.TrainingTask;

/**
 * An extended generic Dao for training task. enables retrieval through status
 * 
 * @author Henry Xing
 * @version 7.29
 */
public interface ITrainingTaskDao extends IGenericDao<TrainingTask> {

	/**
	 * Get the first persistent training task by its status
	 * 
	 * @param status
	 *            the status of the training task
	 * @return the first managed entity from the persistent context if training
	 *         task with the status exists or null
	 */
	public TrainingTask getFirstTaskByStatus(int status);

}
