package com.njoin.analyticsmanager.service;

import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;

/**
 * A Service that fetches the status of the specified model
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
public interface IModelService {

	/**
	 * Get the status of the model identified by the modelId (not the analyzer)
	 * 
	 * @param modelId
	 * @param jid
	 * @return
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	public String getModelStatus(int modelId, String jid) throws BackEndServerException, SessionExpiredException;

}
