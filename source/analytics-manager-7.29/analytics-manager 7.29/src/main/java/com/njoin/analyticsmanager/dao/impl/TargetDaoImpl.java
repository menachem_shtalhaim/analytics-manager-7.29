package com.njoin.analyticsmanager.dao.impl;

import org.springframework.stereotype.Repository;

import com.njoin.analyticsmanager.dao.AbstractAnalyzerDao;
import com.njoin.analyticsmanager.entity.analyzer.TargetAnalyzer;

/**
 * A concrete implementation of analyzer dao for target analyzer
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Repository
public class TargetDaoImpl extends AbstractAnalyzerDao<TargetAnalyzer>{

	@Override
	protected Class<TargetAnalyzer> getClazz() {
		return TargetAnalyzer.class;
	}
}
