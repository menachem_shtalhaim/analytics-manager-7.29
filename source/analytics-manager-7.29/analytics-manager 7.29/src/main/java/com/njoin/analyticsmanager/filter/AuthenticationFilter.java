package com.njoin.analyticsmanager.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * A filter that intercepts client requests before login in, redirects the
 * client to the login page
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Component
@Order(1)
public class AuthenticationFilter implements Filter {

	@Override
	public void destroy() {
	}

	/**
	 * Redirects to the login page based on the following criteria:
	 * <p>
	 * <ul>
	 * <li>the request url starts with '/analytics'</li>
	 * <li>the request url is not the login url itself: '/analytics/login'</li>
	 * <li>the session cookie is not set</i>
	 * </ul>
	 * </p>
	 */
	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) req;
		HttpServletResponse httpResponse = (HttpServletResponse) res;
		String uri = httpRequest.getRequestURI();
		if (uri.startsWith("/analytics") && !uri.startsWith("/analytics/login")) {
			String jid = (String) httpRequest.getSession().getAttribute("jid");
			if (jid == null) {
				String newURI = httpRequest.getScheme() + "://" + httpRequest.getServerName() + ":"
						+ httpRequest.getServerPort() + httpRequest.getContextPath() + "/analytics/login/login.html";
				httpResponse.sendRedirect(newURI);
				return;
			}
		}
		chain.doFilter(req, res);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

}
