package com.njoin.analyticsmanager.buffer;

import java.util.concurrent.Callable;

import com.njoin.analyticsmanager.service.IRequestService;

/**
 * Callable for query of group list. Retrieves the response from the specified
 * url
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */

public class GroupCallable implements Callable<String> {

	private String jid;
	private String url;
	private IRequestService requestService;

	public GroupCallable(String jid, String url, IRequestService requestService) {
		super();
		this.jid = jid;
		this.url = url;
		this.requestService = requestService;
	}

	@Override
	public String call() throws Exception {
		String result = requestService.getJSON(url, jid, null);
		return result;
	}
}
