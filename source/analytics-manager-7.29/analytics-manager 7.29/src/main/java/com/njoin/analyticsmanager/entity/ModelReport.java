package com.njoin.analyticsmanager.entity;

import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An entity that represents a single model training report as a component of
 * the analyzer training report. A model report corresponds to one and only one
 * report.
 * 
 * The class also maintains the static constants of the name of the semantic
 * unit types
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Entity
@Table(name = "training_record_model")
@JsonIgnoreProperties(value = { "report" })
public class ModelReport {

	public static final String GROUP_TYPE = "group";
	public static final String REGISTER_TYPE = "register";

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	@Column(name = "end_time")
	private Long endTime;
	@Column(name = "data_start_time")
	private Long dataStartTime;
	@Column(name = "data_end_time")
	private Long dataEndTime;
	@Column(name = "error")
	private String error;
	@Basic
	private String type;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "report_id")
	private Report report;

	// The semantic unit number for the model report paired with the respective
	// semantic unit type name. One model report can have one or multiple
	// semantic units (i.e. duplicate analyzer)
	@Transient
	private Map<String, Integer> su;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public Long getDataStartTime() {
		return dataStartTime;
	}

	public void setDataStartTime(Long dataStartTime) {
		this.dataStartTime = dataStartTime;
	}

	public Long getDataEndTime() {
		return dataEndTime;
	}

	public void setDataEndTime(Long dataEndTime) {
		this.dataEndTime = dataEndTime;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Report getReport() {
		return report;
	}

	public void setReport(Report report) {
		this.report = report;
	}

	public Map<String, Integer> getSu() {
		return su;
	}

	public void setSu(Map<String, Integer> su) {
		this.su = su;
	}
}
