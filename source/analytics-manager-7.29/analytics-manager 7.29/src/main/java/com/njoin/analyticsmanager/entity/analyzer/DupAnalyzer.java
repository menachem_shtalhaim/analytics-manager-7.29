package com.njoin.analyticsmanager.entity.analyzer;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * A Duplicate Analyzer entity
 * 
 * Encompasses the following components, in the order of creation:
 * <p>
 * <ul>
 * <li>Duplicate Registers Anomaly Model</li>
 * <li>Duplicate Registers Group Induction Model</li>
 * <li>Analysis Engine for Duplicate Registers Anomaly Model</li>
 * </ul>
 * </p>
 * 
 * The analyzer allows a different set of (sampleRate, upperTime, trainingDays)
 * for the anomaly and group induction model
 * 
 * @author Henry Xing
 * @version 7.29
 */
@Entity
@DiscriminatorValue("D")
@Table(name = "dup_analyzer")
public class DupAnalyzer extends Analyzer {

	@Column(name = "model_id")
	private int modelId;
	@Column(name = "gi_model_id")
	private int giModelId;
	@Column(name = "ae_id")
	private int aeId;

	@Column(name = "sample_rate_anomaly")
	private int anomalySampleRate;
	@Column(name = "training_days_anomaly")
	private int anomalyTrainingDays;
	@Column(name = "upper_time_anomaly")
	private Long anomalyUpperTime;

	public int getModelId() {
		return modelId;
	}

	public void setModelId(int modelId) {
		this.modelId = modelId;
	}

	public int getGiModelId() {
		return giModelId;
	}

	public void setGiModelId(int giModelId) {
		this.giModelId = giModelId;
	}

	public int getAeId() {
		return aeId;
	}

	public void setAeId(int aeId) {
		this.aeId = aeId;
	}

	public int getAnomalySampleRate() {
		return anomalySampleRate;
	}

	public void setAnomalySampleRate(int anomalySampleRate) {
		this.anomalySampleRate = anomalySampleRate;
	}

	public int getAnomalyTrainingDays() {
		return anomalyTrainingDays;
	}

	public void setAnomalyTrainingDays(int anomalyTrainingDays) {
		this.anomalyTrainingDays = anomalyTrainingDays;
	}

	public Long getAnomalyUpperTime() {
		return anomalyUpperTime;
	}

	public void setAnomalyUpperTime(Long anomalyUpperTime) {
		this.anomalyUpperTime = anomalyUpperTime;
	}
}