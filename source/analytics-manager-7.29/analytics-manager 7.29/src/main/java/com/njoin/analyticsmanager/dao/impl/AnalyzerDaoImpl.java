package com.njoin.analyticsmanager.dao.impl;

import org.springframework.stereotype.Repository;

import com.njoin.analyticsmanager.dao.AbstractAnalyzerDao;
import com.njoin.analyticsmanager.entity.analyzer.Analyzer;

/**
 * A concrete implementation of analyzer dao for analyzer
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Repository
public class AnalyzerDaoImpl extends AbstractAnalyzerDao<Analyzer> {

	@Override
	protected Class<Analyzer> getClazz() {
		return Analyzer.class;
	}
}
