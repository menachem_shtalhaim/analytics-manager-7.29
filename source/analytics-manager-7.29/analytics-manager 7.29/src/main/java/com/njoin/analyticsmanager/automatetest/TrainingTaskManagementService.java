package com.njoin.analyticsmanager.automatetest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.njoin.analyticsmanager.dao.IAnalyzerDao;
import com.njoin.analyticsmanager.dao.ITrainingTaskDao;
import com.njoin.analyticsmanager.entity.AnalyzerCreateData;
import com.njoin.analyticsmanager.entity.Report;
import com.njoin.analyticsmanager.entity.TrainingTask;
import com.njoin.analyticsmanager.entity.analyzer.Analyzer;
import com.njoin.analyticsmanager.entity.analyzer.DupAnalyzer;
import com.njoin.analyticsmanager.entity.analyzer.KNNAnalyzer;
import com.njoin.analyticsmanager.entity.analyzer.MotifAnalyzer;
import com.njoin.analyticsmanager.entity.analyzer.TargetAnalyzer;
import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;
import com.njoin.analyticsmanager.service.IAnalyzerReportService;
import com.njoin.analyticsmanager.service.IAnalyzerService;
import com.njoin.analyticsmanager.service.ILoginService;
import com.njoin.analyticsmanager.service.IReportService;
import com.njoin.analyticsmanager.utils.SerialUtil;

/**
 * A scheduled service that periodically maintains the training tasks. Every
 * time the scheduled event is triggered, the service will update the status of
 * the tasks.
 * 
 * If it finds a task running, it will tract the task until it finishes. It
 * deletes the task (and the underlying analyzer if the task is marked as
 * temporary) after it finishes and extracts the training records.
 * 
 * If all tasks are idle, it will select the first one and start the training
 * process, updating the database.
 * 
 * At one time, there can only be at most one task running
 * 
 * The training batch size and the interval the service is run are both
 * configurable in the application.properties
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Service
@Transactional
public class TrainingTaskManagementService {

	@Value("${server.login.username}")
	private String username;
	@Value("${server.login.password}")
	private String password;
	@Value("${automate.train.batch}")
	private int batch;

	@Autowired
	private ITrainingTaskDao trainingTaskDao;
	@Autowired
	private IAnalyzerDao<KNNAnalyzer> knnDao;
	@Autowired
	private IAnalyzerDao<MotifAnalyzer> motifDao;
	@Autowired
	private IAnalyzerDao<DupAnalyzer> dupDao;
	@Autowired
	private IAnalyzerDao<TargetAnalyzer> targetDao;

	@Autowired
	private IAnalyzerService<KNNAnalyzer> knnService;
	@Autowired
	private IAnalyzerService<MotifAnalyzer> motifService;
	@Autowired
	private IAnalyzerService<DupAnalyzer> dupService;
	@Autowired
	private IAnalyzerService<TargetAnalyzer> targetService;
	@Autowired
	private IAnalyzerReportService<KNNAnalyzer> knnReportService;
	@Autowired
	private IAnalyzerReportService<MotifAnalyzer> motifReportService;
	@Autowired
	private IAnalyzerReportService<DupAnalyzer> dupReportService;
	@Autowired
	private IAnalyzerReportService<TargetAnalyzer> targetReportService;
	@Autowired
	private ILoginService loginService;
	@Autowired
	private IReportService reportService;

	/**
	 * A scheduled task triggered through the predefined interval. Manages the
	 * running task or the first idle task
	 * 
	 * @throws Throwable
	 */
	@Scheduled(fixedRateString = "${training.task.manage.interval}")
	public void manageTrainingTask() throws Throwable {
		// login before the management process
		String jid = loginService.login(username, password);
		// get first running task
		TrainingTask task = trainingTaskDao.getFirstTaskByStatus(1);
		if (task != null) {
			// there is a running task
			processRunningTask(task, jid);
		} else {
			// no task running
			// get first idle task
			task = trainingTaskDao.getFirstTaskByStatus(0);
			if (task != null) {
				// there is an idle task
				processIdleTask(task, jid);
			}
		}
	}

	/**
	 * Process the running task, if the task is finished, updates the training
	 * reports and delete the training task (and the underlying analyzer if the
	 * task is marked as temporary)
	 * 
	 * @param task
	 *            the running task, must be a managed instance in the
	 *            persistence context
	 * @param jid
	 * @throws Throwable
	 */
	private void processRunningTask(TrainingTask task, String jid) throws Throwable {
		// Fetch the reports of the underlying analyzer of the task
		List<Report> reports = new LinkedList<>();
		if (task.getAnalyzerType().equals(Analyzer.KNN_TYPE)) {
			KNNAnalyzer analyzer = knnDao.getById(task.getAnalyzerId());
			reports = knnReportService.fetchReports(analyzer, jid, false);
		} else if (task.getAnalyzerType().equals(Analyzer.MOTIF_TYPE)) {
			MotifAnalyzer analyzer = motifDao.getById(task.getAnalyzerId());
			reports = motifReportService.fetchReports(analyzer, jid, false);
		} else if (task.getAnalyzerType().equals(Analyzer.DUP_TYPE)) {
			DupAnalyzer analyzer = dupDao.getById(task.getAnalyzerId());
			reports = dupReportService.fetchReports(analyzer, jid, false);
		} else if (task.getAnalyzerType().equals(Analyzer.TARGET_TYPE)) {
			TargetAnalyzer analyzer = targetDao.getById(task.getAnalyzerId());
			reports = targetReportService.fetchReports(analyzer, jid, false);
		}

		if (!reports.isEmpty()) {
			// Reports have been generated
			// Get the last (latest) report
			Report report = reports.get(reports.size() - 1);
			task.setReportId(report.getId());
			String status = report.getStatus();
			if (!status.equals("RUNNING")) {
				// Analyzer is stopped
				// Archiving the task
				if (task.isTemporary()) {
					// updates the reports in the database and archives the
					// analyzers
					reportService.update(reports, false);
					if (task.getAnalyzerType().equals(Analyzer.KNN_TYPE)) {
						knnService.delete(task.getAnalyzerId(), jid);
					} else if (task.getAnalyzerType().equals(Analyzer.MOTIF_TYPE)) {
						motifService.delete(task.getAnalyzerId(), jid);
					} else if (task.getAnalyzerType().equals(Analyzer.DUP_TYPE)) {
						dupService.delete(task.getAnalyzerId(), jid);
					} else if (task.getAnalyzerType().equals(Analyzer.TARGET_TYPE)) {
						targetService.delete(task.getAnalyzerId(), jid);
					}
				}
				// delete the task after it finishes
				trainingTaskDao.delete(task);
			}

		}
	}

	/**
	 * Process the idle task, create the analyzer according to the create data
	 * of the task and starts training
	 * 
	 * @param task
	 *            the running task, must be a managed instance in the
	 *            persistence context
	 * @param jid
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	private void processIdleTask(TrainingTask task, String jid) throws BackEndServerException, SessionExpiredException {
		AnalyzerCreateData data = task.getAnalyzerCreateData();
		Map<String, Object> payload = prepairPayload(data);
		// create the analyzer according to the create data and starts training
		if (task.getAnalyzerType().equals(Analyzer.KNN_TYPE)) {
			task.setAnalyzerId(knnService.create(payload, jid).getId());
			knnService.train(task.getAnalyzerId(), batch, jid);
		} else if (task.getAnalyzerType().equals(Analyzer.MOTIF_TYPE)) {
			task.setAnalyzerId(motifService.create(payload, jid).getId());
			motifService.train(task.getAnalyzerId(), batch, jid);
		} else if (task.getAnalyzerType().equals(Analyzer.DUP_TYPE)) {
			task.setAnalyzerId(dupService.create(payload, jid).getId());
			dupService.train(task.getAnalyzerId(), batch, jid);
		} else if (task.getAnalyzerType().equals(Analyzer.TARGET_TYPE)) {
			task.setAnalyzerId(targetService.create(payload, jid).getId());
			targetService.train(task.getAnalyzerId(), batch, jid);
		}
		// set task status to training
		task.setStatus(1);
	}

	/**
	 * Prepare the analyzer creation request payload according to the create
	 * data.
	 * 
	 * Other than the fields specified in the payload, the following parameters
	 * are auto-generated:
	 * <p>
	 * <ul>
	 * <li>name: assigned a unique serial number</li>
	 * <li>description: uniform template</li>
	 * <li>dataSource: always 'db', training task cannot be created from file
	 * </li>
	 * <li>active: false</li>
	 * <li>visible: false</li>
	 * </ul>
	 * </p>
	 * 
	 * @param data
	 *            the create data associated with the training task
	 * @return
	 */
	private Map<String, Object> prepairPayload(AnalyzerCreateData data) {
		Map<String, Object> payload = new HashMap<>();
		payload.put("sampleRate", data.getSampleRate());
		payload.put("upperTime", data.getUpperTime());
		payload.put("trainingDays", data.getTrainingDays());
		payload.put("anomalySampleRate", data.getAnomalySampleRate());
		payload.put("anomalyUpperTime", data.getAnomalyUpperTime());
		payload.put("anomalyTrainingDays", data.getAnomalyTrainingDays());
		payload.put("targetContent", data.getTargetContent());
		payload.put("maxNumeric", data.getMaxNumeric());
		payload.put("maxNonNumeric", data.getMaxNonNumeric());
		payload.put("minNumericValue", data.getMinNumericValue());
		payload.put("minNonNumericValue", data.getMinNonNumericValue());
		payload.put("devices", data.getDevices());
		payload.put("name", "Training Task " + SerialUtil.getSerial());
		payload.put("description", "automatically generated analyzer for training task");
		payload.put("dataSource", "db");
		payload.put("active", false);
		payload.put("visible", false);
		return payload;
	}
}
