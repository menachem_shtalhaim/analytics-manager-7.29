package com.njoin.analyticsmanager.buffer;

/**
 * A buffer that stores group induction model id paired with their html paged
 * returned from the back-end server temporarily
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
public interface IGroupBuffer extends IGenericBuffer<Integer, String> {}
