package com.njoin.analyticsmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttribute;

import com.njoin.analyticsmanager.entity.Res;
import com.njoin.analyticsmanager.entity.analyzer.Analyzer;
import com.njoin.analyticsmanager.entity.analyzer.DupAnalyzer;
import com.njoin.analyticsmanager.entity.analyzer.KNNAnalyzer;
import com.njoin.analyticsmanager.entity.analyzer.MotifAnalyzer;
import com.njoin.analyticsmanager.entity.analyzer.TargetAnalyzer;
import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;
import com.njoin.analyticsmanager.service.IAnalyzerService;

/**
 * Rest Controller for the deletion of analyzers
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@RestController
public class DeleteController {

	@Autowired
	private IAnalyzerService<KNNAnalyzer> knnService;
	@Autowired
	private IAnalyzerService<MotifAnalyzer> motifService;
	@Autowired
	private IAnalyzerService<DupAnalyzer> dupService;
	@Autowired
	private IAnalyzerService<TargetAnalyzer> targetService;

	/**
	 * The request must specify the type of the analyzer and its id. The
	 * analyzer is archived rather than removed from the database
	 * 
	 * @param type
	 * @param id
	 * @param jid
	 * @return
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	@GetMapping("/analytics/delete")
	public Res delete(@RequestParam("type") String type, @RequestParam("id") int id,
			@SessionAttribute("jid") String jid) throws BackEndServerException, SessionExpiredException {
		if (type.equals(Analyzer.KNN_TYPE)) {
			return new Res(knnService.delete(id, jid));
		} else if (type.equals(Analyzer.MOTIF_TYPE)) {
			return new Res(motifService.delete(id, jid));
		} else if (type.equals(Analyzer.DUP_TYPE)) {
			return new Res(dupService.delete(id, jid));
		} else if (type.equals(Analyzer.TARGET_TYPE)) {
			return new Res(targetService.delete(id, jid));
		} else {
			throw new IllegalArgumentException("Analyzer type not recognized");
		}
	}
}
