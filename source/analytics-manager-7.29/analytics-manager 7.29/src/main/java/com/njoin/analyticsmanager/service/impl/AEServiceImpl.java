package com.njoin.analyticsmanager.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.njoin.analyticsmanager.entity.AnalysisEngine;
import com.njoin.analyticsmanager.exception.AnalyzerCorruptedException;
import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;
import com.njoin.analyticsmanager.service.IAEService;
import com.njoin.analyticsmanager.service.IRequestService;

/**
 * An Implementation of IAEService
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Service
@Transactional
public class AEServiceImpl implements IAEService {

	@Value("${server.create.ae.url}")
	private String createAEURL;
	@Value("${server.toggle.active.url}")
	private String toggleActiveURL;
	@Value("${server.toggle.visible.url}")
	private String toggleVisibleURL;
	@Value("${ae.import.su}")
	private boolean importSU;

	@Autowired
	private IRequestService requestService;

	@Override
	public AnalysisEngine extractAE(JSONObject aeObject) {
		AnalysisEngine ae = new AnalysisEngine();
		ae.setActive(aeObject.getString("active").equals("checked"));
		ae.setVisible(aeObject.getString("visible").equals("checked"));

		try {
			JSONObject subscriber = aeObject.getJSONObject("subscriber");
			try {
				ae.setReceiver(subscriber.getString("displayName"));
				ae.setSuppress(aeObject.getLongValue("ae_suppression_sec"));
				ae.setEmailAddress(subscriber.getString("emailAddress"));
				ae.setEmailSubject(subscriber.getString("messageTitle"));
				ae.setEmailBody(subscriber.getString("messageBody"));
			} catch (Exception e) {
				throw new IllegalArgumentException();
			}
		} catch (JSONException e) {
		}
		return ae;
	}

	@Override
	public int createAE(Map<String, Object> payload, int modelId, String prefix, String jid)
			throws BackEndServerException, SessionExpiredException {
		if (payload.get("name") == null || payload.get("description") == null || payload.get("active") == null) {
			throw new IllegalArgumentException(
					"Parameters 'name', 'description', and 'active' are required for this analyzer");
		}
		Map<String, Object> request = new HashMap<>();
		try {
			request.put("ae_name", "AE@" + prefix + (String) payload.get("name"));
			request.put("ae_description", "AE@" + prefix + (String) payload.get("description"));
			request.put("ae_active", (Boolean) payload.get("active"));
			request.put("ae_email", (String) payload.get("emailAddress"));
			request.put("ae_email_subject", (String) payload.get("emailSubject"));
			request.put("ae_email_body", (String) payload.get("emailBody"));
			request.put("ae_suppression_sec", (Integer) payload.get("suppress"));
			request.put("ae_visible", (Boolean) payload.get("visible"));
		} catch (ClassCastException e) {
			throw new IllegalArgumentException("Illegal input value for this analyzer");
		}
		request.put("ae_modelid", modelId);
		request.put("ae_import_su", importSU);

		String result = requestService.postJSON(createAEURL, jid, request);

		int aeId = processCreateResult(result);
		return aeId;
	}

	private int processCreateResult(String result) throws BackEndServerException {
		int aeId;
		try {
			JSONObject json = JSON.parseObject(result);
			aeId = json.getIntValue("ae_id");
		} catch (Exception e) {
			throw new BackEndServerException("Analyzer cannot be created");
		}
		return aeId;
	}

	@Override
	public String toggleActive(int id, int displayId, String jid)
			throws BackEndServerException, SessionExpiredException, AnalyzerCorruptedException {
		Map<String, Object> map = new HashMap<>();
		map.put("id", id);
		String result = requestService.getJSON(toggleActiveURL, jid, map);
		return parseToggleResult(result, "Analysis engine status changed", displayId);
	}

	@Override
	public String toggleVisible(int id, int displayId, String jid)
			throws BackEndServerException, SessionExpiredException, AnalyzerCorruptedException {
		Map<String, Object> map = new HashMap<>();
		map.put("id", id);
		String result = requestService.getJSON(toggleVisibleURL, jid, map);
		return parseToggleResult(result, "Analysis engine visibility changed", displayId);
	}

	private String parseToggleResult(String result, String expected, int displayId)
			throws BackEndServerException, AnalyzerCorruptedException {
		try {
			JSONObject json = JSONObject.parseObject(result);
			String msg = json.getString("message");
			if (msg.equals(expected)) {
				return "Analyzer status changed";
			}
		} catch (Exception e) {
			throw new BackEndServerException("Unable to read server response");
		}
		throw new AnalyzerCorruptedException(displayId, false);
	}

}
