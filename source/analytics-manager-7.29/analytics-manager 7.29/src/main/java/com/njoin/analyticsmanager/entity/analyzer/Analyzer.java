package com.njoin.analyticsmanager.entity.analyzer;

import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.njoin.analyticsmanager.entity.AnalysisEngine;
import com.njoin.analyticsmanager.entity.DBInput;

/**
 * The abstract super entity for all analyzers. Maintains the necessary and
 * common fields for all different types of analyzers. All analyzers must
 * extends from this class
 * 
 * All analyzer is stored in the 'analyzer' table. Different types of analyzers
 * are in their respective table according to the joined inheritance strategy
 * 
 * This class also contains static constants for the internal types of the
 * existing types of analyzers
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "analyzer_type")
@Table(name = "analyzer")
public abstract class Analyzer {

	public static final String KNN_TYPE = "knn";
	public static final String DUP_TYPE = "dup";
	public static final String MOTIF_TYPE = "motif";
	public static final String TARGET_TYPE = "target";

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	@Column(name = "name")
	private String name;
	@Column(name = "description")
	private String description;
	@Column(name = "type")
	private String type;
	@Column(name = "display_type")
	private String displayType;
	@Column(name = "sample_rate")
	private int sampleRate;
	@Column(name = "upper_time")
	private Long upperTime;
	@Column(name = "training_days")
	private int trainingDays;
	@Column(name = "archived")
	private boolean archived;
	@OneToOne(mappedBy = "analyzer", cascade = CascadeType.ALL, fetch = FetchType.EAGER, optional = true, orphanRemoval = true)
	private DBInput dbInput;

	@Transient
	private AnalysisEngine analysisEngine;
	@Transient
	private Boolean scheduleState;
	@Transient
	private String schedule;
	@Transient
	private Map<String, String> status;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDisplayType() {
		return displayType;
	}

	public void setDisplayType(String displayType) {
		this.displayType = displayType;
	}

	public int getSampleRate() {
		return sampleRate;
	}

	public void setSampleRate(int sampleRate) {
		this.sampleRate = sampleRate;
	}

	public Long getUpperTime() {
		return upperTime;
	}

	public void setUpperTime(Long upperTime) {
		this.upperTime = upperTime;
	}

	public int getTrainingDays() {
		return trainingDays;
	}

	public void setTrainingDays(int trainingDays) {
		this.trainingDays = trainingDays;
	}

	public DBInput getDbInput() {
		return dbInput;
	}

	public void setDbInput(DBInput dbInput) {
		this.dbInput = dbInput;
	}

	public Boolean getScheduleState() {
		return scheduleState;
	}

	public void setScheduleState(Boolean scheduleState) {
		this.scheduleState = scheduleState;
	}

	public String getSchedule() {
		return schedule;
	}

	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}

	public Map<String, String> getStatus() {
		return status;
	}

	public void setStatus(Map<String, String> status) {
		this.status = status;
	}

	public AnalysisEngine getAnalysisEngine() {
		return analysisEngine;
	}

	public void setAnalysisEngine(AnalysisEngine analysisEngine) {
		this.analysisEngine = analysisEngine;
	}

	public boolean isArchived() {
		return archived;
	}

	public void setArchived(boolean archived) {
		this.archived = archived;
	}
}
