package com.njoin.analyticsmanager.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.njoin.analyticsmanager.entity.Res;
import com.njoin.analyticsmanager.entity.Subscriber;
import com.njoin.analyticsmanager.service.ISubscriberService;

/**
 * Rest Controller for managing subscribers
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@RestController
public class SubscriberController {

	@Autowired
	private ISubscriberService subscriberService;

	/**
	 * Show all the current subscribers
	 * 
	 * @return
	 */
	@GetMapping("/analytics/subscriber/show")
	public List<Subscriber> getSubscribers() {
		return subscriberService.getSubscribers();
	}

	/**
	 * Save or update the subscriber
	 * 
	 * @param subscriber
	 * @return
	 */
	@PostMapping("/analytics/subscriber/save")
	public Res saveSubscriber(@RequestBody Subscriber subscriber) {
		return new Res(subscriberService.saveSubscriber(subscriber));
	}

	/**
	 * Delete the specified subscriber
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping("/analytics/subscriber/delete")
	public Res deleteSubscriber(@RequestParam(name = "id") int id) {
		return new Res(subscriberService.deleteSubscriber(id));
	}

}
