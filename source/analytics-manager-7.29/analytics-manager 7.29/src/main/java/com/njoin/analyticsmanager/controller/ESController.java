package com.njoin.analyticsmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttribute;

import com.njoin.analyticsmanager.entity.Res;
import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;
import com.njoin.analyticsmanager.service.IESService;

/**
 * Rest Controller for the management of elastic search repositories. The class
 * supports the same set of actions as the corresponding back-end server API
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@RestController
public class ESController {

	@Autowired
	private IESService esService;

	/**
	 * Show the repositories
	 * 
	 * @param jid
	 * @return
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	@GetMapping("/analytics/es/show")
	public String showES(@SessionAttribute("jid") String jid) throws BackEndServerException, SessionExpiredException {
		return esService.showES(jid);
	}

	/**
	 * Clear the specified repository identified by name
	 * 
	 * @param name
	 * @param jid
	 * @return
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	@GetMapping("/analytics/es/clear")
	public Res clear(@RequestParam("name") String name, @SessionAttribute("jid") String jid)
			throws BackEndServerException, SessionExpiredException {
		return new Res(esService.clear(name, jid));
	}

	/**
	 * Clear and insert all to the specified repository identified by name
	 * 
	 * @param name
	 * @param jid
	 * @return
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	@GetMapping("/analytics/es/clear_insert")
	public Res clearInsert(@RequestParam("name") String name, @SessionAttribute("jid") String jid)
			throws BackEndServerException, SessionExpiredException {
		return new Res(esService.clearInsert(name, jid));
	}

	/**
	 * insert or update all to the specified repository identified by name
	 * 
	 * @param name
	 * @param jid
	 * @return
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	@GetMapping("/analytics/es/insert_update")
	public Res insertUpdate(@RequestParam("name") String name, @SessionAttribute("jid") String jid)
			throws BackEndServerException, SessionExpiredException {
		return new Res(esService.insertUpdate(name, jid));
	}

}
