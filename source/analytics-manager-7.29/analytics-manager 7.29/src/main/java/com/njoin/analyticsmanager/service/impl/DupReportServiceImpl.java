package com.njoin.analyticsmanager.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.njoin.analyticsmanager.entity.Report;
import com.njoin.analyticsmanager.entity.analyzer.DupAnalyzer;

/**
 * An Implementation of Abstract Report Service for the Duplicate analyzer.
 * Particularly, all the training information is maintained in the primary
 * component which contains both register and group induction semantic units
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Service
@Transactional
public class DupReportServiceImpl extends AbstractReportService<DupAnalyzer> {

	@Override
	public List<Report> fetchReports(DupAnalyzer analyzer, String jid, boolean prepareSU) throws Throwable {
		String url = trainReportURL + analyzer.getGiModelId();
		String result = requestService.getJSON(url, jid, null);
		return extractAllReports(analyzer, analyzer.getGiModelId(), result, "Analysis Model", true,
				analyzer.getGiModelId(), jid, prepareSU);
	}
}
