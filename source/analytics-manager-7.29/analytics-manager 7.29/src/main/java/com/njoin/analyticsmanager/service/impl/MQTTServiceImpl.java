package com.njoin.analyticsmanager.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;
import com.njoin.analyticsmanager.service.IMQTTService;
import com.njoin.analyticsmanager.service.IRequestService;

/**
 * An Implementation of IMQTTService
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Service
@Transactional
public class MQTTServiceImpl implements IMQTTService {

	@Autowired
	private IRequestService requestService;
	@Value("${server.mqtt.show.url}")
	private String showMQTTURL;
	@Value("${server.mqtt.create.url}")
	private String createMQTTURL;
	@Value("${mqtt.add.operation}")
	private String add;

	@Override
	public String showMQTT(String jid) throws BackEndServerException, SessionExpiredException {
		return requestService.getJSON(showMQTTURL, jid, null);
	}

	@Override
	public String createMQTT(String name, String address, String identifier, String jid)
			throws BackEndServerException, SessionExpiredException {
		if (name == null || name.length() == 0) {
			throw new IllegalArgumentException("Parameter 'name' cannot be null or empty");
		}
		if (address == null || address.length() == 0) {
			throw new IllegalArgumentException("Parameter 'address' cannot be null or empty");
		}
		if (identifier == null || identifier.length() == 0) {
			throw new IllegalArgumentException("Parameter 'identifier' cannot be null or empty");
		}
		MultiValueMap<String, String> request = new LinkedMultiValueMap<>();
		request.add("giveName", name);
		request.add("address", address);
		request.add("identifier", identifier);
		request.add("Add device", add);

		requestService.postForm(createMQTTURL, jid, request);
		return "MQTT device creation request submitted";
	}

}
