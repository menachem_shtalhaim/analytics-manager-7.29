package com.njoin.analyticsmanager.exception.handler;

import javax.servlet.http.HttpServletResponse;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * An error handler that redirects the user to appropriate error pages
 * 
 * Currently supporting error page for 404 not found and 50X server error, other
 * error will be mapped to the default error page. This avoids the spring
 * default white label error fallback
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Controller
public class AppErrorController implements ErrorController {

	private static final String PATH = "/error";

	/**
	 * Redirects the client to the appropriate error pages when ever an '/error'
	 * path appears
	 * 
	 * @param response
	 * @return
	 */
	@RequestMapping(value = PATH)
	public String error(HttpServletResponse response) {
		int status = response.getStatus();
		if (status == 404) {
			return "/analytics/404.html";
		} else if (status >= 500 && status < 600) {
			return "/analytics/50X.html";
		} else {
			return "/analytics/default_error.html";
		}
	}

	@Override
	public String getErrorPath() {
		return PATH;
	}
}
