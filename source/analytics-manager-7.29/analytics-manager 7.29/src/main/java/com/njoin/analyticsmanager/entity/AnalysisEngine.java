package com.njoin.analyticsmanager.entity;

/**
 * An Analysis Engine entity that maintains the analysis engine and notification
 * specifications. This is a transient object and is not stored in the database.
 * When used, it should be directly parsed from the back-end server
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
public class AnalysisEngine {

	private boolean active;
	private String receiver;
	private String emailAddress;
	private String emailSubject;
	private String emailBody;
	private boolean visible;
	private long suppress;
	private boolean importSU;

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getEmailSubject() {
		return emailSubject;
	}

	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	public String getEmailBody() {
		return emailBody;
	}

	public void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public long getSuppress() {
		return suppress;
	}

	public void setSuppress(long suppress) {
		this.suppress = suppress;
	}

	public boolean isImportSU() {
		return importSU;
	}

	public void setImportSU(boolean importSU) {
		this.importSU = importSU;
	}
}
