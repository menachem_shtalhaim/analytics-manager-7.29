package com.njoin.analyticsmanager.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.njoin.analyticsmanager.entity.DisplayInfo;
import com.njoin.analyticsmanager.service.IDisplayService;

/**
 * Rest Controller for showing the display information, including the internal
 * and external names and the restrictions on analyzer creation
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@RestController
public class DisplayController {

	@Autowired
	private IDisplayService displayService;

	/**
	 * Serves all the display informations
	 * 
	 * @return
	 */
	@GetMapping("/analytics/display")
	public List<DisplayInfo> displayInfos() {
		return displayService.getAllDisplayInfos();
	}

}
