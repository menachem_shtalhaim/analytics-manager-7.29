package com.njoin.analyticsmanager.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttribute;

import com.njoin.analyticsmanager.entity.Res;
import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;
import com.njoin.analyticsmanager.service.ISimulationService;

/**
 * Rest Controller for managing simulations
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@RestController
public class SimulationController {

	@Autowired
	private ISimulationService simulationService;

	/**
	 * Create a new simulation request, the following parameters are required:
	 * <p>
	 * <ul>
	 * <li>from: starting time of simulation</li>
	 * <li>to: ending time of simulation</li>
	 * <li>frequency: number of simulation events per second</li>
	 * </ul>
	 * </p>
	 * 
	 * @param payload
	 * @param jid
	 * @return
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	@PostMapping("/analytics/simulation/create")
	public Res createSimulation(@RequestBody Map<String, Object> payload, @SessionAttribute("jid") String jid)
			throws BackEndServerException, SessionExpiredException {
		return new Res(simulationService.createSimulation(payload, jid));
	}

	/**
	 * Show a map that includes the following entries:
	 * <p>
	 * <ul>
	 * <li>"cache*": motif cache statistics</li>
	 * <li>"queue*": event queue condition</li>
	 * <li>"running": running simulation tasks</li>
	 * <li>"waiting": waiting simulation tasks</li>
	 * <li>"analyzerStats": real time statistics of active analyzers</li>
	 * </ul>
	 * </p>
	 * 
	 * @param jid
	 * @return
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	@GetMapping("/analytics/simulation/show")
	public Map<String, Object> showSimulation(@SessionAttribute("jid") String jid)
			throws BackEndServerException, SessionExpiredException {
		return simulationService.showSimulation(jid);
	}

	/**
	 * Remove the running task
	 * 
	 * @param jid
	 * @return
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	@GetMapping("/analytics/simulation/remove_running")
	public Res removeRunning(@SessionAttribute("jid") String jid)
			throws BackEndServerException, SessionExpiredException {
		return new Res(simulationService.removeRunning(jid));
	}

	/**
	 * Remove all waiting tasks
	 * 
	 * @param jid
	 * @return
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	@GetMapping("/analytics/simulation/remove_waiting")
	public Res removeWaiting(@SessionAttribute("jid") String jid)
			throws BackEndServerException, SessionExpiredException {
		return new Res(simulationService.removeWaiting(jid));
	}

	/**
	 * Clear all the analyzer statistics
	 * 
	 * @param jid
	 * @return
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	@GetMapping("/analytics/simulation/remove_statistics")
	public Res removeStatistics(@SessionAttribute("jid") String jid)
			throws BackEndServerException, SessionExpiredException {
		return new Res(simulationService.removeStatistics(jid));
	}
}
