package com.njoin.analyticsmanager.entity;

import com.njoin.analyticsmanager.entity.analyzer.Analyzer;

/**
 * An Analyzer Stat entity that maintains the real time status of the analysis
 * engines of the corresponding analyzer. This is a transient object and is not
 * stored in the database. When used, it should be directly parsed from the
 * back-end server
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
public class AnalyzerStat {

	private Analyzer analyzer;
	private String type;
	private double lastTime;
	private double lastAvgTime;
	private double avgTime;
	private double maxTime;
	private int hits;
	private int anomalies;

	public Analyzer getAnalyzer() {
		return analyzer;
	}

	public void setAnalyzer(Analyzer analyzer) {
		this.analyzer = analyzer;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double getLastTime() {
		return lastTime;
	}

	public void setLastTime(double lastTime) {
		this.lastTime = lastTime;
	}

	public double getLastAvgTime() {
		return lastAvgTime;
	}

	public void setLastAvgTime(double lastAvgTime) {
		this.lastAvgTime = lastAvgTime;
	}

	public double getAvgTime() {
		return avgTime;
	}

	public void setAvgTime(double avgTime) {
		this.avgTime = avgTime;
	}

	public double getMaxTime() {
		return maxTime;
	}

	public void setMaxTime(double maxTime) {
		this.maxTime = maxTime;
	}

	public int getHits() {
		return hits;
	}

	public void setHits(int hits) {
		this.hits = hits;
	}

	public int getAnomalies() {
		return anomalies;
	}

	public void setAnomalies(int anomalies) {
		this.anomalies = anomalies;
	}
}
