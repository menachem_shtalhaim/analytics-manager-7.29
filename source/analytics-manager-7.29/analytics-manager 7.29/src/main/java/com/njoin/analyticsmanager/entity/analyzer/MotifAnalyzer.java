package com.njoin.analyticsmanager.entity.analyzer;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * A Motif Analyzer entity
 * 
 * Encompasses the following components, in the order of creation:
 * <p>
 * <ul>
 * <li>Dummy Motif Model</li>
 * <li>Motif Pattern Single Register Model</li> 
 * <li>Motif Pattern Pair Register Model</li>
 * <li>Motif Pattern Group Induction Model</li>
 * <li>Analysis Engine for Motif Pattern Single Register Model</li>
 * <li>Analysis Engine for Motif Pattern Pair Register Model</li>
 * </ul>
 * </p>
 * 
 * @author Henry Xing
 * @version 7.29
 */
@Entity
@DiscriminatorValue("M")
@Table(name="motif_analyzer")
public class MotifAnalyzer extends Analyzer{
	
	@Column(name="dummy_model_id")
	private int dummyModelId;
	@Column(name="gi_model_id")
	private int giModelId;
	@Column(name="single_model_id")
	private int singleModelId;
	@Column(name="pair_model_id")
	private int pairModelId;
	@Column(name="single_ae_id")
	private int singleAeId;
	@Column(name="pair_ae_id")
	private int pairAeId;
	
	public int getDummyModelId() {
		return dummyModelId;
	}
	public void setDummyModelId(int dummyModelId) {
		this.dummyModelId = dummyModelId;
	}
	public int getGiModelId() {
		return giModelId;
	}
	public void setGiModelId(int giModelId) {
		this.giModelId = giModelId;
	}
	public int getSingleModelId() {
		return singleModelId;
	}
	public void setSingleModelId(int singleModelId) {
		this.singleModelId = singleModelId;
	}
	public int getPairModelId() {
		return pairModelId;
	}
	public void setPairModelId(int pairModelId) {
		this.pairModelId = pairModelId;
	}
	public int getSingleAeId() {
		return singleAeId;
	}
	public void setSingleAeId(int singleAeId) {
		this.singleAeId = singleAeId;
	}
	public int getPairAeId() {
		return pairAeId;
	}
	public void setPairAeId(int pairAeId) {
		this.pairAeId = pairAeId;
	}
}