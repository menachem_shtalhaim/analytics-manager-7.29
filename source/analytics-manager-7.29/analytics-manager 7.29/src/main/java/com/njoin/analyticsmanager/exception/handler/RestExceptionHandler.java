package com.njoin.analyticsmanager.exception.handler;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.njoin.analyticsmanager.entity.Res;
import com.njoin.analyticsmanager.exception.AnalyzerCorruptedException;
import com.njoin.analyticsmanager.exception.BackEndServerException;

/**
 * An error handler that handles the exceptions as a REST API, provides a
 * uniform error json format through the Res entity
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@ControllerAdvice
@RestController
public class RestExceptionHandler {

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(value = IllegalArgumentException.class)
	public Res handleBadRequests(IllegalArgumentException e) {
		return new Res(e.getMessage());
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(value = NullPointerException.class)
	public Res handleBadRequests(NullPointerException e) {
		return new Res(e.getMessage());
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(value = BackEndServerException.class)
	public Res handleServerError(BackEndServerException e) {
		return new Res(e.getMessage());
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(value = AnalyzerCorruptedException.class)
	public Res handleServerError(AnalyzerCorruptedException e) {
		return new Res(e.getMessage());
	}
}
