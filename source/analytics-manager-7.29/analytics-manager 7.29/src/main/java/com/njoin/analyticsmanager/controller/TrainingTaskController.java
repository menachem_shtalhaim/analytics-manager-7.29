package com.njoin.analyticsmanager.controller;

import java.rmi.ServerException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttribute;

import com.njoin.analyticsmanager.entity.Res;
import com.njoin.analyticsmanager.entity.TrainingTask;
import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;
import com.njoin.analyticsmanager.service.ITrainingTaskService;

/**
 * Rest Controller for managing training tasks
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@RestController
public class TrainingTaskController {

	@Autowired
	private ITrainingTaskService trainingTaskService;

	/**
	 * Create a training task manually.
	 * 
	 * The following parameters are required
	 * <p>
	 * <ul>
	 * <li>type</li>
	 * <li>sampleRate</li>
	 * <li>upperTime</li>
	 * <li>trainingDays</li>
	 * <li>anomalySampleRate: for analyzers support 2 sets of params</li>
	 * <li>anomalyUpperTime: for analyzers support 2 sets of params</li>
	 * <li>anomalyTrainingDays: for analyzers support 2 sets of params</li>
	 * <li>maxNumeric</li>
	 * <li>maxNonNumeric</li>
	 * <li>minNumericValue</li>
	 * <li>minNonNumericValue</li>
	 * <li>targetContent: for 'target analyzer'</li>
	 * <li>temporary</li>
	 * </ul>
	 * </p>
	 * 
	 * The following parameters are optional:
	 * <p>
	 * <ul>
	 * <li>devices</li>
	 * </ul>
	 * </p>
	 * 
	 * @param payload
	 * @param jid
	 * @return
	 * @throws ServerException
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	@PostMapping("/analytics/task/create")
	public Res create(@RequestBody Map<String, Object> payload, @SessionAttribute("jid") String jid)
			throws ServerException, BackEndServerException, SessionExpiredException {
		return new Res(trainingTaskService.createTrainingTask(payload, null));
	}

	/**
	 * Delete the specified training task, the task cannot be deleted while
	 * running
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping("/analytics/task/delete")
	public Res delete(@RequestParam(name = "id") int id) {
		return new Res(trainingTaskService.deleteTrainingTask(id));
	}

	/**
	 * Show all the training tasks. If there is a task running, it is at the
	 * front of the list
	 * 
	 * @return
	 */
	@GetMapping("/analytics/task/show")
	public List<TrainingTask> show() {
		return trainingTaskService.showTrainingTasks();
	}

	/**
	 * Smart generate training tasks automatically
	 * 
	 * The following parameters are required
	 * <p>
	 * <ul>
	 * <li>type</li>
	 * <li>upperTime</li>
	 * <li>(for sampleRate)
	 * <ul>
	 * <li>sampleRateMin</li>
	 * <li>sampleRateInc</li>
	 * <li>sampleRateMax</li>
	 * </ul>
	 * </li>
	 * <li>(for trainingDays)
	 * <ul>
	 * <li>trainingDaysMin</li>
	 * <li>trainingDaysInc</li>
	 * <li>trainingDaysMax</li>
	 * </ul>
	 * </li>
	 * <li>(for maxNumeric)
	 * <ul>
	 * <li>numericMin</li>
	 * <li>numericInc</li>
	 * <li>numericMax</li>
	 * </ul>
	 * </li>
	 * <li>(for maxNonNumeric)
	 * <ul>
	 * <li>nonNumericMin</li>
	 * <li>nonNumericInc</li>
	 * <li>nonNumericMax</li>
	 * </ul>
	 * </li>
	 * <li>minNumeric</li>
	 * <li>minNonNumeric</li>
	 * <li>targetContent: for 'target analyzer'</li>
	 * <li>temporary</li>
	 * </ul>
	 * </p>
	 * 
	 * The following parameters are optional:
	 * <p>
	 * <ul>
	 * <li>devices</li>
	 * </ul>
	 * </p>
	 * 
	 * @param payload
	 * @param jid
	 * @return
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	@PostMapping("/analytics/task/smart")
	public Res smartGenerate(@RequestBody Map<String, Object> payload, @SessionAttribute("jid") String jid)
			throws BackEndServerException, SessionExpiredException {
		return new Res(trainingTaskService.smartGenerate(payload, jid));
	}
}
