package com.njoin.analyticsmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttribute;

import com.njoin.analyticsmanager.entity.Res;
import com.njoin.analyticsmanager.entity.analyzer.Analyzer;
import com.njoin.analyticsmanager.entity.analyzer.DupAnalyzer;
import com.njoin.analyticsmanager.entity.analyzer.KNNAnalyzer;
import com.njoin.analyticsmanager.entity.analyzer.MotifAnalyzer;
import com.njoin.analyticsmanager.entity.analyzer.TargetAnalyzer;
import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;
import com.njoin.analyticsmanager.service.IAnalyzerService;

/**
 * Rest Controller for managing the scheduling of analyzers. For all the
 * actions, the client must specify an analyzer identified by its type and id
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@RestController
public class ScheduleController {

	@Autowired
	private IAnalyzerService<KNNAnalyzer> knnService;
	@Autowired
	private IAnalyzerService<MotifAnalyzer> motifService;
	@Autowired
	private IAnalyzerService<DupAnalyzer> dupService;
	@Autowired
	private IAnalyzerService<TargetAnalyzer> targetService;

	/**
	 * Remove the scheduling of the analyzer. Does nothing if no scheduling
	 * exists
	 * 
	 * @param type
	 * @param id
	 * @param jid
	 * @return
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	@GetMapping("/analytics/schedule/remove")
	public Res removeSchedule(@RequestParam("type") String type, @RequestParam("id") int id,
			@SessionAttribute("jid") String jid) throws BackEndServerException, SessionExpiredException {
		if (type.equals(Analyzer.KNN_TYPE)) {
			return new Res(knnService.removeSchedule(id, jid));
		} else if (type.equals(Analyzer.MOTIF_TYPE)) {
			return new Res(motifService.removeSchedule(id, jid));
		} else if (type.equals(Analyzer.DUP_TYPE)) {
			return new Res(dupService.removeSchedule(id, jid));
		} else if (type.equals(Analyzer.TARGET_TYPE)) {
			return new Res(targetService.removeSchedule(id, jid));
		} else {
			throw new IllegalArgumentException("Analyzer type not recognized");
		}
	}

	/**
	 * Suspend the scheduling of the analyzer. Does nothing if no scheduling
	 * exists or it is already suspended
	 * 
	 * @param type
	 * @param id
	 * @param jid
	 * @return
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	@GetMapping("/analytics/schedule/suspend")
	public Res suspendSchedule(@RequestParam("type") String type, @RequestParam("id") int id,
			@SessionAttribute("jid") String jid) throws BackEndServerException, SessionExpiredException {
		if (type.equals(Analyzer.KNN_TYPE)) {
			return new Res(knnService.suspendSchedule(id, jid));
		} else if (type.equals(Analyzer.MOTIF_TYPE)) {
			return new Res(motifService.suspendSchedule(id, jid));
		} else if (type.equals(Analyzer.DUP_TYPE)) {
			return new Res(dupService.suspendSchedule(id, jid));
		} else if (type.equals(Analyzer.TARGET_TYPE)) {
			return new Res(targetService.suspendSchedule(id, jid));
		} else {
			throw new IllegalArgumentException("Analyzer type not recognized");
		}
	}

	/**
	 * Resumes the scheduling of the analyzer. Does nothing if no scheduling
	 * exists or it is not suspended
	 * 
	 * @param type
	 * @param id
	 * @param jid
	 * @return
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	@GetMapping("/analytics/schedule/resume")
	public Res resumeSchedule(@RequestParam("type") String type, @RequestParam("id") int id,
			@SessionAttribute("jid") String jid) throws BackEndServerException, SessionExpiredException {
		if (type.equals(Analyzer.KNN_TYPE)) {
			return new Res(knnService.resumeSchedule(id, jid));
		} else if (type.equals(Analyzer.MOTIF_TYPE)) {
			return new Res(motifService.resumeSchedule(id, jid));
		} else if (type.equals(Analyzer.DUP_TYPE)) {
			return new Res(dupService.resumeSchedule(id, jid));
		} else if (type.equals(Analyzer.TARGET_TYPE)) {
			return new Res(targetService.resumeSchedule(id, jid));
		} else {
			throw new IllegalArgumentException("Analyzer type not recognized");
		}
	}

	/**
	 * Create a scheduling for the analyzer. Does nothing if scheduling exists
	 * 
	 * @param type
	 * @param id
	 * @param cron
	 * @param jid
	 * @return
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	@GetMapping("/analytics/schedule/create")
	public Res createSchedule(@RequestParam("type") String type, @RequestParam("id") int id,
			@RequestParam("cron") String cron, @SessionAttribute("jid") String jid)
			throws BackEndServerException, SessionExpiredException {
		if (type.equals(Analyzer.KNN_TYPE)) {
			knnService.removeSchedule(id, jid);
			return new Res(knnService.createSchedule(id, cron, jid));
		} else if (type.equals(Analyzer.MOTIF_TYPE)) {
			motifService.removeSchedule(id, jid);
			return new Res(motifService.createSchedule(id, cron, jid));
		} else if (type.equals(Analyzer.DUP_TYPE)) {
			dupService.removeSchedule(id, jid);
			return new Res(dupService.createSchedule(id, cron, jid));
		} else if (type.equals(Analyzer.TARGET_TYPE)) {
			targetService.removeSchedule(id, jid);
			return new Res(targetService.createSchedule(id, cron, jid));
		} else {
			throw new IllegalArgumentException("Analyzer type not recognized");
		}
	}
}
