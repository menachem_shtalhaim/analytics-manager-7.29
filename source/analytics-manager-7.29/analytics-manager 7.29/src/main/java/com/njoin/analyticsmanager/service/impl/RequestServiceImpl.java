package com.njoin.analyticsmanager.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;
import com.njoin.analyticsmanager.service.IRequestService;

/**
 * An Implementation of RequestService
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Service
public class RequestServiceImpl implements IRequestService{
	
	@Value("${redirect.login.url}")
	private String loginRedirect;

	@Override
	public <T> String postJSON(String url, String jid, T payload) 
			throws BackEndServerException, SessionExpiredException {
		return postRequest(url, jid, payload, MediaType.APPLICATION_JSON);
	}
	
	@Override
	public String postForm(String url, String jid, MultiValueMap<String, String> payload) 
			throws BackEndServerException, SessionExpiredException {
		return postRequest(url, jid, payload, MediaType.APPLICATION_FORM_URLENCODED);
	}

	@Override
	public String getJSON(String url, String jid, Map<String, Object> payload) 
			throws BackEndServerException, SessionExpiredException {
		RestTemplate template = new RestTemplate();
		HttpEntity<Map<String, Object>> request = generateJSONGetRequest(jid);
		String newURL = url + generateURL(payload);
		ResponseEntity<String> result = template.exchange(newURL, HttpMethod.GET, request, String.class);
		
		if (result.getStatusCodeValue() != 200) {
			throw new BackEndServerException("Unable to access server");
		}
		
		if (result.getBody().contains("<title>Login Page</title>")) {
			throw new SessionExpiredException();
		}
		
		return result.getBody();
	}
	
	private <T> String postRequest(String url, String jid, T payload, MediaType mediaType) throws SessionExpiredException, BackEndServerException {
		RestTemplate template = new RestTemplate();
		HttpEntity<T> request = generatePostRequest(jid, payload, mediaType);
		ResponseEntity<String> result = template.exchange(url, HttpMethod.POST, request, String.class);
		
		if (result.getStatusCodeValue() == 302 && result.getBody().contains("Location=[" + loginRedirect + "]")) {
			throw new SessionExpiredException();
		}
		
		if (result.getStatusCodeValue() != 200) {
			throw new BackEndServerException("Unable to access server");
		}
		
		return result.getBody();
	}
	
	private <T> HttpEntity<T> generatePostRequest(String jid, T payload, MediaType mediaType) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(mediaType);;
		headers.add("Cookie", jid);
		HttpEntity<T> request = new HttpEntity<T>(payload, headers);
		return request;
	}
	
	private HttpEntity<Map<String, Object>> generateJSONGetRequest(String jid) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", jid);
		HttpEntity<Map<String, Object>> request = new HttpEntity<Map<String,Object>>(headers);
		return request;
	}
	
	private String generateURL(Map<String, Object> payload) {
		if (payload == null || payload.isEmpty()) {
			return "";
		}
		StringBuilder sb = new StringBuilder();
		sb.append('?');
		boolean pre = false;
		for (String key : payload.keySet()) {
			if (pre) {
				sb.append('&');
			}
			pre = true;
			sb.append(key);
			sb.append('=');
			sb.append(payload.get(key).toString());
		}
		return sb.toString();
	}
}
