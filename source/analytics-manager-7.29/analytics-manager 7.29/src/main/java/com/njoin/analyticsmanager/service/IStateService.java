package com.njoin.analyticsmanager.service;

import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;

/**
 * A Service that manages the state register information retrieval. This service
 * operates the state buffer when submitting state detection requests
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
public interface IStateService {

	/**
	 * Submit a register state detection request according to the specified
	 * information, the request is submitted to the state buffer and requested
	 * multi-threadedly to the back-end server, the fulfilled request can be
	 * retrieved from the state buffer afterwards
	 * 
	 * @param start
	 *            the starting time frame of the state detection
	 * @param end
	 *            the ending time frame of the state detection
	 * @param sampleRate
	 *            the sample rate in state detection
	 * @param max
	 *            the maximum number of registers considered in this request
	 * @param jid
	 * @return the detection submitted message
	 */
	public String detectState(long start, long end, int sampleRate, int max, String jid);

	/**
	 * Import a state register file from the filepath in the back-end server,
	 * this method directly wraps around the back-end server API
	 * 
	 * @param filePath
	 *            the path of the state registers file on the back-end server
	 * @param isNew
	 *            whether the state import mode is new, true for new state and
	 *            false for update state
	 * @param jid
	 * @return the back-end server message for state import
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	public String importState(String filePath, boolean isNew, String jid)
			throws BackEndServerException, SessionExpiredException;

}
