package com.njoin.analyticsmanager.entity;

import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * A single training session report for an analyzer. One training report should
 * be paired with one and only one analyzer. The report can also maintain
 * multiple separate model reports if the analyzer is composed of multiple
 * components that train separately
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Entity
@Table(name = "training_record")
public class Report {

	@Id
	@Column(name = "id")
	private int id;
	@Column(name = "status")
	private String status;
	@Column(name = "analyzerId")
	private int analyzerId;
	// The manage status indicates whether the report is processed by the
	// notification system and whether it should be ignored by it
	// 0 for unmanaged, the notification system should manage it
	// 1 for managed but not archived, the notification system should track its
	// status
	// 2 for managed and archived, the notification system should ignore it
	@Column(name = "manage_status")
	private int manageStatus;
	@Column(name = "start_time")
	private Long startTime;
	@Column(name = "found")
	private Integer registersFound;

	@OneToMany(mappedBy = "report", cascade = CascadeType.ALL, orphanRemoval = true)
	@MapKey(name = "type")
	private Map<String, ModelReport> modelReports;

	@Transient
	private String schedule;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getAnalyzerId() {
		return analyzerId;
	}

	public void setAnalyzerId(int analyzerId) {
		this.analyzerId = analyzerId;
	}

	public int getManageStatus() {
		return manageStatus;
	}

	public void setManageStatus(int manageStatus) {
		this.manageStatus = manageStatus;
	}

	public String getSchedule() {
		return schedule;
	}

	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Integer getRegistersFound() {
		return registersFound;
	}

	public void setRegistersFound(Integer registersFound) {
		this.registersFound = registersFound;
	}

	public Map<String, ModelReport> getModelReports() {
		return modelReports;
	}

	public void setModelReports(Map<String, ModelReport> modelReports) {
		this.modelReports = modelReports;
	}
}