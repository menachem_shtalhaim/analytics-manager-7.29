package com.njoin.analyticsmanager.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.alibaba.fastjson.JSON;
import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;
import com.njoin.analyticsmanager.service.IFeedService;
import com.njoin.analyticsmanager.service.IRequestService;

/**
 * An Implementation of IFeedService
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Service
@Transactional
public class FeedServiceImpl implements IFeedService{

	@Autowired
	IRequestService requestService;
	
	@Value("${server.feed.url}")
	private String feedURL;
	
	@Override
	public String feed(String registerId, String value, String jid) throws BackEndServerException, SessionExpiredException {
		if (registerId == null || value == null) {
			throw new IllegalArgumentException("Parameters 'registerId' and 'value' cannot be null");
		}
		try {
			Double.parseDouble(value);
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Parameter 'value' must be a valid double value");
		}
		try {
			Long.parseLong(registerId);
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Parameter 'registerId' must be a valid long value");
		}
		MultiValueMap<String, String> request = new LinkedMultiValueMap<>();
		request.add("register_id", registerId);
		request.add("register_value", value);
		String result = requestService.postForm(feedURL, jid, request);
		try {
			return JSON.parseObject(result).getString("message");
		} catch (Exception e) {
			throw new BackEndServerException("Unable to read server response");
		}
	}
}
