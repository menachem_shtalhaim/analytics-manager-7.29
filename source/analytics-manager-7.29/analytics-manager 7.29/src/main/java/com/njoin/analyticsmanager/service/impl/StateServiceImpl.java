package com.njoin.analyticsmanager.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.alibaba.fastjson.JSONObject;
import com.njoin.analyticsmanager.buffer.IStateListBuffer;
import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;
import com.njoin.analyticsmanager.service.IRequestService;
import com.njoin.analyticsmanager.service.IStateService;
import com.njoin.analyticsmanager.utils.SerialUtil;

/**
 * An Implementation of IStateService
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Service
@Transactional
public class StateServiceImpl implements IStateService {

	@Autowired
	private IStateListBuffer stateListBuffer;
	@Autowired
	private IRequestService requestService;

	@Value("${server.state.import.url}")
	private String importStateURL;

	@Override
	public String detectState(long start, long end, int sampleRate, int max, String jid) {
		MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
		map.add("from", start + "");
		map.add("to", end + "");
		map.add("groupLevel", sampleRate + "");
		map.add("maxRegisters", max + "");
		Map<Integer, MultiValueMap<String, String>> input = new HashMap<>();
		input.put(SerialUtil.getSerial(), map);
		stateListBuffer.prepare(input, jid);
		return "Detect state request submitted";
	}

	@Override
	public String importState(String filePath, boolean isNew, String jid)
			throws BackEndServerException, SessionExpiredException {
		if (filePath == null || filePath.length() == 0) {
			throw new IllegalArgumentException("Parameter 'filePath' cannot be null or empty");
		}
		MultiValueMap<String, String> request = new LinkedMultiValueMap<>();
		request.add("filePath", filePath);
		if (isNew) {
			request.add("update_mode", "new");
		} else {
			request.add("update_mode", "update");
		}
		String result = requestService.postForm(importStateURL, jid, request);
		try {
			JSONObject json = JSONObject.parseObject(result);
			if (json.containsKey("status")) {
				if (json.getIntValue("status") < 0) {
					throw new IllegalArgumentException(json.getString("message"));
				}
			}
			return json.getString("message");
		} catch (IllegalArgumentException e) {
			throw e;
		} catch (Exception e) {
			throw new BackEndServerException("Unable to read server response");
		}
	}
}
