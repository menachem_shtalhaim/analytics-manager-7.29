package com.njoin.analyticsmanager.exception;

/**
 * An Exception indicating an exception in the back-end server, usually thrown
 * when the back-end server is unreachable for the message is unreadable
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@SuppressWarnings("serial")
public class BackEndServerException extends Exception {
	private String message;

	public BackEndServerException(String message) {
		this.message = message;
	}

	@Override
	public String getMessage() {
		return message;
	}
}
