package com.njoin.analyticsmanager.entity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.njoin.analyticsmanager.entity.analyzer.Analyzer;

/**
 * The entity maintains a map of lists of analyzers paired with their respective
 * analyzer type. This is a transient object and is not stored in the database.
 * It is used to wrap the response of showing the analyzers.
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
public class Records {

	private Map<String, List<? extends Analyzer>> records;

	public Records() {
		records = new HashMap<>();
	}

	public void putRecord(String type, List<? extends Analyzer> analyzers) {
		records.put(type, analyzers);
	}

	public Map<String, List<? extends Analyzer>> getRecords() {
		return records;
	}
}
