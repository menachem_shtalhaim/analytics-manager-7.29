package com.njoin.analyticsmanager.service;

import java.util.List;

import com.njoin.analyticsmanager.entity.DisplayInfo;

/**
 * A Service that retrieves Display Info
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
public interface IDisplayService {
	/**
	 * Retrieves all the display infos
	 * 
	 * @return all the existing display infos
	 */
	public List<DisplayInfo> getAllDisplayInfos();

}
