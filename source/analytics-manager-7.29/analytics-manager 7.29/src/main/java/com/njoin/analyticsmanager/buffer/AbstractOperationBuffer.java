package com.njoin.analyticsmanager.buffer;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.njoin.analyticsmanager.exception.BackEndServerException;

/**
 * An abstract implementation of IGenericBuffer. Provides the common feature of
 * a buffer that handles operation multi-threadedly and blocks the retrieval
 * until the operation finishes
 * 
 * @author Henry Xing
 * @version 7.29
 *
 * @param <I>
 * @param <O>
 */
public abstract class AbstractOperationBuffer<I, O> implements IGenericBuffer<I, O> {

	protected ExecutorService threadPool = Executors.newCachedThreadPool();
	protected ConcurrentHashMap<Integer, Future<O>> results = new ConcurrentHashMap<>();

	@Override
	public O getResponse(int id) throws Throwable {
		Future<O> future = results.get(id);
		try {
			return future.get();
		} catch (InterruptedException e) {
			throw new BackEndServerException("An error in processing, please refresh the page and try again");
		} catch (ExecutionException e) {
			throw e.getCause();
		}
	}

	@Override
	public void clear() {
		for (Iterator<Map.Entry<Integer, Future<O>>> iterator = results.entrySet().iterator(); iterator.hasNext();) {
			Future<O> future = iterator.next().getValue();
			future.cancel(true);
			iterator.remove();
		}
	}
}
