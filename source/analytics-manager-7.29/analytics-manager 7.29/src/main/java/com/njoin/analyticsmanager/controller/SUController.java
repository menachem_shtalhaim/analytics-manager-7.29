package com.njoin.analyticsmanager.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.njoin.analyticsmanager.buffer.IGroupBuffer;
import com.njoin.analyticsmanager.buffer.IRegisterBuffer;
import com.njoin.analyticsmanager.entity.ModelReport;

/**
 * Rest Controller for retrieving semantic unit displays
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@RestController
public class SUController {

	@Autowired
	private IGroupBuffer groupBuffer;
	@Autowired
	private IRegisterBuffer registerBuffer;

	/**
	 * Retrieves the specified semantic unit information of the corresponding
	 * record. suType can either be 'group' or 'registers'
	 * 
	 * @param response
	 * @param suType
	 * @param recordId
	 * @throws Throwable
	 */
	@GetMapping("/analytics/su")
	public void getSU(HttpServletResponse response, @RequestParam("suType") String suType,
			@RequestParam("recordId") int recordId) throws Throwable {
		if (suType == null) {
			throw new IllegalArgumentException("Must specify a 'suType' to show semantic units");
		}
		if (suType.equals(ModelReport.GROUP_TYPE)) {
			// open the page fetched from back-end for the groups
			// the page is stored in the group buffer
			response.setContentType("text/html;charset=UTF-8");
			response.getWriter().print(groupBuffer.getResponse(recordId));
		} else if (suType.equals(ModelReport.REGISTER_TYPE)) {
			// download the csv file formatted from the back-end server of the
			// register statistics
			// the csv is stored in the register buffer
			response.setContentType("text/csv;charset=UTF-8");
			response.setHeader("Content-disposition", "attachment;filename=registers.csv");
			response.getOutputStream().print(registerBuffer.getResponse(recordId));
		} else {
			throw new IllegalArgumentException("suType not recognized");
		}
	}

}
