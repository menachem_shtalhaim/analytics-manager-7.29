package com.njoin.analyticsmanager.configuration;

import java.util.List;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Configuration for the blocked method-url map of readonly mode
 * 
 * The information is loading from the application.properties file with the
 * prefix 'mode.readonly.conf.blocked'
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@ConfigurationProperties(prefix = "mode.readonly.conf")
public class BlockedReadonly {

	private Map<String, List<String>> blocked;

	public Map<String, List<String>> getBlocked() {
		return blocked;
	}

	public void setBlocked(Map<String, List<String>> blocked) {
		this.blocked = blocked;
	}
}
