package com.njoin.analyticsmanager.buffer;

import java.util.Map;

import org.springframework.util.MultiValueMap;

/**
 * A buffer that prepares the response for the register state detection request,
 * stores the response from the server.
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
public interface IStateListBuffer extends IGenericBuffer<MultiValueMap<String, String>, String> {

	/**
	 * Remove the specified state detection request
	 * 
	 * @param id
	 *            the id of the request
	 * @return the removal success message
	 */
	public String remove(int id);

	/**
	 * Retrieve the status of the requests
	 * 
	 * @return the status of the requests paired with their id
	 */
	public Map<Integer, Boolean> getStatus();

}
