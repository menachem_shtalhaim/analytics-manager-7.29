package com.njoin.analyticsmanager.dao.impl;

import org.springframework.stereotype.Repository;

import com.njoin.analyticsmanager.dao.AbstractAnalyzerDao;
import com.njoin.analyticsmanager.entity.analyzer.MotifAnalyzer;

/**
 * A concrete implementation of analyzer dao for motif analyzer
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Repository
public class MotifDaoImpl extends AbstractAnalyzerDao<MotifAnalyzer>{

	@Override
	protected Class<MotifAnalyzer> getClazz() {
		return MotifAnalyzer.class;
	}
}
