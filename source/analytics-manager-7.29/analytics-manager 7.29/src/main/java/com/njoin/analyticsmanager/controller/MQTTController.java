package com.njoin.analyticsmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttribute;

import com.njoin.analyticsmanager.entity.Res;
import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;
import com.njoin.analyticsmanager.service.IMQTTService;

/**
 * Rest Controller for the management of MQTT devices. The class supports the
 * same set of actions as the corresponding back-end server API
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@RestController
public class MQTTController {

	@Autowired
	private IMQTTService mqttService;

	/**
	 * Show all the MQTT devices
	 * 
	 * @param jid
	 * @return
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	@GetMapping("/analytics/mqtt/show")
	public String showMQTT(@SessionAttribute("jid") String jid) throws BackEndServerException, SessionExpiredException {
		return mqttService.showMQTT(jid);
	}

	/**
	 * Create a new MQTT device
	 * 
	 * @param name
	 * @param address
	 * @param identifier
	 * @param jid
	 * @return
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	@GetMapping("/analytics/mqtt/create")
	public Res createMQTT(@RequestParam("name") String name, @RequestParam("address") String address,
			@RequestParam("identifier") String identifier, @SessionAttribute("jid") String jid)
			throws BackEndServerException, SessionExpiredException {
		return new Res(mqttService.createMQTT(name, address, identifier, jid));
	}

}
