package com.njoin.analyticsmanager.entity.analyzer;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * A KNN Analyzer entity
 * 
 * Encompasses the following components, in the order of creation:
 * <p>
 * <ul>
 * <li>KNN Anomaly Model</li>
 * <li>Analysis Engine for KNN Anomaly Model</li>
 * </ul>
 * </p>
 * 
 * @author Henry Xing
 * @version 7.29
 */
@Entity
@DiscriminatorValue("K")
@Table(name="knn_analyzer")
public class KNNAnalyzer extends Analyzer{
	
	@Column(name="model_id")
	private int modelId;
	@Column(name="ae_id")
	private int aeId;
	
	public int getModelId() {
		return modelId;
	}
	public void setModelId(int modelId) {
		this.modelId = modelId;
	}
	public int getAeId() {
		return aeId;
	}
	public void setAeId(int aeId) {
		this.aeId = aeId;
	}
}
