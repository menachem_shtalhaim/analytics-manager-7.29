package com.njoin.analyticsmanager.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.njoin.analyticsmanager.entity.Report;
import com.njoin.analyticsmanager.entity.analyzer.KNNAnalyzer;

/**
 * An Implementation of Abstract Report Service for the KNN analyzer.
 * Particularly, all the training information is maintained in the primary
 * component which contains only register and semantic units
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Service
@Transactional
public class KNNReportServiceImpl extends AbstractReportService<KNNAnalyzer> {

	@Override
	public List<Report> fetchReports(KNNAnalyzer analyzer, String jid, boolean prepareSU) throws Throwable {
		String url = trainReportURL + analyzer.getModelId();
		String result = requestService.getJSON(url, jid, null);
		return extractAllReports(analyzer, analyzer.getModelId(), result, "Analysis Model", false, null, jid,
				prepareSU);
	}
}
