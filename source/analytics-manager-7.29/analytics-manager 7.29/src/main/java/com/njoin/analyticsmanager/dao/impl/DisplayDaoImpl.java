package com.njoin.analyticsmanager.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.njoin.analyticsmanager.dao.AbstractDao;
import com.njoin.analyticsmanager.dao.IDisplayDao;
import com.njoin.analyticsmanager.entity.DisplayInfo;

/**
 * A concrete implementation of Display dao
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Repository
public class DisplayDaoImpl extends AbstractDao<DisplayInfo> implements IDisplayDao{
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public DisplayInfo getDisplayInfoByType(String type) {
		String hql = "FROM DisplayInfo AS d WHERE d.type = ?";
		DisplayInfo result = (DisplayInfo) entityManager.createQuery(hql).setParameter(1, type).getSingleResult();
		return result;
	}

	@Override
	protected Class<DisplayInfo> getClazz() {
		return DisplayInfo.class;
	}
}
