package com.njoin.analyticsmanager.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;
import com.njoin.analyticsmanager.service.IModelService;
import com.njoin.analyticsmanager.service.IRequestService;

/**
 * An Implementation of IModelService
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Service
@Transactional
public class ModelServiceImpl implements IModelService{
	
	@Value("${server.show.url}")
	private String showURL;
	@Autowired
	private IRequestService requestService;

	@Override
	public String getModelStatus(int modelId, String jid) throws BackEndServerException, SessionExpiredException {
		String json = requestService.getJSON(showURL, jid, null);
		try {
			JSONObject object = JSON.parseObject(json);
			JSONArray records = object.getJSONArray("records");
			for (Object record : records) {
				JSONObject jsonRecord = (JSONObject) record;
				if (jsonRecord.getInteger("id") == modelId) {
					return jsonRecord.getString("progress");
				}
			}
		} catch (Exception e) {
			throw new BackEndServerException("Unable to read server response");
		}
		throw new IllegalArgumentException("Model not found");
	}
	
}
