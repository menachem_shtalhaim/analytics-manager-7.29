package com.njoin.analyticsmanager.service;

import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;

/**
 * A Service that feeds register values to the real time analysis framework, the
 * methods directly calls the back-end server API
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
public interface IFeedService {

	/**
	 * Feed the register value to the back-end server
	 * 
	 * @param registerId
	 * @param value
	 * @param jid
	 * @return
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	public String feed(String registerId, String value, String jid)
			throws BackEndServerException, SessionExpiredException;

}
