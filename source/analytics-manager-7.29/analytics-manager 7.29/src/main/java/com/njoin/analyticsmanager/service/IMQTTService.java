package com.njoin.analyticsmanager.service;

import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;

/**
 * A Service that manages MQTT devices, the methods directly calls the back-end
 * server API
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
public interface IMQTTService {

	/**
	 * Show all the MQTT devices
	 * 
	 * @param jid
	 * @return
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	public String showMQTT(String jid) throws BackEndServerException, SessionExpiredException;

	/**
	 * create a new MQTT device
	 * 
	 * @param name
	 * @param address
	 * @param identifier
	 * @param jid
	 * @return the create success message
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	public String createMQTT(String name, String address, String identifier, String jid)
			throws BackEndServerException, SessionExpiredException;

}
