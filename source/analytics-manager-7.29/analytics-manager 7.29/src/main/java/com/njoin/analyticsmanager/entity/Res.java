package com.njoin.analyticsmanager.entity;

/**
 * A simple response entity that wraps around the message to the front-end
 * server so as to provide a uniform json API
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
public class Res {

	private String message;

	public Res(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
