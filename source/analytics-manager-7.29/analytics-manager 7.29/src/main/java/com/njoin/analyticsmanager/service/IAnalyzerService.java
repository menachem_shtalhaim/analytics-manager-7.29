package com.njoin.analyticsmanager.service;

import java.util.List;
import java.util.Map;

import com.njoin.analyticsmanager.entity.Report;
import com.njoin.analyticsmanager.entity.analyzer.Analyzer;
import com.njoin.analyticsmanager.exception.AnalyzerCorruptedException;
import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;

/**
 * A Service that manages the operation of analyzers
 * 
 * @author Henry Xing
 * @version 7.29
 *
 * @param <T>
 *            the type of the analyzer
 */
public interface IAnalyzerService<T extends Analyzer> {

	/**
	 * Create an analyzer from the payload
	 * 
	 * @param payload
	 * @param jid
	 * @return the created analyzer
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	public T create(Map<String, Object> payload, String jid) throws BackEndServerException, SessionExpiredException;

	/**
	 * Delete the analyzer with the specified id
	 * 
	 * @param id
	 * @param jid
	 * @return the delete success message
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	public String delete(int id, String jid) throws BackEndServerException, SessionExpiredException;

	/**
	 * Train the analyzer with the specified id and the specified batch size
	 * 
	 * @param id
	 * @param batch
	 * @param jid
	 * @return the train success message
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	public String train(int id, Integer batch, String jid) throws BackEndServerException, SessionExpiredException;

	/**
	 * Stop the analyzer with the specified id
	 * 
	 * @param id
	 * @param jid
	 * @return the stop success message
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	public String stop(int id, String jid) throws BackEndServerException, SessionExpiredException;

	/**
	 * Remove the schedule from the analyzer with the specified id, does nothing
	 * if the schedule does not exist
	 * 
	 * @param id
	 * @param jid
	 * @return the remove success message
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	public String removeSchedule(int id, String jid) throws BackEndServerException, SessionExpiredException;

	/**
	 * suspend the schedule of the analyzer with the specified id, does nothing
	 * if the schedule does not exist or it is already suspended
	 * 
	 * @param id
	 * @param jid
	 * @return the suspend success message
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	public String suspendSchedule(int id, String jid) throws BackEndServerException, SessionExpiredException;

	/**
	 * resumes the schedule of the analyzer with the specified id, does nothing
	 * if the schedule does not exist or it is already up
	 * 
	 * @param id
	 * @param jid
	 * @return the resume success message
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	public String resumeSchedule(int id, String jid) throws BackEndServerException, SessionExpiredException;

	/**
	 * creates the schedule for the analyzer with the specified id with the
	 * specified cron, overrides the existing schedule
	 * 
	 * @param id
	 * @param jid
	 * @return the create success message
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	public String createSchedule(int id, String cron, String jid)
			throws BackEndServerException, SessionExpiredException;

	/**
	 * toggle the real time analysis monitor of the analyzer
	 * 
	 * @param id
	 * @param jid
	 * @return the toggle success message
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	public String toggleActive(int id, String jid)
			throws BackEndServerException, SessionExpiredException, AnalyzerCorruptedException;

	/**
	 * toggle the real time analysis visibility of the analyzer
	 * 
	 * @param id
	 * @param jid
	 * @return the toggle success message
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	public String toggleVisible(int id, String jid)
			throws BackEndServerException, SessionExpiredException, AnalyzerCorruptedException;

	/**
	 * Retrieves all the training reports of the specified analyzer
	 * 
	 * @param id
	 * @param jid
	 * @return
	 * @throws Throwable
	 */
	public List<Report> getAllReports(int id, String jid) throws Throwable;

}
