package com.njoin.analyticsmanager.service.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.njoin.analyticsmanager.buffer.IGroupCountBuffer;
import com.njoin.analyticsmanager.buffer.IRegisterBuffer;
import com.njoin.analyticsmanager.dao.IAnalyzerDao;
import com.njoin.analyticsmanager.entity.AnalysisEngine;
import com.njoin.analyticsmanager.entity.Records;
import com.njoin.analyticsmanager.entity.analyzer.Analyzer;
import com.njoin.analyticsmanager.entity.analyzer.DupAnalyzer;
import com.njoin.analyticsmanager.entity.analyzer.KNNAnalyzer;
import com.njoin.analyticsmanager.entity.analyzer.MotifAnalyzer;
import com.njoin.analyticsmanager.entity.analyzer.TargetAnalyzer;
import com.njoin.analyticsmanager.exception.AnalyzerCorruptedException;
import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;
import com.njoin.analyticsmanager.service.IAEService;
import com.njoin.analyticsmanager.service.IAnalyzerService;
import com.njoin.analyticsmanager.service.IRequestService;
import com.njoin.analyticsmanager.service.IShowService;

/**
 * An Implementation of IShowService
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Service
@Transactional
public class ShowServiceImpl implements IShowService {

	@Autowired
	private IRequestService requestService;
	@Autowired
	private IAEService aeService;
	@Autowired
	private IAnalyzerService<KNNAnalyzer> knnService;
	@Autowired
	private IAnalyzerService<MotifAnalyzer> motifService;
	@Autowired
	private IAnalyzerService<DupAnalyzer> dupService;
	@Autowired
	private IAnalyzerService<TargetAnalyzer> targetService;
	@Autowired
	private IRegisterBuffer registerBuffer;
	@Autowired
	private IGroupCountBuffer groupCountBuffer;

	@Autowired
	private IAnalyzerDao<KNNAnalyzer> knnDao;
	@Autowired
	private IAnalyzerDao<MotifAnalyzer> motifDao;
	@Autowired
	private IAnalyzerDao<DupAnalyzer> dupDao;
	@Autowired
	private IAnalyzerDao<TargetAnalyzer> targetDao;

	@Value("${server.show.url}")
	private String showURL;

	@Override
	public Records showRecords(String jid)
			throws BackEndServerException, SessionExpiredException, AnalyzerCorruptedException {
		String json = requestService.getJSON(showURL, jid, null);

		List<KNNAnalyzer> knnList = knnDao.getAllUnArchived();
		List<MotifAnalyzer> motifList = motifDao.getAllUnArchived();
		List<DupAnalyzer> dupList = dupDao.getAllUnArchived();
		List<TargetAnalyzer> targetList = targetDao.getAllUnArchived();

		Map<Integer, Integer> groupBufferInput = new HashMap<>();

		extractAnalyzers(json, knnList, motifList, dupList, targetList, groupBufferInput, jid);

		Records result = new Records();
		result.putRecord(Analyzer.KNN_TYPE, knnList);
		result.putRecord(Analyzer.MOTIF_TYPE, motifList);
		result.putRecord(Analyzer.DUP_TYPE, dupList);
		result.putRecord(Analyzer.TARGET_TYPE, targetList);

		registerBuffer.clear();
		groupCountBuffer.prepare(groupBufferInput, jid);

		return result;
	}

	private Map<Integer, JSONObject> getMapFromJSONArray(JSONArray array) {
		Map<Integer, JSONObject> result = new HashMap<>();
		for (Object object : array) {
			JSONObject jsonObject = (JSONObject) object;
			result.put(jsonObject.getInteger("id"), jsonObject);
		}
		return result;
	}

	private void extractAnalyzers(
			String json, 
			List<KNNAnalyzer> knnList, 
			List<MotifAnalyzer> motifList,
			List<DupAnalyzer> dupList, 
			List<TargetAnalyzer> targetList,
			Map<Integer, Integer> groupBufferInput, 
			String jid)
			throws BackEndServerException, AnalyzerCorruptedException, SessionExpiredException {
		try {
			JSONObject object = JSON.parseObject(json);
			JSONArray records = object.getJSONArray("records");
			JSONArray aes = object.getJSONArray("aeList");
			Map<Integer, JSONObject> recordMap = getMapFromJSONArray(records);
			Map<Integer, JSONObject> aeMap = getMapFromJSONArray(aes);
			for (KNNAnalyzer analyzer : knnList) {
				extractKNNAnalyzer(recordMap, aeMap, analyzer, jid);
			}
			for (MotifAnalyzer analyzer : motifList) {
				extractMotifAnalyzer(recordMap, aeMap, analyzer, groupBufferInput, jid);
			}
			for (DupAnalyzer analyzer : dupList) {
				extractDupAnalyzer(recordMap, aeMap, analyzer, groupBufferInput, jid);
			}
			for (TargetAnalyzer analyzer : targetList) {
				extractTargetAnalyzer(recordMap, aeMap, analyzer, groupBufferInput, jid);
			}
		} catch (AnalyzerCorruptedException e) {
			throw e;
		} catch (SessionExpiredException e) {
			throw e;
		} catch (Exception e) {
			throw new BackEndServerException("Unable to read server response");
		}
	}

	private void extractKNNAnalyzer(
			Map<Integer, JSONObject> recordMap, 
			Map<Integer, JSONObject> aeMap,
			KNNAnalyzer knnAnalyzer, 
			String jid) throws AnalyzerCorruptedException, BackEndServerException, SessionExpiredException {

		JSONObject knnObject = recordMap.get(knnAnalyzer.getModelId());
		JSONObject aeObject = aeMap.get(knnAnalyzer.getAeId());

		if (knnObject == null || aeObject == null) {
			knnService.delete(knnAnalyzer.getId(), jid);
			throw new AnalyzerCorruptedException(knnAnalyzer.getId(), true);
		}

		knnAnalyzer.setSchedule(knnObject.getString("schedules"));
		knnAnalyzer.setScheduleState(knnObject.getBoolean("schedule_state"));
		
		Map<String, String> status = new LinkedHashMap<>();
		status.put("Analysis Model", knnObject.getString("progress"));
		knnAnalyzer.setStatus(status);
		
		AnalysisEngine ae = aeService.extractAE(aeObject);
		knnAnalyzer.setAnalysisEngine(ae);
	}

	private void extractMotifAnalyzer(
			Map<Integer, JSONObject> recordMap, 
			Map<Integer, JSONObject> aeMap,
			MotifAnalyzer motifAnalyzer, 
			Map<Integer, Integer> groupBufferInput,
			String jid)
			throws AnalyzerCorruptedException, BackEndServerException, SessionExpiredException {

		JSONObject dummyObject = recordMap.get(motifAnalyzer.getDummyModelId());
		JSONObject giObject = recordMap.get(motifAnalyzer.getGiModelId());
		JSONObject singleObject = recordMap.get(motifAnalyzer.getSingleModelId());
		JSONObject pairObject = recordMap.get(motifAnalyzer.getPairModelId());

		JSONObject singleAeObject = aeMap.get(motifAnalyzer.getSingleAeId());
		JSONObject pairAeObject = aeMap.get(motifAnalyzer.getPairAeId());

		if (dummyObject == null || giObject == null || singleObject == null || pairObject == null
				|| singleAeObject == null || pairAeObject == null) {
			motifService.delete(motifAnalyzer.getId(), jid);
			throw new AnalyzerCorruptedException(motifAnalyzer.getId(), true);
		}
		
		motifAnalyzer.setSchedule(giObject.getString("schedules"));
		motifAnalyzer.setScheduleState(giObject.getBoolean("schedule_state"));

		Map<String, String> status = new LinkedHashMap<>();
		status.put("Anomaly Model", dummyObject.getString("progress"));
		status.put("Group Induction", giObject.getString("progress"));
		motifAnalyzer.setStatus(status);

		AnalysisEngine singleAe = aeService.extractAE(singleAeObject);

		motifAnalyzer.setAnalysisEngine(singleAe);
		
		groupBufferInput.put(motifAnalyzer.getGiModelId(), giObject.getInteger("num_of_tsu"));
	}

	private void extractDupAnalyzer(
			Map<Integer, JSONObject> recordMap, 
			Map<Integer, JSONObject> aeMap,
			DupAnalyzer dupAnalyzer, 
			Map<Integer, Integer> groupBufferInput,
			String jid)
			throws AnalyzerCorruptedException, BackEndServerException, SessionExpiredException {

		JSONObject anomalyObject = recordMap.get(dupAnalyzer.getModelId());
		JSONObject giObject = recordMap.get(dupAnalyzer.getGiModelId());

		JSONObject aeObject = aeMap.get(dupAnalyzer.getAeId());

		if (anomalyObject == null || giObject == null || aeObject == null) {
			dupService.delete(dupAnalyzer.getId(), jid);
			throw new AnalyzerCorruptedException(dupAnalyzer.getId(), true);
		}
		
		dupAnalyzer.setSchedule(giObject.getString("schedules"));
		dupAnalyzer.setScheduleState(giObject.getBoolean("schedule_state"));

		Map<String, String> status = new LinkedHashMap<>();
		status.put("Analysis Model", giObject.getString("progress"));
		dupAnalyzer.setStatus(status);

		AnalysisEngine ae = aeService.extractAE(aeObject);

		dupAnalyzer.setAnalysisEngine(ae);
		
		groupBufferInput.put(dupAnalyzer.getGiModelId(), giObject.getInteger("num_of_tsu"));
	}

	private void extractTargetAnalyzer(
			Map<Integer, JSONObject> recordMap, 
			Map<Integer, JSONObject> aeMap,
			TargetAnalyzer targetAnalyzer, 
			Map<Integer, Integer> groupBufferInput,
			String jid)
			throws AnalyzerCorruptedException, BackEndServerException, SessionExpiredException {

		JSONObject anomalyObject = recordMap.get(targetAnalyzer.getModelId());
		JSONObject giObject = recordMap.get(targetAnalyzer.getGiModelId());

		JSONObject aeObject = aeMap.get(targetAnalyzer.getAeId());

		if (anomalyObject == null || giObject == null || aeObject == null) {
			targetService.delete(targetAnalyzer.getId(), jid);
			throw new AnalyzerCorruptedException(targetAnalyzer.getId(), true);
		}
		
		targetAnalyzer.setSchedule(giObject.getString("schedules"));
		targetAnalyzer.setScheduleState(giObject.getBoolean("schedule_state"));

		Map<String, String> status = new LinkedHashMap<>();
		status.put("Anomaly Model", anomalyObject.getString("progress"));
		status.put("Group Induction", giObject.getString("progress"));
		targetAnalyzer.setStatus(status);

		AnalysisEngine ae = aeService.extractAE(aeObject);

		targetAnalyzer.setAnalysisEngine(ae);
		
		groupBufferInput.put(targetAnalyzer.getGiModelId(), giObject.getInteger("num_of_tsu"));
	}
}