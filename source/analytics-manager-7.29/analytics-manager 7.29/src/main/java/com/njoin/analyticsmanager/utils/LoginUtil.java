package com.njoin.analyticsmanager.utils;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 * A Utility that login to the back-end server
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Deprecated
public class LoginUtil {
	private static final String USERNAME = "njoinadmin";
	private static final String PASSWORD = "1qazse42w3";
	private static final String URL = "https://research3.n-join.net/admin/process_login";
	
	/**
	 * Login and retrieves the JSESSIONID
	 * @return the JSESSIONID cookie from the back-end
	 */
	@Deprecated
	public static String login() {
		RestTemplate template = new RestTemplate();
		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("username", USERNAME);
		map.add("password", PASSWORD);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		HttpEntity<MultiValueMap<String, String>> payload = new HttpEntity<>(map, headers); 

		HttpEntity<String> response = template.exchange(URL, HttpMethod.POST, payload, String.class);
		String cookie = response.getHeaders().getFirst("Set-Cookie");
		int start = cookie.indexOf("JSESSIONID=") + 11;
		int end = cookie.indexOf(';', start);
		String result = cookie.substring(start, end);
		return result;
	}
}
