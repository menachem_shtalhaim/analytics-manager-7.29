package com.njoin.analyticsmanager.dao.impl;

import org.springframework.stereotype.Repository;

import com.njoin.analyticsmanager.dao.AbstractAnalyzerDao;
import com.njoin.analyticsmanager.entity.analyzer.DupAnalyzer;

/**
 * A concrete implementation of analyzer dao for duplicate analyzer
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Repository
public class DupDaoImpl extends AbstractAnalyzerDao<DupAnalyzer>{

	@Override
	protected Class<DupAnalyzer> getClazz() {
		return DupAnalyzer.class;
	}
}
