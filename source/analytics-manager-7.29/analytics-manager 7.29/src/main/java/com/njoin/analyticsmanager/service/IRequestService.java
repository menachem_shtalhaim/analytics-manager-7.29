package com.njoin.analyticsmanager.service;

import java.util.Map;

import org.springframework.util.MultiValueMap;

import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;

/**
 * A Service that handles all the requests to the back-end server. This
 * interface provides multiple methods of request and returns the server
 * response body as a string. It also manages the security mechanism by setting
 * the specified JSESSIONID cookie in the requests
 * 
 * Particularly, the login request to the back-end server does not uses this
 * service because the methods provided in this interface does not retrieve
 * cookies from the server response
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
public interface IRequestService {

	/**
	 * Performs a post request to the back-end server with a json payload
	 * 
	 * @param url
	 *            the request url to the back-end server
	 * @param jid
	 *            the JSESSIONID that the server has set in the cookie for the
	 *            current login session
	 * @param payload
	 *            the request payload that can be formatted to a json, typically
	 *            an entity, a list or a map
	 * @return the response body of the HTTP request as a String
	 * @throws BackEndServerException
	 *             if there is an error accessing or reading the response of the
	 *             back-end server
	 * @throws SessionExpiredException
	 *             if the server redirects to the login page, i.e., the server
	 *             does not recognize the JSESSIONID
	 */
	public <T> String postJSON(String url, String jid, T payload)
			throws BackEndServerException, SessionExpiredException;

	/**
	 * Performs a get request to the back-end server
	 * 
	 * @param url
	 *            the request url to the back-end server
	 * @param jid
	 *            the JSESSIONID that the server has set in the cookie for the
	 *            current login session
	 * @param payload
	 *            the request payload that can be parsed to a list of url
	 *            parameters appended to the url
	 * @return the response body of the HTTP request as a String
	 * @throws BackEndServerException
	 *             if there is an error accessing or reading the response of the
	 *             back-end server
	 * @throws SessionExpiredException
	 *             if the server redirects to the login page, i.e., the server
	 *             does not recognize the JSESSIONID
	 */
	public String getJSON(String url, String jid, Map<String, Object> payload)
			throws BackEndServerException, SessionExpiredException;

	/**
	 * Performs a post request to the back-end server with a url form data
	 * payload
	 * 
	 * @param url
	 *            the request url to the back-end server
	 * @param jid
	 *            the JSESSIONID that the server has set in the cookie for the
	 *            current login session
	 * @param payload
	 *            the request payload which will be interpreted as a url form
	 *            data
	 * @return the response body of the HTTP request as a String
	 * @throws BackEndServerException
	 *             if there is an error accessing or reading the response of the
	 *             back-end server
	 * @throws SessionExpiredException
	 *             if the server redirects to the login page, i.e., the server
	 *             does not recognize the JSESSIONID
	 */
	public String postForm(String url, String jid, MultiValueMap<String, String> payload)
			throws BackEndServerException, SessionExpiredException;

}
