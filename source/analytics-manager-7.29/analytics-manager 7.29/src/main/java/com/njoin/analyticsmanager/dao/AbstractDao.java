package com.njoin.analyticsmanager.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * An abstract generic implementation of the Generic Dao interface, provides the
 * basic functionalities. Client Dao classes must return the class of their
 * entity type to be used
 * 
 * @author Henry Xing
 * @version 7.29
 *
 * @param <T>
 */
public abstract class AbstractDao<T> implements IGenericDao<T> {

	@PersistenceContext
	protected EntityManager entityManager;

	protected Class<T> clazz;

	public AbstractDao() {
		clazz = getClazz();
	}

	/**
	 * Get the class of the entity, called within constructor
	 * 
	 * @return the class of the entity
	 */
	protected abstract Class<T> getClazz();

	@SuppressWarnings("unchecked")
	@Override
	public List<T> getAll() {
		String hql = "FROM " + clazz.getName();
		List<T> results = (List<T>) entityManager.createQuery(hql).getResultList();
		return results;
	}

	@Override
	public T getById(int id) {
		return entityManager.find(clazz, id);
	}

	@Override
	public void delete(T t) {
		entityManager.remove(t);
	}

	@Override
	public T saveOrUpdate(T t) {
		return entityManager.merge(t);
	}

}
