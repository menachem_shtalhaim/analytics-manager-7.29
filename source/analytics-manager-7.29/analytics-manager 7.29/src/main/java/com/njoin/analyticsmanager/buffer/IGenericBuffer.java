package com.njoin.analyticsmanager.buffer;

import java.util.Map;

/**
 * The buffer temporarily maintains the result of expensive queries and/or
 * operations. The information can be retrieved upon the client request. It
 * should also be by the user when the data is not relevant to reduce memory
 * usage.
 * 
 * The buffer is parameterized for different input and output types. The buffer
 * should have session scope as one user session communicates with a single
 * buffer.
 * 
 * The buffer can be implemented as a simple temporary data storage or a
 * multi-threaded processor in the backgroud. Depending on its nature, the
 * retrieve operation may block until the output is available.
 * 
 * @author Henry Xing
 * @version 7.29
 *
 * @param <I>
 *            The type of input for the operation
 * @param <O>
 *            The type of output for the operation
 */
public interface IGenericBuffer<I, O> {
	/**
	 * Reads the input and starts the operation.
	 * 
	 * @param records
	 *            The input of the buffer. Provided as a map of input with their
	 *            integer identifier
	 * @param jid
	 */
	public void prepare(Map<Integer, I> input, String jid);

	/**
	 * Retrieve the output identified by id. The method blocks until the output
	 * is available
	 * 
	 * @param id
	 *            The identifier for the response item
	 * @return The response String
	 * @throws Throwable
	 */
	public O getResponse(int id) throws Throwable;

	/**
	 * Clears the buffer and stops all ongoing operations
	 */
	public void clear();
}
