package com.njoin.analyticsmanager.controller;

import java.util.List;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttribute;

import com.njoin.analyticsmanager.entity.Report;
import com.njoin.analyticsmanager.entity.analyzer.Analyzer;
import com.njoin.analyticsmanager.entity.analyzer.DupAnalyzer;
import com.njoin.analyticsmanager.entity.analyzer.KNNAnalyzer;
import com.njoin.analyticsmanager.entity.analyzer.MotifAnalyzer;
import com.njoin.analyticsmanager.entity.analyzer.TargetAnalyzer;
import com.njoin.analyticsmanager.service.IAnalyzerService;
import com.njoin.analyticsmanager.service.IReportService;

/**
 * Rest Controller for displaying the training reports
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@RestController
public class ReportController {

	@Autowired
	private IAnalyzerService<KNNAnalyzer> knnService;
	@Autowired
	private IAnalyzerService<MotifAnalyzer> motifService;
	@Autowired
	private IAnalyzerService<DupAnalyzer> dupService;
	@Autowired
	private IAnalyzerService<TargetAnalyzer> targetService;
	@Autowired
	private IReportService reportService;

	/**
	 * Display the training report for a specified analyzer. The analyzer is
	 * identified by its type and id. It must exist and unarchived
	 * 
	 * @param type
	 * @param id
	 * @param jid
	 * @return
	 * @throws Throwable
	 */
	@GetMapping("/analytics/report")
	public List<Report> showAllReports(@RequestParam("type") String type, @RequestParam("id") int id,
			@SessionAttribute("jid") String jid) throws Throwable {
		if (type.equals(Analyzer.KNN_TYPE)) {
			return knnService.getAllReports(id, jid);
		} else if (type.equals(Analyzer.MOTIF_TYPE)) {
			return motifService.getAllReports(id, jid);
		} else if (type.equals(Analyzer.DUP_TYPE)) {
			return dupService.getAllReports(id, jid);
		} else if (type.equals(Analyzer.TARGET_TYPE)) {
			return targetService.getAllReports(id, jid);
		} else {
			throw new IllegalArgumentException("Analyzer type not recognized");
		}
	}

	/**
	 * Display the training history of server. The response json includes the
	 * following:
	 * <p>
	 * <ul>
	 * <li>"reports": the training reports of every analyzer in the database,
	 * archived or not</li>
	 * <li>"analyzers": a map of analyzer id to its analyzer information, every
	 * analyzer that has at least one training report will exist in the map</li>
	 * </ul>
	 * </p>
	 * 
	 * @param jid
	 * @return
	 * @throws Throwable
	 */
	@GetMapping("/analytics/history")
	public Map<String, Object> showReportHistory(@SessionAttribute("jid") String jid) throws Throwable {
		return reportService.showReports(jid);
	}

}
