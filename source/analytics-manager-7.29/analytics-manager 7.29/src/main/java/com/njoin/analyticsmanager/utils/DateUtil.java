package com.njoin.analyticsmanager.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * A Utility that parses the dates
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
public class DateUtil {

	/**
	 * Parse the epoch time in miliseconds to the standard string format
	 * 
	 * @param milis
	 *            the epoch time
	 * @return the human-readable date string
	 */
	public static String dateLongToString(long milis) {
		Date date = new Date(milis);
		TimeZone tz = TimeZone.getTimeZone("UTC");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		df.setTimeZone(tz);
		return df.format(date);
	}

	/**
	 * Parse the date string according to the specified pattern
	 * 
	 * @param pattern
	 *            the pattern of the date string
	 * @param string
	 *            the date string
	 * @return the equivalent epoch time in miliseconds
	 * @throws ParseException
	 */
	public static Long dateStringTolong(String pattern, String string) throws ParseException {
		DateFormat df = new SimpleDateFormat(pattern);
		return df.parse(string).getTime();
	}

}
