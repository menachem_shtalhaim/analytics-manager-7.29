package com.njoin.analyticsmanager.service;

/**
 * A Service that sends emails
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
public interface IEmailService {

	/**
	 * Sends an email with the specified fields to the specified address
	 * 
	 * @param to
	 * @param subject
	 * @param body
	 */
	public void send(String to, String subject, String body);

}
