package com.njoin.analyticsmanager.service;

import java.util.Map;

import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;

/**
 * A Service that manages all the simulation statistics and creation
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
public interface ISimulationService {

	/**
	 * Create a new simulation task, this method directly encapsulates the
	 * back-end server API
	 * 
	 * @param payload
	 * @param jid
	 * @return the task creation success message
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	public String createSimulation(Map<String, Object> payload, String jid)
			throws BackEndServerException, SessionExpiredException;

	/**
	 * Retrieves all the simulation and real time analysis data from the
	 * back-end server API, including cache, queue, task, and analyzer monitor
	 * statistics
	 * 
	 * @param jid
	 * @return
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	public Map<String, Object> showSimulation(String jid) throws BackEndServerException, SessionExpiredException;

	/**
	 * Terminate and remove all the running simulation tasks
	 * 
	 * @param jid
	 * @return the removal success message
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	public String removeRunning(String jid) throws BackEndServerException, SessionExpiredException;

	/**
	 * Terminate and remove all the waiting simulation tasks in queue
	 * 
	 * @param jid
	 * @return the removal success message
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	public String removeWaiting(String jid) throws BackEndServerException, SessionExpiredException;

	/**
	 * Remove and refresh all the analyzer real time analysis statistics
	 * 
	 * @param jid
	 * @return the removal success message
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	public String removeStatistics(String jid) throws BackEndServerException, SessionExpiredException;

}
