package com.njoin.analyticsmanager.configuration;

import javax.servlet.annotation.WebListener;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextListener;

/**
 * Configures a request context listener that listens for the current session.
 * Important for managing session scoped buffers
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Configuration
@WebListener
public class AppRequestContextListener extends RequestContextListener {

}
