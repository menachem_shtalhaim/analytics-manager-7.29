package com.njoin.analyticsmanager.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The entity maintains the metadata of each type of analyzer. For each type,
 * there must be one and only one entry for display info. It administers the
 * internal and external names of the type of analyzer as well as restrictions
 * on their register type that they consume
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Entity
@Table(name = "display_info")
public class DisplayInfo {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	@Column(name = "type")
	private String type;
	@Column(name = "display_type")
	private String displayType;
	@Column(name = "numeric")
	private boolean numeric;
	@Column(name = "non_numeric")
	private boolean nonNumeric;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDisplayType() {
		return displayType;
	}

	public void setDisplayType(String displayType) {
		this.displayType = displayType;
	}

	public boolean isNumeric() {
		return numeric;
	}

	public void setNumeric(boolean numeric) {
		this.numeric = numeric;
	}

	public boolean isNonNumeric() {
		return nonNumeric;
	}

	public void setNonNumeric(boolean nonNumeric) {
		this.nonNumeric = nonNumeric;
	}
}
