package com.njoin.analyticsmanager.buffer;

import java.util.Map;
import java.util.concurrent.Future;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import com.njoin.analyticsmanager.service.IRequestService;

/**
 * An implementation of IGroupBuffer
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Service
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class GroupBuffer extends AbstractOperationBuffer<Integer, String> implements IGroupBuffer {

	@Value("${server.train.report.group.url}")
	private String getGroupListURL;
	@Autowired
	private IRequestService requestService;

	/**
	 * The buffer is cleared upon new input.
	 */
	@Override
	public void prepare(Map<Integer, Integer> records, String jid) {
		clear();
		for (Map.Entry<Integer, Integer> entry : records.entrySet()) {
			Integer giId = entry.getValue();
			Future<String> future = threadPool.submit(new GroupCallable(jid, getGroupListURL + giId, requestService));
			results.put(entry.getKey(), future);
		}
	}

}
