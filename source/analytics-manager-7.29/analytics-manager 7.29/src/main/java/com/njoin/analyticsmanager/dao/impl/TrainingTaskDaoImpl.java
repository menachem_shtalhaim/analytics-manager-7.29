package com.njoin.analyticsmanager.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.njoin.analyticsmanager.dao.AbstractDao;
import com.njoin.analyticsmanager.dao.ITrainingTaskDao;
import com.njoin.analyticsmanager.entity.TrainingTask;

/**
 * A concrete implementation of training task dao
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Repository
public class TrainingTaskDaoImpl extends AbstractDao<TrainingTask> implements ITrainingTaskDao{

	@SuppressWarnings("unchecked")
	@Override
	public TrainingTask getFirstTaskByStatus(int status) {
		String hql = "FROM TrainingTask AS t WHERE t.status = :status";
		List<TrainingTask> tasks = (List<TrainingTask>) entityManager.createQuery(hql).setParameter("status", status).setMaxResults(1).getResultList();
		if (!tasks.isEmpty()) {
			return tasks.get(0);
		} else {
			return null;
		}
	}

	@Override
	protected Class<TrainingTask> getClazz() {
		return TrainingTask.class;
	}

}
