package com.njoin.analyticsmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttribute;

import com.njoin.analyticsmanager.entity.Res;
import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;
import com.njoin.analyticsmanager.service.IFeedService;

/**
 * Rest Controller for the feeding register values to the real time analysis
 * framework. back-end server API
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@RestController
public class FeedController {

	@Autowired
	private IFeedService feedService;

	/**
	 * Feed a register alongside its value to the back-end server
	 * 
	 * @param registerId
	 * @param value
	 * @param jid
	 * @return
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	@GetMapping("/analytics/feed")
	public Res feedValue(@RequestParam("registerId") String registerId, @RequestParam("value") String value,
			@SessionAttribute("jid") String jid) throws BackEndServerException, SessionExpiredException {
		return new Res(feedService.feed(registerId, value, jid));
	}

}
