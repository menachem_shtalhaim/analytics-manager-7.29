package com.njoin.analyticsmanager.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.njoin.analyticsmanager.entity.Report;
import com.njoin.analyticsmanager.entity.analyzer.MotifAnalyzer;

/**
 * An Implementation of Abstract Report Service for the Motif analyzer.
 * Particularly, the training information is maintained in a primary component
 * which contains register semantic units and a group induction component with
 * the group semantic units
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Service
@Transactional
public class MotifReportServiceImpl extends AbstractReportService<MotifAnalyzer> {

	@Override
	public List<Report> fetchReports(MotifAnalyzer analyzer, String jid, boolean prepareSU) throws Throwable {
		String anomalyURL = trainReportURL + analyzer.getDummyModelId();
		String anomalyResult = requestService.getJSON(anomalyURL, jid, null);
		String giURL = trainReportURL + analyzer.getGiModelId();
		String giResult = requestService.getJSON(giURL, jid, null);
		List<Report> reports = extractAllReports(analyzer, analyzer.getGiModelId(), anomalyResult, "Anomaly Model",
				false, null, jid, prepareSU);
		addGroupInductionModelToEachReport(giResult, reports, "Group Induction", analyzer.getGiModelId(), jid,
				prepareSU);
		return reports;
	}

}
