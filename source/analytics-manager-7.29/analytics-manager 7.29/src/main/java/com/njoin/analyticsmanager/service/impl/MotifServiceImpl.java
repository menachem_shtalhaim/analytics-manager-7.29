package com.njoin.analyticsmanager.service.impl;

import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.njoin.analyticsmanager.dao.IDisplayDao;
import com.njoin.analyticsmanager.entity.DBInput;
import com.njoin.analyticsmanager.entity.analyzer.Analyzer;
import com.njoin.analyticsmanager.entity.analyzer.MotifAnalyzer;
import com.njoin.analyticsmanager.exception.AnalyzerCorruptedException;
import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;

/**
 * An Implementation of Abstract Analyzer Service for the motif analyzer.
 * 
 * Particularly, the creation of the analyzer involves, sequentially, the dummy
 * motif model, the motif single register model, the motif register pair model
 * and the motif group induction model
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Service
@Transactional
public class MotifServiceImpl extends AbstractAnalyzerService<MotifAnalyzer> {

	@Autowired
	private IDisplayDao displayDao;

	private void checkExist() {
		if (analyzerDao.getAll().size() != 0) {
			String type = displayDao.getDisplayInfoByType(Analyzer.MOTIF_TYPE).getDisplayType();
			throw new IllegalArgumentException("You can only maintain one " + type + " analyzer");
		}
	}

	private void analyzerSpecificPrepareRequest(Map<String, Object> dummyRequest, Map<String, Object> singleRequest,
			Map<String, Object> pairRequest, Map<String, Object> giRequest, MotifAnalyzer analyzer) {
		dummyRequest.put("type", "DummyMotifModel");
		singleRequest.put("type", "MotifSingleRegisterAnomalyModel");
		pairRequest.put("type", "MotifRegisterPairAnomalyModel");
		giRequest.put("type", "MotifPatternGroupInductionModel");
		analyzer.setType(Analyzer.MOTIF_TYPE);
		analyzer.setDisplayType(displayDao.getDisplayInfoByType(Analyzer.MOTIF_TYPE).getDisplayType());
	}

	private void createSequence(MotifAnalyzer analyzer, Map<String, Object> payload, Map<String, Object> dummyRequest,
			Map<String, Object> singleRequest, Map<String, Object> pairRequest, Map<String, Object> giRequest,
			String jid) throws BackEndServerException, SessionExpiredException {
		String dummyResult = requestService.postJSON(createModelURL, jid, dummyRequest);
		String singleResult = requestService.postJSON(createModelURL, jid, singleRequest);
		String pairResult = requestService.postJSON(createModelURL, jid, pairRequest);
		int dummyModelId = processCreateResult(dummyResult);
		int singleModelId = processCreateResult(singleResult);
		int pairModelId = processCreateResult(pairResult);

		giRequest.put("anomaly_model_modelid", pairModelId);
		String giResult = requestService.postJSON(createModelURL, jid, giRequest);
		int giModelId = processCreateResult(giResult);

		int singleAeId = aeService.createAE(payload, singleModelId, "single@", jid);
		int pairAeId = aeService.createAE(payload, pairModelId, "pair@", jid);

		analyzer.setDummyModelId(dummyModelId);
		analyzer.setSingleModelId(singleModelId);
		analyzer.setPairModelId(pairModelId);
		analyzer.setGiModelId(giModelId);
		analyzer.setSingleAeId(singleAeId);
		analyzer.setPairAeId(pairAeId);
	}

	@Override
	public MotifAnalyzer createRequestFromFile(Map<String, Object> payload, String fileContent, String jid)
			throws BackEndServerException, SessionExpiredException {

		checkExist();

		MotifAnalyzer analyzer = new MotifAnalyzer();
		populateAnalyzer(analyzer, payload);

		Map<String, Object> dummyRequest = prepairModelRequest(analyzer, "dummy@");
		Map<String, Object> singleRequest = prepairModelRequest(analyzer, "single@");
		Map<String, Object> pairRequest = prepairModelRequest(analyzer, "pair@");
		Map<String, Object> giRequest = prepairModelRequest(analyzer, "gi@");

		analyzerSpecificPrepareRequest(dummyRequest, singleRequest, pairRequest, giRequest, analyzer);

		populateFileRequest(dummyRequest, fileContent);

		createSequence(analyzer, payload, dummyRequest, singleRequest, pairRequest, giRequest, jid);

		return analyzer;
	};

	@Override
	public MotifAnalyzer createRequestFromDB(Map<String, Object> payload, DBInput input, String jid)
			throws BackEndServerException, SessionExpiredException {

		checkExist();

		MotifAnalyzer analyzer = new MotifAnalyzer();
		populateAnalyzer(analyzer, payload);

		Map<String, Object> dummyRequest = prepairModelRequest(analyzer, "dummy@");
		Map<String, Object> singleRequest = prepairModelRequest(analyzer, "single@");
		Map<String, Object> pairRequest = prepairModelRequest(analyzer, "pair@");
		Map<String, Object> giRequest = prepairModelRequest(analyzer, "gi@");

		analyzerSpecificPrepareRequest(dummyRequest, singleRequest, pairRequest, giRequest, analyzer);

		populateDBRequest(dummyRequest, input);

		createSequence(analyzer, payload, dummyRequest, singleRequest, pairRequest, giRequest, jid);
		return analyzer;
	};

	@Override
	public String delete(int id, String jid) throws BackEndServerException, SessionExpiredException {
		MotifAnalyzer analyzer = retriveExist(id);
		requestOperation(analyzer.getSingleAeId(), deleteAEURL, jid);
		analyzer.setArchived(true);
		analyzerDao.saveOrUpdate(analyzer);
		requestOperation(analyzer.getPairAeId(), deleteAEURL, jid);
		requestOperation(analyzer.getGiModelId(), deleteModelURL, jid);
		requestOperation(analyzer.getSingleModelId(), deleteModelURL, jid);
		requestOperation(analyzer.getPairModelId(), deleteModelURL, jid);
		requestOperation(analyzer.getDummyModelId(), deleteModelURL, jid);
		return "Analyzer removed";
	}

	@Override
	public String train(int id, Integer batch, String jid) throws BackEndServerException, SessionExpiredException {

		MotifAnalyzer analyzer = retriveExist(id);
		trainOperation(analyzer.getGiModelId(), batch, jid);
		return "Model submitted for training";
	}

	@Override
	public String stop(int id, String jid) throws BackEndServerException, SessionExpiredException {

		MotifAnalyzer analyzer = retriveExist(id);
		requestOperation(analyzer.getGiModelId(), stopURL, jid);
		return "Model train session abort request submitted";
	}

	@Override
	public String removeSchedule(int id, String jid) throws BackEndServerException, SessionExpiredException {
		MotifAnalyzer analyzer = retriveExist(id);
		String result = requestOperation(analyzer.getGiModelId(), removeScheduleURL, jid);
		return parseScheduleResponse(result);
	}

	@Override
	public String suspendSchedule(int id, String jid) throws BackEndServerException, SessionExpiredException {
		MotifAnalyzer analyzer = retriveExist(id);
		String result = requestOperation(analyzer.getGiModelId(), suspendScheduleURL, jid);
		return parseScheduleResponse(result);
	}

	@Override
	public String resumeSchedule(int id, String jid) throws BackEndServerException, SessionExpiredException {
		MotifAnalyzer analyzer = retriveExist(id);
		String result = requestOperation(analyzer.getGiModelId(), resumeScheduleURL, jid);
		return parseScheduleResponse(result);
	}

	@Override
	public String createSchedule(int id, String cron, String jid)
			throws BackEndServerException, SessionExpiredException {
		MotifAnalyzer analyzer = retriveExist(id);
		String result = scheduleOperation(analyzer.getGiModelId(), cron, jid);
		return parseScheduleCreateResponse(result);
	}

	@Override
	public String toggleActive(int id, String jid)
			throws BackEndServerException, SessionExpiredException, AnalyzerCorruptedException {
		MotifAnalyzer analyzer = retriveExist(id);
		aeService.toggleActive(analyzer.getSingleAeId(), analyzer.getId(), jid);
		return aeService.toggleActive(analyzer.getPairAeId(), analyzer.getId(), jid);
	}

	@Override
	public String toggleVisible(int id, String jid)
			throws BackEndServerException, SessionExpiredException, AnalyzerCorruptedException {
		MotifAnalyzer analyzer = retriveExist(id);
		aeService.toggleVisible(analyzer.getSingleAeId(), analyzer.getId(), jid);
		return aeService.toggleVisible(analyzer.getPairAeId(), analyzer.getId(), jid);
	}
}