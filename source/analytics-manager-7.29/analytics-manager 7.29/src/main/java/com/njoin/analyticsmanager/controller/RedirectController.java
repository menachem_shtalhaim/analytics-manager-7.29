package com.njoin.analyticsmanager.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Controller that wraps around the back-end server pages. Useful if the user
 * want to access the original server pages
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Controller
public class RedirectController {

	@Value("${server.admin.homepage.url}")
	private String originalURL;

	/**
	 * Redirect to the admin home page of the original server
	 * 
	 * @param response
	 * @throws IOException
	 */
	@GetMapping("/original")
	public void getOriginal(HttpServletResponse response) throws IOException {
		response.sendRedirect(originalURL);
	}

}
