package com.njoin.analyticsmanager.buffer;

import java.util.concurrent.Callable;

import org.springframework.util.MultiValueMap;

import com.njoin.analyticsmanager.service.IRequestService;

/**
 * Callable for query of state list. It requests for the data from the back-end
 * server
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
public class StateListCallable implements Callable<String> {

	private String jid;
	private MultiValueMap<String, String> map;
	private String url;
	private IRequestService requestService;

	public StateListCallable(String jid, MultiValueMap<String, String> map, String url,
			IRequestService requestService) {
		super();
		this.jid = jid;
		this.map = map;
		this.url = url;
		this.requestService = requestService;
	}

	@Override
	public String call() throws Exception {
		return requestService.postForm(url, jid, map);
	}

}
