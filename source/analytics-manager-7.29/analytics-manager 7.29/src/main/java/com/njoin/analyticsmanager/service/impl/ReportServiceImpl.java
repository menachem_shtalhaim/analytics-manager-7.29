package com.njoin.analyticsmanager.service.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.njoin.analyticsmanager.dao.IAnalyzerDao;
import com.njoin.analyticsmanager.dao.IGenericDao;
import com.njoin.analyticsmanager.entity.Report;
import com.njoin.analyticsmanager.entity.analyzer.Analyzer;
import com.njoin.analyticsmanager.entity.analyzer.DupAnalyzer;
import com.njoin.analyticsmanager.entity.analyzer.KNNAnalyzer;
import com.njoin.analyticsmanager.entity.analyzer.MotifAnalyzer;
import com.njoin.analyticsmanager.entity.analyzer.TargetAnalyzer;
import com.njoin.analyticsmanager.service.IAnalyzerReportService;
import com.njoin.analyticsmanager.service.IReportService;

/**
 * An Implementation of IReportService
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Service
@Transactional
public class ReportServiceImpl implements IReportService {

	@Autowired
	private IAnalyzerReportService<KNNAnalyzer> knnReportService;
	@Autowired
	private IAnalyzerReportService<MotifAnalyzer> motifReportService;
	@Autowired
	private IAnalyzerReportService<DupAnalyzer> dupReportService;
	@Autowired
	private IAnalyzerReportService<TargetAnalyzer> targetReportService;

	@Autowired
	private IAnalyzerDao<KNNAnalyzer> knnDao;
	@Autowired
	private IAnalyzerDao<MotifAnalyzer> motifDao;
	@Autowired
	private IAnalyzerDao<DupAnalyzer> dupDao;
	@Autowired
	private IAnalyzerDao<TargetAnalyzer> targetDao;
	@Autowired
	private IAnalyzerDao<Analyzer> analyzerDao;
	@Autowired
	private IGenericDao<Report> reportDao;

	@Override
	public List<Report> getAllReports(String jid) throws Throwable {
		List<KNNAnalyzer> knnAnalyzers = knnDao.getAllUnArchived();
		List<MotifAnalyzer> motifAnalyzers = motifDao.getAllUnArchived();
		List<DupAnalyzer> dupAnalyzers = dupDao.getAllUnArchived();
		List<TargetAnalyzer> targetAnalyzers = targetDao.getAllUnArchived();

		List<Report> reports = new LinkedList<>();
		for (KNNAnalyzer knnAnalyzer : knnAnalyzers) {
			reports.addAll(knnReportService.fetchReports(knnAnalyzer, jid, false));
		}
		for (MotifAnalyzer motifAnalyzer : motifAnalyzers) {
			reports.addAll(motifReportService.fetchReports(motifAnalyzer, jid, false));
		}
		for (DupAnalyzer dupAnalyzer : dupAnalyzers) {
			reports.addAll(dupReportService.fetchReports(dupAnalyzer, jid, false));
		}
		for (TargetAnalyzer targetAnalyzer : targetAnalyzers) {
			reports.addAll(targetReportService.fetchReports(targetAnalyzer, jid, false));
		}
		return reports;
	}

	@Override
	public List<Report> update(List<Report> reports, boolean manage) {
		List<Report> updated = new LinkedList<>();
		for (Report report : reports) {
			if (update(report, manage)) {
				updated.add(report);
			}
		}
		return updated;
	}

	@Override
	public List<Report> getAllSaved() {
		return reportDao.getAll();
	}

	private boolean update(Report report, boolean manage) {
		Report dbReport = reportDao.getById(report.getId());
		if (dbReport == null) {
			// New Report
			if (report.getStatus().equals("RUNNING")) {
				// Running analyzer
				// not need to manage
				report.setManageStatus(0);
				reportDao.saveOrUpdate(report);
				return false;
			} else {
				// Stoped analyzer
				if (manage) {
					// managing and archiving the analyzer
					report.setManageStatus(2);
					reportDao.saveOrUpdate(report);
					return true;
				} else {
					// update report but not managed
					report.setManageStatus(0);
					reportDao.saveOrUpdate(report);
					return false;
				}
			}

		} else {
			// Report already exists
			if (manage) {
				if (dbReport.getManageStatus() == 2) {
					// report is already archived
					return false;
				} else {
					// report is not archived
					if (report.getStatus().equals(dbReport.getStatus())) {
						// report status is not changed, managing the report if
						// not already
						dbReport.setManageStatus(1);
						dbReport.getModelReports().putAll(report.getModelReports());
						return false;
					} else {
						// report status changed (analyzer stopped)
						// managing and archiving the report
						dbReport.setManageStatus(1);
						dbReport.setStatus(report.getStatus());
						dbReport.getModelReports().putAll(report.getModelReports());
						return true;
					}
				}
			} else {
				// updating the report, does not change the manage status
				dbReport.setStatus(report.getStatus());
				dbReport.getModelReports().putAll(report.getModelReports());
				return false;
			}
		}
	}

	@Override
	public Map<String, Object> showReports(String jid) throws Throwable {
		update(getAllReports(jid), false);
		List<Report> reports = getAllSaved();
		Map<Integer, Analyzer> map = new HashMap<>();
		for (Report report : reports) {
			int analyzerId = report.getAnalyzerId();
			if (!map.containsKey(analyzerId)) {
				map.put(analyzerId, analyzerDao.getById(analyzerId));
			}
		}
		Map<String, Object> result = new HashMap<>(2);
		result.put("reports", reports);
		result.put("analyzers", map);
		return result;
	}

}
