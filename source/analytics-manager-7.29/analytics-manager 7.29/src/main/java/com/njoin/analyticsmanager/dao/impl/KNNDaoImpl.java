package com.njoin.analyticsmanager.dao.impl;

import org.springframework.stereotype.Repository;

import com.njoin.analyticsmanager.dao.AbstractAnalyzerDao;
import com.njoin.analyticsmanager.entity.analyzer.KNNAnalyzer;

/**
 * A concrete implementation of analyzer dao for KNN analyzer
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Repository
public class KNNDaoImpl extends AbstractAnalyzerDao<KNNAnalyzer>{

	@Override
	protected Class<KNNAnalyzer> getClazz() {
		return KNNAnalyzer.class;
	}
}
