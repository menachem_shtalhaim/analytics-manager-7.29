package com.njoin.analyticsmanager.service;

import com.alibaba.fastjson.JSONArray;
import com.njoin.analyticsmanager.entity.DBInput;

/**
 * A Service that manages DBInput
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
public interface IDBInputService {

	/**
	 * Extract a dbinput entity from the back-end server json response
	 * 
	 * @param attributes
	 * @param numeric
	 *            whether numeric is allowed for the analyzer
	 * @param nonNumeric
	 *            whether non-numeric is allowed for the analyzer
	 * @return the extracted dbinput
	 */
	public DBInput extractDBInput(JSONArray attributes, boolean numeric, boolean nonNumeric);

}
