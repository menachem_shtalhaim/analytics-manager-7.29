package com.njoin.analyticsmanager.service.impl;

import java.text.ParseException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.njoin.analyticsmanager.dao.IDisplayDao;
import com.njoin.analyticsmanager.dao.IGenericDao;
import com.njoin.analyticsmanager.entity.AnalyzerStat;
import com.njoin.analyticsmanager.entity.DisplayInfo;
import com.njoin.analyticsmanager.entity.SimulationTask;
import com.njoin.analyticsmanager.entity.analyzer.Analyzer;
import com.njoin.analyticsmanager.entity.analyzer.DupAnalyzer;
import com.njoin.analyticsmanager.entity.analyzer.KNNAnalyzer;
import com.njoin.analyticsmanager.entity.analyzer.MotifAnalyzer;
import com.njoin.analyticsmanager.entity.analyzer.TargetAnalyzer;
import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;
import com.njoin.analyticsmanager.service.IRequestService;
import com.njoin.analyticsmanager.service.ISimulationService;
import com.njoin.analyticsmanager.utils.DateUtil;

/**
 * An Implementation of ISimulationService
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Service
@Transactional
public class SimulationServiceImpl implements ISimulationService {

	@Value("${simulation.show.motif}")
	private boolean showMotif;

	@Value("${server.simulation.create.url}")
	private String simulationCreateURL;
	@Value("${server.simulation.show.url}")
	private String simulationShowURL;
	@Value("${server.simulation.remove.running.url}")
	private String removeRunningURL;
	@Value("${server.simulation.remove.waiting.url}")
	private String removeWaitingURL;
	@Value("${server.simulation.remove.statistics.url}")
	private String removeStatisticsURL;

	@Autowired
	private IGenericDao<KNNAnalyzer> knnDao;
	@Autowired
	private IGenericDao<MotifAnalyzer> motifDao;
	@Autowired
	private IGenericDao<DupAnalyzer> dupDao;
	@Autowired
	private IGenericDao<TargetAnalyzer> targetDao;
	@Autowired
	private IDisplayDao displayDao;

	@Autowired
	private IRequestService requestService;

	@Override
	public String createSimulation(Map<String, Object> payload, String jid)
			throws BackEndServerException, SessionExpiredException {
		if (payload.get("from") == null || payload.get("to") == null || payload.get("frequency") == null) {
			throw new IllegalArgumentException(
					"Parameters 'from', 'to', and 'frequency' are required for simulation task");
		}
		Map<String, Object> request = new HashMap<>();
		try {
			request.put("utc_from", (Long) payload.get("from"));
			request.put("utc_to", (Long) payload.get("to"));
			request.put("simulatorSampleRate", (Integer) payload.get("frequency"));
		} catch (ClassCastException e) {
			throw new IllegalArgumentException("Incorrect data format for simulation task");
		}

		request.put("show_motif_cache", showMotif);
		requestService.postJSON(simulationCreateURL, jid, request);
		return "Simulation task created";
	}

	@Override
	public Map<String, Object> showSimulation(String jid) throws BackEndServerException, SessionExpiredException {
		String result = requestService.postJSON(simulationShowURL, jid, null);
		Map<String, Object> response = new HashMap<>();
		try {
			JSONObject json = JSONObject.parseObject(result);
			response.put("cacheSizeTotal", json.getLong("motif_cache_size"));
			response.put("cacheSizeMin", json.getLong("motif_cache_min"));
			response.put("cacheSizeMax", json.getLong("motif_cache_max"));
			response.put("cacheSizeAvg", json.getLong("motif_cache_avg"));
			response.put("cacheNum", json.getLong("motif_cache_num_registers"));
			response.put("queueSize", json.getLong("backlog"));
			response.put("queueSizeMax", json.getLong("backlog_queue_size"));
			List<SimulationTask> running = new LinkedList<>();
			List<SimulationTask> waiting = new LinkedList<>();
			for (Object object : json.getJSONArray("sim_tasks_running")) {
				running.add(parseTask((JSONObject) object));
			}
			for (Object object : json.getJSONArray("sim_tasks_waiting")) {
				waiting.add(parseTask((JSONObject) object));
			}
			response.put("running", running);
			response.put("waiting", waiting);

			List<AnalyzerStat> analyzerStats = parseStats(json.getJSONArray("engines_stat"));
			response.put("analyzerStats", analyzerStats);
		} catch (Exception e) {
			throw new BackEndServerException("Unable to read server response");
		}
		return response;
	}

	private SimulationTask parseTask(JSONObject json) throws ParseException {
		SimulationTask task = new SimulationTask();
		task.setFrequency(json.getIntValue("sample_rate"));
		task.setStart(DateUtil.dateStringTolong(SimulationTask.PATTERN, json.getString("start_date")));
		task.setEnd(DateUtil.dateStringTolong(SimulationTask.PATTERN, json.getString("end_date")));
		task.setProcessed(json.getLongValue("processed"));
		task.setProgress(json.getDoubleValue("progress"));
		return task;
	}

	private List<AnalyzerStat> parseStats(JSONArray jsonArray) {
		List<AnalyzerStat> analyzerStats = new LinkedList<>();
		Map<Integer, JSONObject> map = new HashMap<>();
		for (Object o : jsonArray) {
			JSONObject jsonObject = (JSONObject) o;
			map.put(jsonObject.getInteger("engine_id"), jsonObject);
		}
		List<KNNAnalyzer> knnAnalyzers = knnDao.getAll();
		List<MotifAnalyzer> motifAnalyzers = motifDao.getAll();
		List<DupAnalyzer> dupAnalyzers = dupDao.getAll();
		List<TargetAnalyzer> targetAnalyzers = targetDao.getAll();

		DisplayInfo knnInfo = displayDao.getDisplayInfoByType(Analyzer.KNN_TYPE);
		DisplayInfo motifInfo = displayDao.getDisplayInfoByType(Analyzer.MOTIF_TYPE);
		DisplayInfo dupInfo = displayDao.getDisplayInfoByType(Analyzer.DUP_TYPE);
		DisplayInfo targetInfo = displayDao.getDisplayInfoByType(Analyzer.TARGET_TYPE);

		JSONObject jsonObject = null;
		for (KNNAnalyzer knnAnalyzer : knnAnalyzers) {
			knnAnalyzer.setDisplayType(knnInfo.getDisplayType());
			jsonObject = map.get(knnAnalyzer.getAeId());
			if (jsonObject != null) {
				AnalyzerStat analyzerStat = parseStat(jsonObject);
				analyzerStat.setAnalyzer(knnAnalyzer);
				analyzerStat.setType("Register Monitor");
				analyzerStats.add(analyzerStat);
			}
		}
		for (MotifAnalyzer motifAnalyzer : motifAnalyzers) {
			motifAnalyzer.setDisplayType(motifInfo.getDisplayType());
			jsonObject = map.get(motifAnalyzer.getSingleAeId());
			if (jsonObject != null) {
				AnalyzerStat analyzerStat = parseStat(jsonObject);
				analyzerStat.setAnalyzer(motifAnalyzer);
				analyzerStat.setType("Register Monitor");
				analyzerStats.add(analyzerStat);
			}
			jsonObject = map.get(motifAnalyzer.getPairAeId());
			if (jsonObject != null) {
				AnalyzerStat analyzerStat = parseStat(jsonObject);
				analyzerStat.setAnalyzer(motifAnalyzer);
				analyzerStat.setType("Group Monitor");
				analyzerStats.add(analyzerStat);
			}
		}
		for (DupAnalyzer dupAnalyzer : dupAnalyzers) {
			dupAnalyzer.setDisplayType(dupInfo.getDisplayType());
			jsonObject = map.get(dupAnalyzer.getAeId());
			if (jsonObject != null) {
				AnalyzerStat analyzerStat = parseStat(jsonObject);
				analyzerStat.setAnalyzer(dupAnalyzer);
				analyzerStat.setType("Group Monitor");
				analyzerStats.add(analyzerStat);
			}
		}
		for (TargetAnalyzer targetAnalyzer : targetAnalyzers) {
			targetAnalyzer.setDisplayType(targetInfo.getDisplayType());
			jsonObject = map.get(targetAnalyzer.getAeId());
			if (jsonObject != null) {
				AnalyzerStat analyzerStat = parseStat(jsonObject);
				analyzerStat.setAnalyzer(targetAnalyzer);
				analyzerStat.setType("Group Monitor");
				analyzerStats.add(analyzerStat);
			}
		}
		return analyzerStats;
	}

	private AnalyzerStat parseStat(JSONObject json) {
		AnalyzerStat analyzerStat = new AnalyzerStat();
		analyzerStat.setAvgTime(json.getDoubleValue("engine_avg"));
		analyzerStat.setLastAvgTime(json.getDoubleValue("engine_avg_last"));
		analyzerStat.setLastTime(json.getDoubleValue("engine_last_duration"));
		analyzerStat.setMaxTime(json.getDoubleValue("engine_max_duration"));
		analyzerStat.setHits(json.getIntValue("engine_total_hits"));
		analyzerStat.setAnomalies(json.getIntValue("engine_num_anomalies"));
		return analyzerStat;
	}

	@Override
	public String removeRunning(String jid) throws BackEndServerException, SessionExpiredException {
		requestService.getJSON(removeRunningURL, jid, null);
		return "Running task removed";
	}

	@Override
	public String removeWaiting(String jid) throws BackEndServerException, SessionExpiredException {
		requestService.getJSON(removeWaitingURL, jid, null);
		return "All waiting tasks removed";
	}

	@Override
	public String removeStatistics(String jid) throws BackEndServerException, SessionExpiredException {
		requestService.getJSON(removeStatisticsURL, jid, null);
		return "All analyzer statistics removed";
	}

}
