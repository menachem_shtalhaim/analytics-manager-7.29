package com.njoin.analyticsmanager.exception;

/**
 * An Exception indicating that the session is expired, thrown when the system
 * is unable to login to the back-end server
 * 
 * Should be handled by login in again
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@SuppressWarnings("serial")
public class SessionExpiredException extends Exception {
	private String message;

	public SessionExpiredException() {
		this.message = "Session expired";
	}

	@Override
	public String getMessage() {
		return message;
	}
}