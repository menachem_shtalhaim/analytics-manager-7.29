package com.njoin.analyticsmanager.aspect;

import java.util.Map;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * An AspectJ service for logging at predefined cutting points. This class does
 * not interfere with the normal functionality of the system and can be removed
 * or disabled safely.
 * 
 * Particularly, this class logs every request to the original back-end server
 * as well as its response. This is logged at the trace level. It should be
 * enabled with care since sometimes the back-end response might be large and
 * may halt the server.
 * 
 * It also logs every notification refresh at the debug level
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Component
@Aspect
public class LogAspect {

	private static final Logger LOGGER = LoggerFactory.getLogger(LogAspect.class);

	/**
	 * Logs the url and payload to every back-end server request.
	 * 
	 * @param url
	 * @param jid
	 * @param payload
	 */
	@Before("execution(public java.lang.String com.njoin.analyticsmanager.service.impl.RequestServiceImpl.* (..)) && args(url, jid, payload,..)")
	public void logBeforeRequest(String url, String jid, Map<String, Object> payload) {
		StringBuilder sb = new StringBuilder("\n    Request to backend server:\n");
		sb.append("        URL: ");
		sb.append(url);
		sb.append('\n');
		if (payload == null) {
			sb.append("        Payload: null");
		} else {
			sb.append("        Payload:\n");
			for (String key : payload.keySet()) {
				sb.append("            ");
				sb.append(key);
				sb.append(" : ");
				sb.append(payload.get(key));
				sb.append('\n');
			}
		}
		LOGGER.trace(sb.toString());
	}

	/**
	 * Logs the return value of every back-end server request. This sometimes
	 * halt the server due to large responses
	 * 
	 * @param returnVal
	 */
	@AfterReturning(value = "execution(public java.lang.String com.njoin.analyticsmanager.service.impl.RequestServiceImpl.* (..))", returning = "returnVal")
	public void logAfterRequest(String returnVal) {
		LOGGER.trace(returnVal);
	}

	@Before("execution(public void com.njoin.analyticsmanager.notification.TrainingNotificationService.notification())")
	public void logBeforeNotification() {
		LOGGER.debug("Entering notification()");
	}

	@AfterReturning("execution(public void com.njoin.analyticsmanager.notification.TrainingNotificationService.notification())")
	public void logAfterNotification() {
		LOGGER.debug("Returning from notification()");
	}

}
