package com.njoin.analyticsmanager.service;

import java.util.List;
import java.util.Map;

import com.njoin.analyticsmanager.entity.Report;

/**
 * A non generic service that manages the training reports, provides a uniform
 * interface for analyzer level training report management across different
 * types
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
public interface IReportService {

	/**
	 * Retrieves all the saved training reports in the database alongside a
	 * relevant list of corresponding analyzers, whether archived or not. This
	 * function does not updates the reports in the database.
	 * 
	 * @param jid
	 * @return a map with "reports" paired with the list of reports and
	 *         "analyzers" paired with the analyzer information that has at
	 *         least one training reports
	 * @throws Throwable
	 */
	public Map<String, Object> showReports(String jid) throws Throwable;

	/**
	 * Fetch all the training reports from the back-end server, corresponding to
	 * all the unarchived analyzers in the database. This method does not access
	 * or updates the database for training reports. This function also does not
	 * fetch the semantic units for each report
	 * 
	 * @param jid
	 * @return a list of all training reports for unarchived analyzers
	 * @throws Throwable
	 */
	public List<Report> getAllReports(String jid) throws Throwable;

	/**
	 * Updates the database with the new report, updates the report and saves it
	 * in the appropriate management status
	 * 
	 * @param report
	 *            the new report to be added updated in the database
	 * @param manage
	 *            whether this operation will manage and archive the report
	 * @return whether the report is archived after this operation (manage
	 *         status set to 2). always returns false if manage is false.
	 */
	public List<Report> update(List<Report> reports, boolean manage);

	/**
	 * Retrieves all the saved training reports from the database with all the
	 * status corresponding to all the analyzers, whether archived or not. This
	 * function does not updates the reports in the database.
	 * 
	 * @return a list of all reports saved in the database
	 */
	public List<Report> getAllSaved();

}
