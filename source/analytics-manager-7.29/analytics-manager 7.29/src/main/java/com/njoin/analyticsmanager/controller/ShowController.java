package com.njoin.analyticsmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttribute;

import com.njoin.analyticsmanager.entity.Records;
import com.njoin.analyticsmanager.exception.AnalyzerCorruptedException;
import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;
import com.njoin.analyticsmanager.service.IShowService;

/**
 * Rest Controller for showing all the analyzers
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@RestController
public class ShowController {

	@Autowired
	private IShowService showService;

	/**
	 * Show all the analyzers that has not been archived
	 * 
	 * @param jid
	 * @return
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 * @throws AnalyzerCorruptedException
	 */
	@GetMapping(value = "analytics/show", produces = MediaType.APPLICATION_JSON_VALUE)
	public Records show(@SessionAttribute("jid") String jid)
			throws BackEndServerException, SessionExpiredException, AnalyzerCorruptedException {
		Records records = showService.showRecords(jid);
		return records;
	}
}
