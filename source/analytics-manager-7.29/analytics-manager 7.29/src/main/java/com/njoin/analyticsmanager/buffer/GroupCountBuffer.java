package com.njoin.analyticsmanager.buffer;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

/**
 * An implementation of IGroupCountBuffer. The buffer input comes in batches and
 * clears with new input
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Service
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class GroupCountBuffer implements IGroupCountBuffer {

	private ConcurrentHashMap<Integer, Integer> data = new ConcurrentHashMap<>();

	/**
	 * The buffer is cleared before preparation
	 */
	@Override
	public void prepare(Map<Integer, Integer> input, String jid) {
		clear();
		data.putAll(input);
	}

	@Override
	public Integer getResponse(int id) throws Throwable {
		return data.get(id);
	}

	@Override
	public void clear() {
		data.clear();
	}
}
