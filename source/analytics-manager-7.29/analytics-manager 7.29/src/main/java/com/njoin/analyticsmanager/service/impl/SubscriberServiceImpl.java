package com.njoin.analyticsmanager.service.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.njoin.analyticsmanager.dao.IGenericDao;
import com.njoin.analyticsmanager.entity.Subscriber;
import com.njoin.analyticsmanager.service.ISubscriberService;

/**
 * An Implementation of ISubscriberService
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Service
@Transactional
public class SubscriberServiceImpl implements ISubscriberService {

	@Autowired
	private IGenericDao<Subscriber> subscriberDao;

	@Override
	public List<Subscriber> getSubscribers() {
		return subscriberDao.getAll();
	}

	@Override
	@Transactional
	public String saveSubscriber(Subscriber subscriber) {
		if (subscriber.getAddress() == null || subscriber.getName() == null || subscriber.getSubjectTemplate() == null
				|| subscriber.getBodyTemplate() == null) {
			throw new IllegalArgumentException(
					"Parameters 'name', 'address', 'subjectTemplate', and 'bodyTemplate' cannot be null");
		}
		subscriberDao.saveOrUpdate(subscriber);
		return "Subscriber saved";
	}

	@Override
	public String deleteSubscriber(int id) {
		Subscriber subscriber = subscriberDao.getById(id);
		if (subscriber == null) {
			throw new IllegalArgumentException("Subscriber does not exist");
		}
		subscriberDao.delete(subscriber);
		return "Subscriber deleted";
	}

	@Override
	public Map<Integer, List<Subscriber>> getSubscriberMap() {
		List<Subscriber> subscribers = subscriberDao.getAll();
		Map<Integer, List<Subscriber>> map = new HashMap<>(2);
		map.put(1, new LinkedList<Subscriber>());
		map.put(2, new LinkedList<Subscriber>());
		for (Subscriber subscriber : subscribers) {
			if (subscriber.getLevel() > 0) {
				map.get(1).add(subscriber);
			}
			if (subscriber.getLevel() > 1) {
				map.get(2).add(subscriber);
			}
		}
		return map;
	}
}
