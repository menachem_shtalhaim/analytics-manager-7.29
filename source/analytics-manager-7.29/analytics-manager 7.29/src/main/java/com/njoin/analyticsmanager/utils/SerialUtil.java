package com.njoin.analyticsmanager.utils;

/**
 * A Utility that creates a unique increasing serial number
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
public class SerialUtil {

	private static int serial = 0;

	/**
	 * Get the next serial number
	 * 
	 * @return the next serial number
	 */
	public static int getSerial() {
		return serial++;
	}

}
