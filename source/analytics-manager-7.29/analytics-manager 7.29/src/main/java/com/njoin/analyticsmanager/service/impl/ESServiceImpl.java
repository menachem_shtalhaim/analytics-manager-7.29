package com.njoin.analyticsmanager.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;
import com.njoin.analyticsmanager.service.IESService;
import com.njoin.analyticsmanager.service.IRequestService;

/**
 * An Implementation of IESService
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Service
@Transactional
public class ESServiceImpl implements IESService{
	
	@Value("${server.es.show.url}")
	private String esShowURL;
	@Value("${server.es.clear.url}")
	private String esClearURL;
	@Value("${server.es.clearInsert.url}")
	private String esClearInsertURL;
	@Value("${server.es.insertUpdate.url}")
	private String esInsertUpdateURL;
	
	@Value("${es.clear.async}")
	private boolean async;
	
	@Autowired
	IRequestService requestService;

	@Override
	public String showES(String jid) throws BackEndServerException, SessionExpiredException {
		return requestService.getJSON(esShowURL, jid, null);
	}

	@Override
	public String clear(String name, String jid) throws BackEndServerException, SessionExpiredException {
		checkNull(name);
		String url = esClearURL + name + "?aync=" + async;
		String result = requestService.postJSON(url, jid, null);
		try {
			return JSONObject.parseObject(result).getString("message");
		} catch(Exception e) {
			throw new IllegalArgumentException("Unable to read server response");
		}
	}

	@Override
	public String clearInsert(String name, String jid) throws BackEndServerException, SessionExpiredException {
		checkNull(name);
		String url = esClearInsertURL + name;
		String result = requestService.postJSON(url, jid, null);
		try {
			return JSONObject.parseObject(result).getString("message");
		} catch(Exception e) {
			throw new IllegalArgumentException("Unable to read server response");
		}
	}

	@Override
	public String insertUpdate(String name, String jid) throws BackEndServerException, SessionExpiredException {
		checkNull(name);
		String url = esInsertUpdateURL + name;
		String result = requestService.postJSON(url, jid, null);
		try {
			return JSONObject.parseObject(result).getString("message");
		} catch(Exception e) {
			throw new IllegalArgumentException("Unable to read server response");
		}
	}
	
	private void checkNull(String name) {
		if (name == null || name.length() == 0) {
			throw new IllegalArgumentException("Parameter 'name' cannot be null or empty");
		}
	}

}
