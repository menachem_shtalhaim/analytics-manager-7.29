package com.njoin.analyticsmanager.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * The root controller of the application
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Controller
public class AppController {

	@GetMapping("/")
	public String login() {
		return "redirect:/analytics/login";
	}

	@GetMapping("/analytics")
	public String mainView() {
		return "/analytics/index.html";
	}
}
