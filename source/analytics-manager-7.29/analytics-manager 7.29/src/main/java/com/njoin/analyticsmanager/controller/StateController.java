package com.njoin.analyticsmanager.controller;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttribute;

import com.njoin.analyticsmanager.buffer.IStateListBuffer;
import com.njoin.analyticsmanager.entity.Res;
import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;
import com.njoin.analyticsmanager.service.IStateService;

/**
 * Rest Controller for managing state registers
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@RestController
public class StateController {

	@Autowired
	private IStateService stateService;
	@Autowired
	private IStateListBuffer stateListBuffer;

	/**
	 * Submit a register state detection request
	 * 
	 * @param start
	 *            the starting time frame of the state detection
	 * @param end
	 *            the ending time frame of the state detection
	 * @param sampleRate
	 *            the sample rate in state detection
	 * @param max
	 *            the maximum number of registers considered in this request
	 * @param jid
	 * @return
	 */
	@GetMapping("/analytics/state/detect")
	public Res detectState(@RequestParam("start") long start, @RequestParam("end") long end,
			@RequestParam("sampleRate") int sampleRate, @RequestParam("max") int max,
			@SessionAttribute("jid") String jid) {
		return new Res(stateService.detectState(start, end, sampleRate, max, jid));
	}

	/**
	 * Show the status of current requests, the status corresponds to whether
	 * the request is done and the result is available in the buffer
	 * 
	 * @return
	 */
	@GetMapping("/analytics/state/requests")
	public Map<Integer, Boolean> getRequests() {
		return stateListBuffer.getStatus();
	}

	/**
	 * Remove a request, the request will be immediately halted
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping("/analytics/state/remove")
	public Res removeRequest(@RequestParam("requestId") int id) {
		return new Res(stateListBuffer.remove(id));
	}

	/**
	 * Download the result of the request as a csv file, the request will be
	 * deleted from the buffer after download
	 * 
	 * @param response
	 * @param id
	 * @throws Throwable
	 */
	@GetMapping("/analytics/state/download")
	public void downloadState(HttpServletResponse response, @RequestParam("requestId") int id) throws Throwable {
		response.setContentType("text/csv;charset=UTF-8");
		response.setHeader("Content-disposition", "attachment;filename=state_registers.csv");
		response.getOutputStream().print(stateListBuffer.getResponse(id));
	}

	/**
	 * Import the states by specifying a path on the back-end server
	 * 
	 * @param filePath
	 *            the path of the state registers file on the back-end server
	 * @param isNew
	 *            whether the state import mode is new, true for new state and
	 *            false for update state
	 * @param jid
	 * @return
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	@GetMapping("/analytics/state/import")
	public Res importState(@RequestParam("filePath") String filePath, @RequestParam("isNew") boolean isNew,
			@SessionAttribute("jid") String jid) throws BackEndServerException, SessionExpiredException {
		return new Res(stateService.importState(filePath, isNew, jid));
	}

}
