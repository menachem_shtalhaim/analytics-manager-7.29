package com.njoin.analyticsmanager.entity;

/**
 * A simulation task that maintains the relevant statistics and status of the
 * task. This is a transient object and is not stored in the database. When
 * used, it should be directly parsed from the back-end server
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
public class SimulationTask {

	// the pattern of the back-end API response of time for simulation tasks
	public static final String PATTERN = "EEE MMM dd HH:mm:ss zzz yyyy";

	private long start;
	private long end;
	private int frequency;
	private long processed;
	private double progress;

	public long getStart() {
		return start;
	}

	public void setStart(long start) {
		this.start = start;
	}

	public long getEnd() {
		return end;
	}

	public void setEnd(long end) {
		this.end = end;
	}

	public int getFrequency() {
		return frequency;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}

	public long getProcessed() {
		return processed;
	}

	public void setProcessed(long processed) {
		this.processed = processed;
	}

	public double getProgress() {
		return progress;
	}

	public void setProgress(double progress) {
		this.progress = progress;
	}
}
