package com.njoin.analyticsmanager.notification;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.njoin.analyticsmanager.entity.ModelReport;
import com.njoin.analyticsmanager.entity.Report;
import com.njoin.analyticsmanager.entity.Subscriber;
import com.njoin.analyticsmanager.service.IEmailService;
import com.njoin.analyticsmanager.service.ILoginService;
import com.njoin.analyticsmanager.service.IReportService;
import com.njoin.analyticsmanager.service.ISubscriberService;

/**
 * A scheduled service that periodically updates and manages the training
 * reports. Whenever the training report status changed, the service sends email
 * to the appropriate subscribers.
 * 
 * It is responsible to enter update report method as managed and archiving the
 * reports. All unarchived reports, once finished, will be delivered to the
 * subscribers
 * 
 * The interval the service is run are configurable in the
 * application.properties
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Service
@Transactional
public class TrainingNotificationService {

	@Value("${server.train.report.url}")
	private String trainReportURL;
	@Value("${server.login.username}")
	private String username;
	@Value("${server.login.password}")
	private String password;

	@Autowired
	private ILoginService loginService;
	@Autowired
	private IEmailService emailService;
	@Autowired
	private ISubscriberService subscriberService;
	@Autowired
	private IReportService reportService;

	/**
	 * A scheduled task that updates and manages the training reports and
	 * delivers the changed status to appropriate subscribers, if any
	 * 
	 * @throws Throwable
	 */
	@Scheduled(fixedRateString = "${subscriber.notification.interval}")
	public void notification() throws Throwable {
		// login before the management process
		String jid = loginService.login(username, password);
		List<Report> reports = reportService.getAllReports(jid);

		Map<Integer, List<Subscriber>> map = subscriberService.getSubscriberMap();

		List<Report> updated = reportService.update(reports, true);
		for (Report report : updated) {
			if (hasError(report)) {
				// sends message to subscriber that accepts error message (level
				// 1 and 2)
				for (Subscriber subscriber : map.get(1)) {
					sendMessage(report, subscriber, 1);
				}
			} else {
				// sends message to subscriber that accepts info message (level
				// 2)
				for (Subscriber subscriber : map.get(2)) {
					sendMessage(report, subscriber, 2);
				}
			}
		}
	}

	/**
	 * Tests whether there is error in the report, in any of the model reports
	 * 
	 * @param report
	 *            the report to be tested
	 * @return true if there is at least one error present, false otherwise
	 */
	private boolean hasError(Report report) {
		boolean result = false;
		for (Map.Entry<String, ModelReport> entry : report.getModelReports().entrySet()) {
			if (entry.getValue().getError() != null) {
				result = true;
			}
		}
		return result;
	}

	/**
	 * Sends the message according to the specified parameters
	 * 
	 * @param report
	 *            the report with status change
	 * @param subscriber
	 *            the subscriber to be notified
	 * @param level
	 *            the level of notification, can be INFO or ERROR
	 */
	private void sendMessage(Report report, Subscriber subscriber, int level) {
		String subjectTemplate = subscriber.getSubjectTemplate();
		String bodyTemplate = subscriber.getBodyTemplate();
		String subject = fillTemplate(report, subjectTemplate, level);
		String body = fillTemplate(report, bodyTemplate, level);
		emailService.send(subscriber.getAddress(), subject, body);
	}

	/**
	 * Fills in the template with the appropriate information, the following
	 * templates can be used:
	 * <p>
	 * <ul>
	 * <li>$id$</li>
	 * <li>$level$</li>
	 * <li>$status$</li>
	 * <li>$error$</li>
	 * </ul>
	 * </p>
	 * 
	 * @param report
	 *            the report with status change
	 * @param string
	 *            the template string to be filled
	 * @param level
	 *            the level of notification, can be INFO or ERROR
	 * @return the filled template
	 */
	private String fillTemplate(Report report, String string, int level) {
		string = string.replace("$id$", report.getAnalyzerId() + "");
		if (level == 1) {
			string = string.replace("$level$", "ERROR");
		} else if (level == 2) {
			string = string.replace("$level$", "INFO");
		}
		string = string.replace("$status$", report.getStatus());
		StringBuilder sb = new StringBuilder();
		for (Map.Entry<String, ModelReport> entry : report.getModelReports().entrySet()) {
			sb.append(entry.getKey());
			sb.append(": ");
			String error = entry.getValue().getError();
			if (error == null) {
				sb.append("no error");
			} else {
				sb.append(error);
			}
			sb.append('\n');
		}
		string = string.replace("$error$", sb.toString());
		return string;
	}
}
