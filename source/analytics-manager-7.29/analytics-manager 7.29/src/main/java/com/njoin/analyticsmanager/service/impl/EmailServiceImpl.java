package com.njoin.analyticsmanager.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.njoin.analyticsmanager.service.IEmailService;

/**
 * An Implementation of IEmailService
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Service
public class EmailServiceImpl implements IEmailService {

	@Autowired
	private JavaMailSender emailSender;

	@Override
	public void send(String to, String subject, String body) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(to);
		message.setSubject(subject);
		message.setText(body);
		emailSender.send(message);
	}
}
