package com.njoin.analyticsmanager.service;

/**
 * A Service that manages Login requests
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
public interface ILoginService {

	/**
	 * Attempt to login to the back-end server
	 * 
	 * @param username
	 * @param password
	 * @return the JSESSIONID cookie from the back-end server, null if the login
	 *         is not successful
	 */
	public String login(String username, String password);

}
