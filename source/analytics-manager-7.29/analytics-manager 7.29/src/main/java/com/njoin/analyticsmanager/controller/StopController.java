package com.njoin.analyticsmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttribute;

import com.njoin.analyticsmanager.entity.Res;
import com.njoin.analyticsmanager.entity.analyzer.Analyzer;
import com.njoin.analyticsmanager.entity.analyzer.DupAnalyzer;
import com.njoin.analyticsmanager.entity.analyzer.KNNAnalyzer;
import com.njoin.analyticsmanager.entity.analyzer.MotifAnalyzer;
import com.njoin.analyticsmanager.entity.analyzer.TargetAnalyzer;
import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;
import com.njoin.analyticsmanager.service.IAnalyzerService;

/**
 * Rest Controller for stopping the analyzers
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@RestController
public class StopController {

	@Autowired
	private IAnalyzerService<KNNAnalyzer> knnService;
	@Autowired
	private IAnalyzerService<MotifAnalyzer> motifService;
	@Autowired
	private IAnalyzerService<DupAnalyzer> dupService;
	@Autowired
	private IAnalyzerService<TargetAnalyzer> targetService;

	/**
	 * Stop the specified analyzer identified by its type and id
	 * 
	 * @param type
	 * @param id
	 * @param jid
	 * @return
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	@GetMapping("/analytics/stop")
	public Res stop(@RequestParam("type") String type, @RequestParam("id") int id, @SessionAttribute("jid") String jid)
			throws BackEndServerException, SessionExpiredException {
		if (type.equals(Analyzer.KNN_TYPE)) {
			return new Res(knnService.stop(id, jid));
		} else if (type.equals(Analyzer.MOTIF_TYPE)) {
			return new Res(motifService.stop(id, jid));
		} else if (type.equals(Analyzer.DUP_TYPE)) {
			return new Res(dupService.stop(id, jid));
		} else if (type.equals(Analyzer.TARGET_TYPE)) {
			return new Res(targetService.stop(id, jid));
		} else {
			throw new IllegalArgumentException("Analyzer type not recognized");
		}
	}
}
