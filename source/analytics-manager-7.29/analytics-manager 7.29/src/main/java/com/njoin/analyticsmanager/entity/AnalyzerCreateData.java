package com.njoin.analyticsmanager.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The entity maintains the data relevant to create the underlying analyzer of
 * the corresponding training task. A analyzer create data corresponds to one
 * and only one training task
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Entity
@Table(name = "create_data")
@JsonIgnoreProperties(value = { "trainingTask" })
public class AnalyzerCreateData {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "task_id")
	private TrainingTask trainingTask;

	// Time Frame
	@Column(name = "sample_rate")
	private int sampleRate;
	@Column(name = "training_days")
	private int trainingDays;
	@Column(name = "upper_time")
	private Long upperTime;

	// Anomaly Time Frame
	@Column(name = "anomaly_sample_rate")
	private int anomalySampleRate;
	@Column(name = "anomaly_training_days")
	private int anomalyTrainingDays;
	@Column(name = "anomaly_upper_time")
	private Long anomalyUpperTime;

	// DB query
	@Column(name = "max_numeric")
	private int maxNumeric;
	@Column(name = "max_non_numeric")
	private int maxNonNumeric;
	@Column(name = "min_numeric")
	private int minNumericValue;
	@Column(name = "min_non_numeric")
	private int minNonNumericValue;
	@Column(name = "devices")
	private String devices;

	// Target
	@Column(name = "target")
	private String targetContent;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public TrainingTask getTrainingTask() {
		return trainingTask;
	}

	public void setTrainingTask(TrainingTask trainingTask) {
		this.trainingTask = trainingTask;
	}

	public int getSampleRate() {
		return sampleRate;
	}

	public void setSampleRate(int sampleRate) {
		this.sampleRate = sampleRate;
	}

	public int getTrainingDays() {
		return trainingDays;
	}

	public void setTrainingDays(int trainingDays) {
		this.trainingDays = trainingDays;
	}

	public Long getUpperTime() {
		return upperTime;
	}

	public void setUpperTime(Long upperTime) {
		this.upperTime = upperTime;
	}

	public int getAnomalySampleRate() {
		return anomalySampleRate;
	}

	public void setAnomalySampleRate(int anomalySampleRate) {
		this.anomalySampleRate = anomalySampleRate;
	}

	public int getAnomalyTrainingDays() {
		return anomalyTrainingDays;
	}

	public void setAnomalyTrainingDays(int anomalyTrainingDays) {
		this.anomalyTrainingDays = anomalyTrainingDays;
	}

	public Long getAnomalyUpperTime() {
		return anomalyUpperTime;
	}

	public void setAnomalyUpperTime(Long anomalyUpperTime) {
		this.anomalyUpperTime = anomalyUpperTime;
	}

	public int getMaxNumeric() {
		return maxNumeric;
	}

	public void setMaxNumeric(int maxNumeric) {
		this.maxNumeric = maxNumeric;
	}

	public int getMaxNonNumeric() {
		return maxNonNumeric;
	}

	public void setMaxNonNumeric(int maxNonNumeric) {
		this.maxNonNumeric = maxNonNumeric;
	}

	public int getMinNumericValue() {
		return minNumericValue;
	}

	public void setMinNumericValue(int minNumericValue) {
		this.minNumericValue = minNumericValue;
	}

	public int getMinNonNumericValue() {
		return minNonNumericValue;
	}

	public void setMinNonNumericValue(int minNonNumericValue) {
		this.minNonNumericValue = minNonNumericValue;
	}

	public String getDevices() {
		return devices;
	}

	public void setDevices(String devices) {
		this.devices = devices;
	}

	public String getTargetContent() {
		return targetContent;
	}

	public void setTargetContent(String targetContent) {
		this.targetContent = targetContent;
	}

}
