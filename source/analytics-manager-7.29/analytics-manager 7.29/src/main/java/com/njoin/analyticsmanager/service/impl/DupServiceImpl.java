package com.njoin.analyticsmanager.service.impl;

import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.njoin.analyticsmanager.entity.DBInput;
import com.njoin.analyticsmanager.entity.analyzer.Analyzer;
import com.njoin.analyticsmanager.entity.analyzer.DupAnalyzer;
import com.njoin.analyticsmanager.exception.AnalyzerCorruptedException;
import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;
import com.njoin.analyticsmanager.utils.DateUtil;

/**
 * An Implementation of Abstract Analyzer Service for the Duplicate analyzer.
 * 
 * Particularly, the creation of the analyzer involves, sequentially, the
 * duplicate anomaly model and the duplicate group induction model
 * 
 * The creation of the analyzer requires the additional (sampleRate,
 * trainingDays, upperTime) set of parameters for the anomaly model, the
 * original set will be used for the group induction model
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Service
@Transactional
public class DupServiceImpl extends AbstractAnalyzerService<DupAnalyzer> {

	private void createSequence(DupAnalyzer analyzer, Map<String, Object> payload, Map<String, Object> anomalyRequest,
			Map<String, Object> giRequest, String jid) throws BackEndServerException, SessionExpiredException {
		String anomalyResult = requestService.postJSON(createModelURL, jid, anomalyRequest);

		int anomalyModelId = processCreateResult(anomalyResult);

		giRequest.put("anomaly_model_modelid", anomalyModelId);
		String giResult = requestService.postJSON(createModelURL, jid, giRequest);
		int giModelId = processCreateResult(giResult);

		int aeId = aeService.createAE(payload, anomalyModelId, "", jid);

		analyzer.setModelId(anomalyModelId);
		analyzer.setGiModelId(giModelId);
		analyzer.setAeId(aeId);
	}

	private void analyzerSpecificPrepareRequest(Map<String, Object> anomalyRequest, Map<String, Object> giRequest,
			Map<String, Object> payload, DupAnalyzer analyzer) {
		anomalyRequest.put("type", "DuplicateRegisterAnomalyModel");
		giRequest.put("type", "DuplicateRegisterGroupInductionModel");
		analyzer.setDisplayType(displayDao.getDisplayInfoByType(Analyzer.DUP_TYPE).getDisplayType());
		analyzer.setType(Analyzer.DUP_TYPE);

		if (payload.get("anomalySampleRate") == null || payload.get("anomalyTrainingDays") == null) {
			throw new IllegalArgumentException(
					"Parameter 'anomalySampleRate' and 'anomalyTrainingDays' is required for this analyzer");
		}
		try {
			int anomalySampleRate = (Integer) payload.get("anomalySampleRate");
			int anomalyTrainingDays = (Integer) payload.get("anomalyTrainingDays");
			anomalyRequest.put("sample_rate", anomalySampleRate);
			anomalyRequest.put("train_time_frame", anomalyTrainingDays);
			analyzer.setAnomalySampleRate(anomalySampleRate);
			analyzer.setAnomalyTrainingDays(anomalyTrainingDays);
			if (payload.get("anomalyUpperTime") != null) {
				long anomalyUpperTime = (Long) payload.get("anomalyUpperTime");
				anomalyRequest.put("top_date", DateUtil.dateLongToString(anomalyUpperTime));
				analyzer.setAnomalyUpperTime(anomalyUpperTime);
			}
		} catch (ClassCastException e) {
			throw new IllegalArgumentException("Illegal input value for this analyzer");
		}
	}

	@Override
	public DupAnalyzer createRequestFromFile(Map<String, Object> payload, String fileContent, String jid)
			throws BackEndServerException, SessionExpiredException {

		DupAnalyzer analyzer = new DupAnalyzer();
		populateAnalyzer(analyzer, payload);

		Map<String, Object> anomalyRequest = prepairModelRequest(analyzer, "anomaly@");
		Map<String, Object> giRequest = prepairModelRequest(analyzer, "gi@");

		analyzerSpecificPrepareRequest(anomalyRequest, giRequest, payload, analyzer);

		populateFileRequest(giRequest, fileContent);

		createSequence(analyzer, payload, anomalyRequest, giRequest, jid);

		return analyzer;
	};

	@Override
	public DupAnalyzer createRequestFromDB(Map<String, Object> payload, DBInput input, String jid)
			throws BackEndServerException, SessionExpiredException {

		DupAnalyzer analyzer = new DupAnalyzer();
		populateAnalyzer(analyzer, payload);

		Map<String, Object> anomalyRequest = prepairModelRequest(analyzer, "anomaly@");
		Map<String, Object> giRequest = prepairModelRequest(analyzer, "gi@");

		analyzerSpecificPrepareRequest(anomalyRequest, giRequest, payload, analyzer);

		populateDBRequest(giRequest, input);

		createSequence(analyzer, payload, anomalyRequest, giRequest, jid);
		return analyzer;
	};

	@Override
	public String delete(int id, String jid) throws BackEndServerException, SessionExpiredException {

		DupAnalyzer analyzer = retriveExist(id);
		requestOperation(analyzer.getAeId(), deleteAEURL, jid);
		analyzer.setArchived(true);
		analyzerDao.saveOrUpdate(analyzer);
		requestOperation(analyzer.getGiModelId(), deleteModelURL, jid);
		requestOperation(analyzer.getModelId(), deleteModelURL, jid);

		return "Analyzer removed";
	}

	@Override
	public String train(int id, Integer batch, String jid) throws BackEndServerException, SessionExpiredException {

		DupAnalyzer analyzer = retriveExist(id);
		trainOperation(analyzer.getGiModelId(), batch, jid);
		return "Model submitted for training";
	}

	@Override
	public String stop(int id, String jid) throws BackEndServerException, SessionExpiredException {

		DupAnalyzer analyzer = retriveExist(id);
		requestOperation(analyzer.getGiModelId(), stopURL, jid);
		return "Model train session abort request submitted";
	}

	@Override
	public String removeSchedule(int id, String jid) throws BackEndServerException, SessionExpiredException {
		DupAnalyzer analyzer = retriveExist(id);
		String result = requestOperation(analyzer.getGiModelId(), removeScheduleURL, jid);
		return parseScheduleResponse(result);
	}

	@Override
	public String suspendSchedule(int id, String jid) throws BackEndServerException, SessionExpiredException {
		DupAnalyzer analyzer = retriveExist(id);
		String result = requestOperation(analyzer.getGiModelId(), suspendScheduleURL, jid);
		return parseScheduleResponse(result);
	}

	@Override
	public String resumeSchedule(int id, String jid) throws BackEndServerException, SessionExpiredException {
		DupAnalyzer analyzer = retriveExist(id);
		String result = requestOperation(analyzer.getGiModelId(), resumeScheduleURL, jid);
		return parseScheduleResponse(result);
	}

	@Override
	public String createSchedule(int id, String cron, String jid)
			throws BackEndServerException, SessionExpiredException {
		DupAnalyzer analyzer = retriveExist(id);
		String result = scheduleOperation(analyzer.getGiModelId(), cron, jid);
		return parseScheduleCreateResponse(result);
	}

	@Override
	public String toggleActive(int id, String jid)
			throws BackEndServerException, SessionExpiredException, AnalyzerCorruptedException {
		DupAnalyzer analyzer = retriveExist(id);
		return aeService.toggleActive(analyzer.getAeId(), analyzer.getId(), jid);
	}

	@Override
	public String toggleVisible(int id, String jid)
			throws BackEndServerException, SessionExpiredException, AnalyzerCorruptedException {
		DupAnalyzer analyzer = retriveExist(id);
		return aeService.toggleVisible(analyzer.getAeId(), analyzer.getId(), jid);
	}
}