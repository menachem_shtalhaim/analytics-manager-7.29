package com.njoin.analyticsmanager.dao;

import java.util.List;

import com.njoin.analyticsmanager.entity.analyzer.Analyzer;

/**
 * An extended generic Dao for analyzers. Distinguishes between archived and
 * unarchived analyzers with retrieval
 * 
 * @author Henry Xing
 * @version 7.29
 * 
 * @param <T>
 *            the type of the analyzer entity
 */
public interface IAnalyzerDao<T extends Analyzer> extends IGenericDao<T> {

	/**
	 * Retrieve all entities of this type that is not archived
	 * 
	 * @return a list of all unarchived managed entities for this analyzer type
	 */
	public List<T> getAllUnArchived();
}
