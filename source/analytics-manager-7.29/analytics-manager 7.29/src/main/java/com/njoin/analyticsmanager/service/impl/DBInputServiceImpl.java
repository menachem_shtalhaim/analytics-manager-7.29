package com.njoin.analyticsmanager.service.impl;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.njoin.analyticsmanager.entity.DBInput;
import com.njoin.analyticsmanager.service.IDBInputService;

/**
 * An Implementation of IDBInputService
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Service
@Transactional
public class DBInputServiceImpl implements IDBInputService{

	@Override
	public DBInput extractDBInput(JSONArray attributes, boolean numeric, boolean nonNumeric) {
		DBInput input = new DBInput();
		for (Object o : attributes) {
			JSONObject item = (JSONObject) o;
			switch (item.getString("name")) {
			case "DEVICES_IDS":
				input.setDevices(item.getString("value"));
				break;
			case "TGI_MAX_REGISTERS":
				if (numeric) {
					input.setMaxNumeric(Integer.parseInt(item.getString("value")));
				}
				break;
			case "NON_NUMERIC_MAX_REGISTERS":
				if (nonNumeric) {
					input.setMaxNonNumeric(Integer.parseInt(item.getString("value")));
				}
				break;
			case "min.values.per.day":
				if (numeric) {
					input.setMinNumericValue(Integer.parseInt(item.getString("value")));
				}
				break;
			case "NON_NUMERIC_MIN_VALUES_PER_DAY":
				if (nonNumeric) {
					input.setMinNonNumericValue(Integer.parseInt(item.getString("value")));
				}
			}
		}
		return input;
	}

}
