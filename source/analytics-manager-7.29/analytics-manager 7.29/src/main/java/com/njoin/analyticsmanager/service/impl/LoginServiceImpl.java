package com.njoin.analyticsmanager.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.njoin.analyticsmanager.service.ILoginService;

/**
 * An Implementation of ILoginService
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Service
@Transactional
public class LoginServiceImpl implements ILoginService{
	
	@Value("${server.login.url}")
	private String url;

	@Override
	public String login(String username, String password) {
		RestTemplate template = new RestTemplate();
		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("username", username);
		map.add("password", password);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		HttpEntity<MultiValueMap<String, String>> payload = new HttpEntity<>(map, headers); 
		try {
			HttpEntity<String> response = template.exchange(url, HttpMethod.POST, payload, String.class);
			String cookie = response.getHeaders().getFirst("Set-Cookie");
			int start = cookie.indexOf("JSESSIONID=");
			int end = cookie.indexOf(';', start);
			String result = cookie.substring(start, end);
			return result;
		} catch (HttpClientErrorException e) {
			return null;
		}
	}
	
}
