package com.njoin.analyticsmanager.dao.impl;

import org.springframework.stereotype.Repository;

import com.njoin.analyticsmanager.dao.AbstractDao;
import com.njoin.analyticsmanager.entity.Subscriber;

/**
 * A concrete implementation of dao for subscriber
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Repository
public class SubscriberDaoImpl extends AbstractDao<Subscriber>{

	@Override
	protected Class<Subscriber> getClazz() {
		return Subscriber.class;
	}
}
