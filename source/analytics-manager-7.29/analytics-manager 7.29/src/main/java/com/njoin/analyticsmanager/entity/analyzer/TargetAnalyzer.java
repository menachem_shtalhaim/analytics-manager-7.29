package com.njoin.analyticsmanager.entity.analyzer;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * A Target Analyzer entity
 * 
 * Encompasses the following components, in the order of creation:
 * <p>
 * <ul>
 * <li>Targeted Anomaly Model</li>
 * <li>Targeted Group Induction Model</li>
 * <li>Analysis Engine for Targeted Anomaly Model</li>
 * </ul>
 * </p>
 * 
 * The analyzer allows a different set of (sampleRate, upperTime, trainingDays)
 * for the anomaly and group induction model
 * 
 * @author Henry Xing
 * @version 7.29
 */
@Entity
@DiscriminatorValue("T")
@Table(name="target_analyzer")
public class TargetAnalyzer extends Analyzer{
	
	@Column(name="model_id")
	private int modelId;
	@Column(name="gi_model_id")
	private int giModelId;
	@Column(name="ae_id")
	private int aeId;
	
	@Column(name="sample_rate_anomaly")
	private int anomalySampleRate;
	@Column(name="training_days_anomaly")
	private int anomalyTrainingDays;
	@Column(name="upper_time_anomaly")
	private Long anomalyUpperTime;
	@Column(name="target_content")
	private String targetContent;
	
	public int getModelId() {
		return modelId;
	}
	public void setModelId(int modelId) {
		this.modelId = modelId;
	}
	public int getGiModelId() {
		return giModelId;
	}
	public void setGiModelId(int giModelId) {
		this.giModelId = giModelId;
	}
	public int getAeId() {
		return aeId;
	}
	public void setAeId(int aeId) {
		this.aeId = aeId;
	}
	public int getAnomalySampleRate() {
		return anomalySampleRate;
	}
	public void setAnomalySampleRate(int anomalySampleRate) {
		this.anomalySampleRate = anomalySampleRate;
	}
	public int getAnomalyTrainingDays() {
		return anomalyTrainingDays;
	}
	public void setAnomalyTrainingDays(int anomalyTrainingDays) {
		this.anomalyTrainingDays = anomalyTrainingDays;
	}
	public Long getAnomalyUpperTime() {
		return anomalyUpperTime;
	}
	public void setAnomalyUpperTime(Long anomalyUpperTime) {
		this.anomalyUpperTime = anomalyUpperTime;
	}
	public String getTargetContent() {
		return targetContent;
	}
	public void setTargetContent(String targetContent) {
		this.targetContent = targetContent;
	}
}