package com.njoin.analyticsmanager.service.impl;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.njoin.analyticsmanager.automatetest.SmartTaskGenerator;
import com.njoin.analyticsmanager.dao.IDisplayDao;
import com.njoin.analyticsmanager.dao.IGenericDao;
import com.njoin.analyticsmanager.entity.AnalyzerCreateData;
import com.njoin.analyticsmanager.entity.TrainingTask;
import com.njoin.analyticsmanager.entity.analyzer.Analyzer;
import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.DummyException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;
import com.njoin.analyticsmanager.service.ITrainingTaskService;

/**
 * An Implementation of ITrainingTaskService
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Service
@Transactional
public class TrainingTaskServiceImpl implements ITrainingTaskService {

	@Autowired
	private IGenericDao<TrainingTask> trainingTaskDao;
	@Autowired
	private SmartTaskGenerator generator;
	@Autowired
	private IDisplayDao displayDao;

	@Override
	public String createTrainingTask(Map<String, Object> payload, String jid)
			throws BackEndServerException, SessionExpiredException {
		if (payload.get("type") == null) {
			throw new IllegalArgumentException("Must specify a 'type' to create task");
		}
		TrainingTask task = new TrainingTask();
		if (payload.get("type").equals(Analyzer.MOTIF_TYPE) || payload.get("type").equals(Analyzer.KNN_TYPE)
				|| payload.get("type").equals(Analyzer.DUP_TYPE) || payload.get("type").equals(Analyzer.TARGET_TYPE)) {
			AnalyzerCreateData data = extractCreateData(payload);
			data.setTrainingTask(task);
			task.setAnalyzerCreateData(data);
			task.setStatus(0);

			try {
				task.setTemporary((Boolean) payload.get("temporary"));
				task.setAnalyzerType((String) payload.get("type"));
			} catch (Exception e) {
				throw new IllegalArgumentException("Illegal input for creating task");
			}

			trainingTaskDao.saveOrUpdate(task);
			return "Training task created";
		} else {
			throw new IllegalArgumentException("Analyzer type not recognized");
		}
	}

	private AnalyzerCreateData extractCreateData(Map<String, Object> payload) {
		AnalyzerCreateData data = new AnalyzerCreateData();
		try {
			data.setSampleRate((Integer) payload.get("sampleRate"));
			data.setUpperTime((Long) payload.get("upperTime"));
			data.setTrainingDays((Integer) payload.get("trainingDays"));
			if (payload.get("type").equals(Analyzer.DUP_TYPE) || payload.get("type").equals(Analyzer.TARGET_TYPE)) {
				data.setAnomalySampleRate((Integer) payload.get("anomalySampleRate"));
				data.setAnomalyUpperTime((Long) payload.get("anomalyUpperTime"));
				data.setAnomalyTrainingDays((Integer) payload.get("anomalyTrainingDays"));
			}
			if (payload.get("type").equals(Analyzer.TARGET_TYPE)) {
				if (payload.get("targetContent") == null) {
					throw new DummyException();
				}
				data.setTargetContent((String) payload.get("targetContent"));
			}

			data.setMaxNumeric((Integer) payload.get("maxNumeric"));
			data.setMaxNonNumeric((Integer) payload.get("maxNonNumeric"));
			data.setMinNumericValue((Integer) payload.get("minNumericValue"));
			data.setMinNonNumericValue((Integer) payload.get("minNonNumericValue"));
			data.setDevices((String) payload.get("devices"));
		} catch (Exception e) {
			throw new IllegalArgumentException("Illegal input for creating task");
		}
		return data;
	}

	@Override
	public String deleteTrainingTask(int id) {
		TrainingTask task = trainingTaskDao.getById(id);
		if (task == null) {
			throw new IllegalArgumentException("Training task not found");
		}
		if (task.getStatus() == 1) {
			throw new IllegalArgumentException("Running tasks cannot be deleted");
		}
		trainingTaskDao.delete(task);
		return "Training task deleted";
	}

	@Override
	public List<TrainingTask> showTrainingTasks() {
		List<TrainingTask> list = trainingTaskDao.getAll();
		for (TrainingTask task : list) {
			task.setDisplayType(displayDao.getDisplayInfoByType(task.getAnalyzerType()).getDisplayType());
		}
		return list;

	}

	@Override
	public String smartGenerate(Map<String, Object> payload, String jid)
			throws BackEndServerException, SessionExpiredException {
		int count = generator.generate(payload, jid);
		return count + " tasks generated";
	}

}
