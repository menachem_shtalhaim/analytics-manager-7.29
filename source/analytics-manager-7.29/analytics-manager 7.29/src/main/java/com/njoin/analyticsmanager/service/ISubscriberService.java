package com.njoin.analyticsmanager.service;

import java.util.List;
import java.util.Map;

import com.njoin.analyticsmanager.entity.Subscriber;

/**
 * A Service that manages the subscribers of the system for analyzer training
 * notifications
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
public interface ISubscriberService {

	/**
	 * Retrieve all the subscribers information in the database
	 * 
	 * @return a list of all saved subscribers
	 */
	public List<Subscriber> getSubscribers();

	/**
	 * Save a subscriber to the database, this can be creating a new subscriber
	 * or updating an existing subscriber with the specified id
	 * 
	 * @param subscriber
	 *            to be created or updated
	 * @return the saving success message
	 */
	public String saveSubscriber(Subscriber subscriber);

	/**
	 * Delete a subscriber from the database with the specified id
	 * 
	 * @param id
	 * @return the deletion success message
	 */
	public String deleteSubscriber(int id);

	/**
	 * Map all the subscribers in the database to the type of notification level
	 * that they can receive
	 * 
	 * Subscribers that does not receive any notification will be ignored
	 * 
	 * At the current stage, only two levels will be specified:
	 * <p>
	 * <ul>
	 * <li>1: all ERROR messages, corresponding to subscribers with level 1 and
	 * 2</li>
	 * <li>2: all INFO messages, corresponding to subscribers with level 2</li>
	 * </ul>
	 * </p>
	 * 
	 * @return all the subscribers paired with the level of notification they
	 *         can accept
	 */
	public Map<Integer, List<Subscriber>> getSubscriberMap();

}
