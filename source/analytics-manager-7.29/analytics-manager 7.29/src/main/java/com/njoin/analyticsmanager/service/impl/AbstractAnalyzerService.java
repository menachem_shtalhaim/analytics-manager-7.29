package com.njoin.analyticsmanager.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.njoin.analyticsmanager.dao.IDisplayDao;
import com.njoin.analyticsmanager.dao.IGenericDao;
import com.njoin.analyticsmanager.entity.DBInput;
import com.njoin.analyticsmanager.entity.Report;
import com.njoin.analyticsmanager.entity.analyzer.Analyzer;
import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.DummyException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;
import com.njoin.analyticsmanager.service.IAEService;
import com.njoin.analyticsmanager.service.IAnalyzerReportService;
import com.njoin.analyticsmanager.service.IAnalyzerService;
import com.njoin.analyticsmanager.service.IRequestService;
import com.njoin.analyticsmanager.utils.DateUtil;

/**
 * An Abstract implementation for the analyzer service, extracts the common
 * operations for each type of analyzer. It provides a templatized
 * implementation of the methods specified in IAnalyzerService and delegates the
 * type-specific tasks to the concrete implementations through protected
 * abstract method declarations
 * 
 * This class also maintains all the relevant request urls and relevant general
 * usage services that all concrete implementations will use
 * 
 * When parsing data from the payload, this class checks for user inputs, either
 * in terms of unprovided fields or unexpected data types, it throws
 * IllegalArgumentException whenever necessary. The implementation is robust
 * against mistyped input
 * 
 * @author Henry Xing
 * @version 7.29
 *
 * @param <T>
 *            the type of the Analyzer
 */
@Service
@Transactional
public abstract class AbstractAnalyzerService<T extends Analyzer> implements IAnalyzerService<T> {

	@Autowired
	protected IGenericDao<T> analyzerDao;
	@Autowired
	protected IDisplayDao displayDao;

	@Autowired
	protected IRequestService requestService;
	@Autowired
	protected IAEService aeService;
	@Autowired
	protected IAnalyzerReportService<T> reportService;

	@Value("${server.create.model.url}")
	protected String createModelURL;
	@Value("${server.delete.model.url}")
	protected String deleteModelURL;
	@Value("${server.delete.ae.url}")
	protected String deleteAEURL;
	@Value("${server.stop.url}")
	protected String stopURL;
	@Value("${server.train.url}")
	protected String trainURL;

	@Value("${server.schedule.remove.url}")
	protected String removeScheduleURL;
	@Value("${server.schedule.suspend.url}")
	protected String suspendScheduleURL;
	@Value("${server.schedule.resume.url}")
	protected String resumeScheduleURL;
	@Value("${server.schedule.create.url}")
	protected String createScheduleURL;

	@Override
	public T create(Map<String, Object> payload, String jid) throws BackEndServerException, SessionExpiredException {

		if (payload.get("name") == null || payload.get("description") == null || payload.get("sampleRate") == null
				|| payload.get("trainingDays") == null || payload.get("dataSource") == null
				|| payload.get("active") == null) {
			throw new IllegalArgumentException(
					"Parameters 'name', 'description', 'sampleRate', 'trainingDays', 'dataSource', and 'active' are required for this analyzer");
		}

		T analyzer;

		if (payload.get("dataSource").equals("db")) {
			analyzer = createFromDB(payload, jid);
		} else if (payload.get("dataSource").equals("file")) {
			analyzer = createFromFile(payload, jid);
		} else {
			throw new IllegalArgumentException("Parameter 'dataSource' must be 'db' or 'file'");
		}
		// the newly created analyzer is always unarchived
		analyzer.setArchived(false);

		return analyzerDao.saveOrUpdate(analyzer);
	}

	/**
	 * Encapsulates the analyzer get request to the back-end for payloads with a
	 * single parameter id, extracts the common logic
	 * 
	 * @param id
	 *            the id as specified in the request to back-end
	 * @param url
	 *            the request url to the back-end server
	 * @param jid
	 * @return the back-end response body as a String
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	protected String requestOperation(int id, String url, String jid)
			throws BackEndServerException, SessionExpiredException {
		Map<String, Object> request = new HashMap<>();
		request.put("id", id);
		return requestService.getJSON(url, jid, request);
	}

	/**
	 * Encapsulates the operation of training an analyzer, specified by the
	 * model id and the batch size
	 * 
	 * @param id
	 *            the id of the model to be trained
	 * @param batch
	 *            the batch size for training
	 * @param jid
	 * @return the back-end response body as a String
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	protected String trainOperation(int id, int batch, String jid)
			throws BackEndServerException, SessionExpiredException {
		Map<String, Object> map = new HashMap<>();
		map.put("id", id);
		map.put("batch", batch);
		return requestService.getJSON(trainURL, jid, map);
	}

	/**
	 * Encapsulates the operation of creating a scheduling an analyzer,
	 * specified by the model id and the batch size
	 * 
	 * @param id
	 *            the id of the model to be trained
	 * @param cron
	 *            the cron string of the scheduling
	 * @param jid
	 * @return the back-end response body as a String
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	protected String scheduleOperation(int id, String cron, String jid)
			throws BackEndServerException, SessionExpiredException {
		Map<String, Object> map = new HashMap<>();
		map.put("id", id);
		map.put("cron", cron);
		return requestService.getJSON(createScheduleURL, jid, map);
	}

	/**
	 * Parse the server json response of the scheduling actions, returns the
	 * server message upon success
	 * 
	 * @param result
	 *            the back-end server response body
	 * @return the back-end server response message if the operation succeeds
	 * @throws BackEndServerException
	 */
	protected String parseScheduleResponse(String result) throws BackEndServerException {
		try {
			JSONObject json = JSONObject.parseObject(result);
			return json.getString("message");
		} catch (Exception e) {
			throw new BackEndServerException("Unable to read server response");
		}
	}

	/**
	 * Parse the server json response of creating a new scheduling, examines
	 * whether the schedule is created successfully
	 * 
	 * @param result
	 *            the back-end server response body
	 * @return the creation success message
	 * @throws BackEndServerException
	 */
	protected String parseScheduleCreateResponse(String result) throws BackEndServerException {
		try {
			JSONObject json = JSONObject.parseObject(result);
			if (json.containsKey("chronId")) {
				return "Schedule created";
			} else {
				throw new IllegalArgumentException(json.getString("message"));
			}
		} catch (IllegalArgumentException e) {
			throw e;
		} catch (Exception e) {
			throw new BackEndServerException("Unable to read server response");
		}
	}

	/**
	 * Create the analyzer from database query, populates the relevant database
	 * query fields and delegates the specific creation sequence to the concrete
	 * implementations
	 * 
	 * @param payload
	 * @param jid
	 * @return the created analyzer entity that has not been persisted
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	protected T createFromDB(Map<String, Object> payload, String jid)
			throws BackEndServerException, SessionExpiredException {
		if (payload.get("maxNumeric") == null || payload.get("maxNonNumeric") == null
				|| payload.get("minNumericValue") == null || payload.get("minNonNumericValue") == null) {
			throw new IllegalArgumentException(
					"maxNumeric, maxNonNumeric, minNumeric, minNonNumeric must not be empty");
		}
		DBInput input = new DBInput();
		try {
			input.setMaxNumeric((Integer) payload.get("maxNumeric"));
			input.setMaxNonNumeric((Integer) payload.get("maxNonNumeric"));
			input.setMinNumericValue((Integer) payload.get("minNumericValue"));
			input.setMinNonNumericValue((Integer) payload.get("minNonNumericValue"));
		} catch (ClassCastException e) {
			throw new IllegalArgumentException(
					"maxNumeric, maxNonNumeric, minNumeric, minNonNumeric must be valid integer values");
		}

		try {
			String devices = (String) payload.get("devices");
			input.setDevices(devices);
		} catch (ClassCastException e) {
			throw new IllegalArgumentException("Illegal value for devices");
		}

		T analyzer = createRequestFromDB(payload, input, jid);
		analyzer.setDbInput(input);
		input.setAnalyzer(analyzer);
		return analyzer;

	}

	/**
	 * Create the analyzer from uploaded file, populates the relevant file
	 * content field and delegates the specific creation sequence to the
	 * concrete implementations
	 * 
	 * @param payload
	 * @param jid
	 * @return the created analyzer entity that has not been persisted
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	protected T createFromFile(Map<String, Object> payload, String jid)
			throws BackEndServerException, SessionExpiredException {
		if (payload.get("fileContent") == null) {
			throw new IllegalArgumentException("File content must not be empty");
		}
		String fileContent = "";
		try {
			fileContent = (String) payload.get("fileContent");
		} catch (ClassCastException e) {
			throw new IllegalArgumentException("Illegal value for fileContent");
		}

		T analyzer = createRequestFromFile(payload, fileContent, jid);

		return analyzer;
	}

	/**
	 * Populates the common fields of the abstract analyzer entity
	 * 
	 * @param analyzer
	 *            the analyzer to be populated
	 * @param payload
	 *            the client request payload for creating the analyzer
	 */
	protected void populateAnalyzer(T analyzer, Map<String, Object> payload) {
		try {
			analyzer.setName((String) payload.get("name"));
			analyzer.setDescription((String) payload.get("description"));
			analyzer.setSampleRate((Integer) payload.get("sampleRate"));
			analyzer.setUpperTime((Long) payload.get("upperTime"));
			analyzer.setTrainingDays((Integer) payload.get("trainingDays"));
		} catch (ClassCastException e) {
			throw new IllegalArgumentException("Illegal input value for this analyzer");
		}
	}

	/**
	 * Prepare the common fields of the request payload of a model creation in
	 * the back-end server, retrives the data from the specified analyzer
	 * 
	 * @param analyzer
	 *            the analyzer containing the relevant information of the
	 *            analyzer creation
	 * @param prefix
	 *            the prefix to the name and description of the model for
	 *            identification of components
	 * @return the prepared request
	 */
	protected Map<String, Object> prepairModelRequest(T analyzer, String prefix) {
		Map<String, Object> map = new HashMap<>();
		map.put("name", prefix + analyzer.getName());
		map.put("description", prefix + analyzer.getDescription());
		map.put("sample_rate", analyzer.getSampleRate());
		map.put("train_time_frame", analyzer.getTrainingDays());
		Long upperTime = analyzer.getUpperTime();
		if (upperTime != null) {
			map.put("top_date", DateUtil.dateLongToString(upperTime));
		}
		return map;
	}

	/**
	 * Parse the back-end server response of the model creation request and
	 * extracts the id of the newly created model upon success
	 * 
	 * @param result
	 *            the back-end server response body of the model creation
	 *            process
	 * @return the id of the created model, if any
	 * @throws BackEndServerException
	 */
	protected int processCreateResult(String result) throws BackEndServerException {
		int modelId;
		try {
			JSONObject json = JSON.parseObject(result);
			if (json.getIntValue("status") != 0) {
				throw new DummyException();
			}
			modelId = json.getIntValue("modelId");
		} catch (Exception e) {
			throw new BackEndServerException("Analyzer cannot be created");
		}
		return modelId;
	}

	/**
	 * Add the fields of a file request to the payload according to the
	 * specified file content
	 * 
	 * @param request
	 *            the raw request to the back-end server
	 * @param fileContent
	 *            the file content as uploaded by the client
	 */
	protected void populateFileRequest(Map<String, Object> request, String fileContent) {
		request.put("training_set_source", "LIST_IN_FILE");
		request.put("registers_list_file_content", fileContent);
	}

	/**
	 * Add the fields of a databae query request to the payload according to the
	 * specified dbinput entity
	 * 
	 * @param request
	 *            the raw request to the back-end server
	 * @param input
	 *            the dbinput entity that contains the relevant information for
	 *            an analyzer creation from database
	 */
	protected void populateDBRequest(Map<String, Object> request, DBInput input) {
		request.put("training_set_source", "DB_QUERY");
		request.put("max_registers", input.getMaxNumeric());
		request.put("boolean_max_registers", input.getMaxNonNumeric());
		request.put("min_values", input.getMinNumericValue());
		request.put("boolean_min_registers_per_day", input.getMinNonNumericValue());
		if (input.getDevices() != null) {
			request.put("devices", input.getDevices());
		}
	}

	/**
	 * Retrieve the analyzer from the database according to the specified id,
	 * throws an exception if the analyzer foes not exist or has already been
	 * archived
	 * 
	 * @param id
	 *            the id of the analyzer to retrieve
	 * @return the recrived analyzer if exists
	 */
	protected T retriveExist(int id) {
		T analyzer = analyzerDao.getById(id);
		if (analyzer == null || analyzer.isArchived()) {
			throw new IllegalArgumentException("The analyzer does not exist");
		}
		return analyzer;
	}

	@Override
	public List<Report> getAllReports(int id, String jid) throws Throwable {
		T analyzer = retriveExist(id);
		return reportService.fetchReports(analyzer, jid, true);
	}

	/**
	 * prepares, sends, and parses the analyzer creation request to the back-end
	 * server using the client uploaded file content. This method should be
	 * implemented by the specific concrete implementation of different analyzer
	 * types, the relevant helper methods for the creation is provided in this
	 * class as protected methods
	 * 
	 * @param payload
	 *            the request payload from the client
	 * @param fileContent
	 *            the file content as uplaoded by the client
	 * @param jid
	 * @return the created analyzer entity, not yet persisted
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	protected abstract T createRequestFromFile(Map<String, Object> payload, String fileContent, String jid)
			throws BackEndServerException, SessionExpiredException;

	/**
	 * prepares, sends, and parses the analyzer creation request to the back-end
	 * server using the database query mode. This method should be implemented
	 * by the specific concrete implementation of different analyzer types, the
	 * relevant helper methods for the creation is provided in this class as
	 * protected methods
	 * 
	 * @param payload
	 *            the request payload from the client
	 * @param input
	 *            the parsed dbinput entity extracted and prepared according to
	 *            the client request payload
	 * @param jid
	 * @return the created analyzer entity, not yet persisted
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	protected abstract T createRequestFromDB(Map<String, Object> payload, DBInput input, String jid)
			throws BackEndServerException, SessionExpiredException;

}
