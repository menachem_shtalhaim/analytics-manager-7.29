package com.njoin.analyticsmanager.buffer;

/**
 * A buffer that stores group induction model id paired with the number of
 * groups found by the model temporarily.
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
public interface IGroupCountBuffer extends IGenericBuffer<Integer, Integer>{}
