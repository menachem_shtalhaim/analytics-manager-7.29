package com.njoin.analyticsmanager.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.njoin.analyticsmanager.dao.IDisplayDao;
import com.njoin.analyticsmanager.entity.DisplayInfo;
import com.njoin.analyticsmanager.service.IDisplayService;

/**
 * An Implementation of IDisplayService
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Service
@Transactional
public class DisplayServiceImpl implements IDisplayService{
	
	@Autowired
	private IDisplayDao displayDao;

	@Override
	public List<DisplayInfo> getAllDisplayInfos() {
		return displayDao.getAll();
	}

}
