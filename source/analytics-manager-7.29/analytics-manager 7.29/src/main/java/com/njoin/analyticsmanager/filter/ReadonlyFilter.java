package com.njoin.analyticsmanager.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.njoin.analyticsmanager.configuration.BlockedReadonly;

/**
 * A filter that intercepts illegal client requests in a readonly mode, only
 * active in the readonly mode, as configured in aplication.properties
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Component
@Order(2)
public class ReadonlyFilter implements Filter {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReadonlyFilter.class);

	@Value("${mode.readonly}")
	private boolean readonly;
	@Autowired
	private BlockedReadonly blockedReadonly;

	@Override
	public void destroy() {
	}

	/**
	 * Intercepts the client request and sends error message when the requested
	 * method and url combination is forbidden in the readonly mode. The
	 * forbidden combinations are specified in the application.properties
	 */
	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		if (readonly) {
			HttpServletRequest httpRequest = (HttpServletRequest) req;
			HttpServletResponse httpResponse = (HttpServletResponse) res;
			String uri = httpRequest.getRequestURI();
			String method = httpRequest.getMethod();
			if (isFiltered(method, uri)) {
				LOGGER.info("Request denied for readonly mode: " + method + " " + uri);
				httpResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);
				httpResponse.getWriter().print("{\"message\":\"This function is readonly\"}");
				return;
			}
		}

		chain.doFilter(req, res);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

	/**
	 * Tests whether the specified http method and url combination is blocked
	 * 
	 * @param method
	 * @param uri
	 * @return
	 */
	private boolean isFiltered(String method, String uri) {
		for (String pattern : blockedReadonly.getBlocked().get(method)) {
			if (uri.startsWith(pattern)) {
				return true;
			}
		}
		return false;
	}

}
