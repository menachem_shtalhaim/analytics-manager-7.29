package com.njoin.analyticsmanager.configuration;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

/**
 * The base configuration for the application 
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Configuration
@EnableAspectJAutoProxy
@EnableConfigurationProperties(BlockedReadonly.class)
@Component
public class AppConfig {
}  