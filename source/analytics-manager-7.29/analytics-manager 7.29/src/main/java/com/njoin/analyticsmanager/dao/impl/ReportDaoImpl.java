package com.njoin.analyticsmanager.dao.impl;

import org.springframework.stereotype.Repository;

import com.njoin.analyticsmanager.dao.AbstractDao;
import com.njoin.analyticsmanager.entity.Report;

/**
 * A concrete implementation of dao for reports
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Repository
public class ReportDaoImpl extends AbstractDao<Report>{

	@Override
	protected Class<Report> getClazz() {
		return Report.class;
	}

}
