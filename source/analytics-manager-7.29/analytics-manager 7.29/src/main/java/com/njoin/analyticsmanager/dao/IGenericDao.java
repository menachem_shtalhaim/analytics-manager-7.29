package com.njoin.analyticsmanager.dao;

import java.util.List;

/**
 * A Generic DAO interface that supports the basic CRUD operations
 * 
 * @author Henry Xing
 * @version 7.29
 *
 * @param <T>
 *            the class of the entity
 */
public interface IGenericDao<T> {

	/**
	 * Get the persistent entity by id
	 * 
	 * @param id
	 *            the id of the entity
	 * @return a managed entity from the persistent context if the id exists or
	 *         null
	 */
	public T getById(int id);

	/**
	 * Delete the specified entity
	 * 
	 * @param t
	 *            the managed entity to be deleted
	 */
	public void delete(T t);

	/**
	 * Retrieve all entities of this type
	 * 
	 * @return a list of all managed entities for this type
	 */
	public List<T> getAll();

	/**
	 * Save a entity id does not exist, update the original copy if it is
	 * 
	 * @param t
	 *            the entity to be persisted
	 * @return the managed entity that's saved in the persistent context
	 */
	public T saveOrUpdate(T t);
}
