package com.njoin.analyticsmanager.buffer;

import java.util.concurrent.Callable;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.service.IRequestService;

/**
 * Callable for query of register list. It transforms the back-end server
 * response to a csv format
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
public class RegisterCallable implements Callable<String> {

	private String jid;
	private JSONArray jsonArray;
	private String url;
	private IRequestService requestService;

	public RegisterCallable(String jid, JSONArray jsonArray, String url, IRequestService requestService) {
		super();
		this.jid = jid;
		this.jsonArray = jsonArray;
		this.url = url;
		this.requestService = requestService;
	}

	@Override
	public String call() throws Exception {
		String result = requestService.postJSON(url, jid, jsonArray);
		return parseResult(result);
	}

	/**
	 * Parse the result to a csv formatted String
	 * 
	 * @param result
	 *            the json string from back-end query of register information
	 * @return the csv String
	 * @throws BackEndServerException
	 */
	private String parseResult(String result) throws BackEndServerException {
		StringBuilder sb = new StringBuilder("#,Register ID,Name,Device,Data Block,Type,Counter\n");
		try {
			JSONArray array = JSONObject.parseObject(result).getJSONArray("su");
			for (Object object : array) {
				JSONObject jsonObject = (JSONObject) object;
				sb.append(jsonObject.get("ordinal"));
				sb.append(',');
				sb.append(jsonObject.get("id"));
				sb.append(',');
				sb.append(jsonObject.get("name"));
				sb.append(',');
				sb.append(jsonObject.get("device"));
				sb.append(',');
				sb.append(jsonObject.get("DataBlock"));
				sb.append(',');
				sb.append(jsonObject.get("type"));
				sb.append(',');
				sb.append(jsonObject.get("counter"));
				sb.append('\n');
			}
		} catch (Exception e) {
			throw new BackEndServerException("Unable to read server response");
		}
		return sb.toString();
	}

}
