package com.njoin.analyticsmanager.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.njoin.analyticsmanager.configuration.BlockedReadonly;

/**
 * Rest Controller for displaying the blocked urls and corresponding http method
 * for the current session
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@RestController
public class BlockedController {

	@Autowired
	private BlockedReadonly blockedReadonly;
	@Value("${mode.readonly}")
	private boolean readonly;

	/**
	 * Return a map of http methods corresponding with the list of blocked urls.
	 * Returns an empty map unless the user is in readonly mode
	 * 
	 * @return
	 */
	@GetMapping("/blocked")
	public Map<String, List<String>> getBlocked() {
		if (readonly) {
			return blockedReadonly.getBlocked();
		} else {
			return new HashMap<>();
		}
	}

}
