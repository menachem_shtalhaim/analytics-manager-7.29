package com.njoin.analyticsmanager.dao;

import com.njoin.analyticsmanager.entity.DisplayInfo;

/**
 * An extended generic Dao for display info. enables retrieval through type
 * 
 * @author Henry Xing
 * @version 7.29
 */
public interface IDisplayDao extends IGenericDao<DisplayInfo> {

	/**
	 * Get the persistent display info by its type
	 * 
	 * @param type
	 *            the internal type of the analyzer display info
	 * @return a managed entity from the persistent context if the type exists
	 *         or null
	 */
	public DisplayInfo getDisplayInfoByType(String type);
}
