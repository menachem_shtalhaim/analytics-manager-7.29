package com.njoin.analyticsmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * The Spring Boot Application starting point.
 * 
 * Uncomment the @PropertySource annotation when deploying on the server to
 * externalize the properties file
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@SuppressWarnings("unused")
@SpringBootApplication
@PropertySource(value = {"file:${app.root}/config/application.properties"})
@EnableScheduling
public class AnalyticsManagerApplication {

	public static void main(String[] args) {
		System.setProperty("https.protocols", "TLSv1.2,TLSv1.1,TLSv1");
		SpringApplication.run(AnalyticsManagerApplication.class, args);
	}
}
