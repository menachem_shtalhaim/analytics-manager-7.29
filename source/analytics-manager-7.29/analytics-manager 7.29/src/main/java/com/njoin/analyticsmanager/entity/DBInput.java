package com.njoin.analyticsmanager.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.njoin.analyticsmanager.entity.analyzer.Analyzer;

/**
 * The entity maintains the information of the creation of an existing analyzer.
 * It stores the relevant parameters used to perform the database query. This
 * should be saved whenever a new analyzer is created. A dbinput corresponds to
 * one and only one training task
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Entity
@Table(name = "db_input")
@JsonIgnoreProperties(value = { "analyzer" })
public class DBInput {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	@Column(name = "max_numeric")
	private Integer maxNumeric;
	@Column(name = "max_non_numeric")
	private Integer maxNonNumeric;
	@Column(name = "min_numeric")
	private Integer minNumericValue;
	@Column(name = "min_non_numeric")
	private Integer minNonNumericValue;
	@Column(name = "devices")
	private String devices;
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "analyzer_id")
	private Analyzer analyzer;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getMaxNumeric() {
		return maxNumeric;
	}

	public void setMaxNumeric(Integer maxNumeric) {
		this.maxNumeric = maxNumeric;
	}

	public Integer getMaxNonNumeric() {
		return maxNonNumeric;
	}

	public void setMaxNonNumeric(Integer maxNonNumeric) {
		this.maxNonNumeric = maxNonNumeric;
	}

	public Integer getMinNumericValue() {
		return minNumericValue;
	}

	public void setMinNumericValue(Integer minNumericValue) {
		this.minNumericValue = minNumericValue;
	}

	public Integer getMinNonNumericValue() {
		return minNonNumericValue;
	}

	public void setMinNonNumericValue(Integer minNonNumericValue) {
		this.minNonNumericValue = minNonNumericValue;
	}

	public String getDevices() {
		return devices;
	}

	public void setDevices(String devices) {
		this.devices = devices;
	}

	public Analyzer getAnalyzer() {
		return analyzer;
	}

	public void setAnalyzer(Analyzer analyzer) {
		this.analyzer = analyzer;
	}
}
