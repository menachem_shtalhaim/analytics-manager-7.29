package com.njoin.analyticsmanager.controller;

import java.rmi.ServerException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttribute;

import com.njoin.analyticsmanager.entity.Res;
import com.njoin.analyticsmanager.entity.analyzer.Analyzer;
import com.njoin.analyticsmanager.entity.analyzer.DupAnalyzer;
import com.njoin.analyticsmanager.entity.analyzer.KNNAnalyzer;
import com.njoin.analyticsmanager.entity.analyzer.MotifAnalyzer;
import com.njoin.analyticsmanager.entity.analyzer.TargetAnalyzer;
import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;
import com.njoin.analyticsmanager.service.IAnalyzerService;

/**
 * Rest Controller for the creation of analyzers
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@RestController
public class CreateController {

	@Autowired
	private IAnalyzerService<KNNAnalyzer> knnService;
	@Autowired
	private IAnalyzerService<MotifAnalyzer> motifService;
	@Autowired
	private IAnalyzerService<DupAnalyzer> dupService;
	@Autowired
	private IAnalyzerService<TargetAnalyzer> targetService;

	/**
	 * Creates the analyzer based on the payload
	 * 
	 * The following parameters are required:
	 * <p>
	 * <ul>
	 * <li>type</li>
	 * <li>name</li>
	 * <li>description</li>
	 * <li>sampleRate</li>
	 * <li>trainingDays</li>
	 * <li>anomalySampleRate: for analyzers support 2 sets of params</li>
	 * <li>anomalyTrainingDays: for analyzers support 2 sets of params</li>
	 * <li>dataSource: 'db' or 'file'</li>
	 * <li>maxNumeric: for db</li>
	 * <li>maxNonNumeric: for db</li>
	 * <li>minNumericValue: for db</li>
	 * <li>minNonNumericValue: for db</li>
	 * <li>fileContent: for 'file'</li>
	 * <li>targetContent: for 'target analyzer'</li>
	 * </ul>
	 * </p>
	 * 
	 * The following parameters are optional:
	 * <p>
	 * <ul>
	 * <li>devices</li>
	 * <li>upperTime</li>
	 * <li>anomalyUpperTime</li>
	 * <li>visible</li>
	 * <li>emailAddress</li>
	 * <li>emailSubject</li>
	 * <li>emailBody</li>
	 * <li>suppress</li>
	 * </ul>
	 * </p>
	 * 
	 * @param payload
	 * @param jid
	 * @return
	 * @throws ServerException
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	@PostMapping("/analytics/create")
	public Res create(@RequestBody Map<String, Object> payload, @SessionAttribute("jid") String jid)
			throws ServerException, BackEndServerException, SessionExpiredException {
		if (payload.get("type") == null) {
			throw new IllegalArgumentException("Must specify a 'type' to create analyzer");
		}
		if (payload.get("type").equals(Analyzer.KNN_TYPE)) {
			return new Res("Analyzer created (id = " + knnService.create(payload, jid).getId() + ")");
		} else if (payload.get("type").equals(Analyzer.MOTIF_TYPE)) {
			return new Res("Analyzer created (id = " + motifService.create(payload, jid).getId() + ")");
		} else if (payload.get("type").equals(Analyzer.DUP_TYPE)) {
			return new Res("Analyzer created (id = " + dupService.create(payload, jid).getId() + ")");
		} else if (payload.get("type").equals(Analyzer.TARGET_TYPE)) {
			return new Res("Analyzer created (id = " + targetService.create(payload, jid).getId() + ")");
		} else {
			throw new IllegalArgumentException("Analyzer type not recognized");
		}
	}
}
