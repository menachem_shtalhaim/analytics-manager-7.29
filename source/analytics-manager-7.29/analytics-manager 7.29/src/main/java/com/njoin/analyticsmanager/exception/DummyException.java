package com.njoin.analyticsmanager.exception;

/**
 * A Dummy Exception, usually as a wrapper of other exceptions
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@SuppressWarnings("serial")
public class DummyException extends Exception {
}
