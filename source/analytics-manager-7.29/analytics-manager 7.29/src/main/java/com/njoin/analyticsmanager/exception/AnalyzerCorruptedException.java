package com.njoin.analyticsmanager.exception;

/**
 * An Exception indicating an analyzer is corruptyed, typically thrown when the
 * analyzer information in the database is inconsistent with the information
 * queried from the back-end
 * 
 * The solution for this exception is usually by deleting the analyzer
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@SuppressWarnings("serial")
public class AnalyzerCorruptedException extends Exception {
	private String message;

	/**
	 * Constructs an analyzer corrupted exception and generating the message
	 * 
	 * @param id
	 *            the id of the analyzer that is corrupted
	 * @param automatic
	 *            whether the system will automatically delete the analyzer
	 */
	public AnalyzerCorruptedException(int id, boolean automatic) {
		if (automatic) {
			this.message = "Analyzer with id " + id
					+ " is corrupted, automatically deleting the analyzer, please try again";
		} else {
			this.message = "Analyzer with id " + id + " is corrupted, please delete the analyzer and try again";
		}
	}

	@Override
	public String getMessage() {
		return message;
	}
}