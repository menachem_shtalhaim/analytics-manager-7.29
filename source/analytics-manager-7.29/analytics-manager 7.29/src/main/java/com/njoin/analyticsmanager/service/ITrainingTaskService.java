package com.njoin.analyticsmanager.service;

import java.util.List;
import java.util.Map;

import com.njoin.analyticsmanager.entity.TrainingTask;
import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;

/**
 * A Service that manages the training tasks. This service is not responsible
 * for executing tasks, it is provides an interface for the CRUD operation of
 * tasks
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
public interface ITrainingTaskService {

	/**
	 * Create a new training task from the payload
	 * 
	 * @param payload
	 * @param jid
	 * @return the creation success message
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	public String createTrainingTask(Map<String, Object> payload, String jid)
			throws BackEndServerException, SessionExpiredException;

	/**
	 * Delete the training task with the specified id, a running training task
	 * cannot be deleted
	 * 
	 * @param id
	 * @return the deletion success message
	 */
	public String deleteTrainingTask(int id);

	/**
	 * Retrieves all the training tasks from the database
	 * 
	 * @return all current training tasks
	 */
	public List<TrainingTask> showTrainingTasks();

	/**
	 * Smart generate the training tasks based on the specified information
	 * 
	 * @param payload
	 * @param jid
	 * @return the generation success message, along with the number of training
	 *         tasks successfully generated
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	public String smartGenerate(Map<String, Object> payload, String jid)
			throws BackEndServerException, SessionExpiredException;
}
