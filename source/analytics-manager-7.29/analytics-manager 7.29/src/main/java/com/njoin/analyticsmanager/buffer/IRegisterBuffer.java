package com.njoin.analyticsmanager.buffer;

import com.alibaba.fastjson.JSONArray;

/**
 * A buffer that prepares the response for the query of register list and stores
 * the response as a csv formatted data
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
public interface IRegisterBuffer extends IGenericBuffer<JSONArray, String> {
}
