package com.njoin.analyticsmanager.exception.handler;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.njoin.analyticsmanager.exception.SessionExpiredException;

/**
 * An error handler that handles the session expired exception
 * 
 * It redirects the user to the login page after redirecting the error
 * 
 * TODO: Prompts the user to login again when this exception happens in a AJAX
 * request
 * 
 * TODO: redirects the whole page to login and not just the ng-view
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@ControllerAdvice
@Controller
public class ExpireExceptionHandler {

	/**
	 * Redirects the client to the login page after catching the session expired
	 * exception
	 * 
	 * @param e
	 * @return
	 */
	@ExceptionHandler(value = SessionExpiredException.class)
	public String handleSessionExpiration(SessionExpiredException e) {
		return "redirect:/analytics/login";
	}

}
