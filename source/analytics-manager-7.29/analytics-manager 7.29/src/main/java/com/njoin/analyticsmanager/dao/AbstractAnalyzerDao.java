package com.njoin.analyticsmanager.dao;

import java.util.List;

import com.njoin.analyticsmanager.entity.analyzer.Analyzer;

/**
 * An abstract generic implementation of the Analyzer Dao interface, provides
 * the basic functionalities. extends the functionality provided by the abstract
 * dao
 * 
 * @author Henry Xing
 * @version 7.29
 *
 * @param <T>
 *            the type of the analyzer entity
 */
public abstract class AbstractAnalyzerDao<T extends Analyzer> extends AbstractDao<T> implements IAnalyzerDao<T> {

	@Override
	@SuppressWarnings("unchecked")
	public List<T> getAllUnArchived() {
		String hql = "FROM " + clazz.getName() + " AS a WHERE a.archived = FALSE";
		List<T> results = (List<T>) entityManager.createQuery(hql).getResultList();
		return results;
	}
}
