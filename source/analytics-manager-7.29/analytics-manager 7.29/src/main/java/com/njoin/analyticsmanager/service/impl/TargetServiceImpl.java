package com.njoin.analyticsmanager.service.impl;

import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.njoin.analyticsmanager.entity.DBInput;
import com.njoin.analyticsmanager.entity.analyzer.Analyzer;
import com.njoin.analyticsmanager.entity.analyzer.TargetAnalyzer;
import com.njoin.analyticsmanager.exception.AnalyzerCorruptedException;
import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;
import com.njoin.analyticsmanager.utils.DateUtil;

/**
 * An Implementation of Abstract Analyzer Service for the Targeted analyzer.
 * 
 * Particularly, the creation of the analyzer involves, sequentially, the
 * targeted anomaly model and the targeted group induction model
 * 
 * The creation of the analyzer requires the additional (sampleRate,
 * trainingDays, upperTime) set of parameters for the anomaly model, the
 * original set will be used for the group induction model
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Service
@Transactional
public class TargetServiceImpl extends AbstractAnalyzerService<TargetAnalyzer> {

	private void createSequence(TargetAnalyzer analyzer, Map<String, Object> payload,
			Map<String, Object> anomalyRequest, Map<String, Object> giRequest, String jid)
			throws BackEndServerException, SessionExpiredException {
		String anomalyResult = requestService.postJSON(createModelURL, jid, anomalyRequest);

		int anomalyModelId = processCreateResult(anomalyResult);

		giRequest.put("anomaly_model_modelid", anomalyModelId);
		String giResult = requestService.postJSON(createModelURL, jid, giRequest);
		int giModelId = processCreateResult(giResult);

		int aeId = aeService.createAE(payload, anomalyModelId, "", jid);

		analyzer.setModelId(anomalyModelId);
		analyzer.setGiModelId(giModelId);
		analyzer.setAeId(aeId);
	}

	private void analyzerSpecificPrepareRequest(Map<String, Object> anomalyRequest, Map<String, Object> giRequest,
			Map<String, Object> payload, TargetAnalyzer analyzer) {
		anomalyRequest.put("type", "TargetedPredictionAnomalyModel");
		giRequest.put("type", "TargetedPredictionGroupInductionModel");
		analyzer.setDisplayType(displayDao.getDisplayInfoByType(Analyzer.TARGET_TYPE).getDisplayType());
		analyzer.setType(Analyzer.TARGET_TYPE);

		if (payload.get("targetContent") == null) {
			throw new IllegalArgumentException("Parameter 'targetContent' is required for this analyzer");
		}
		if (payload.get("anomalySampleRate") == null || payload.get("anomalyTrainingDays") == null) {
			throw new IllegalArgumentException(
					"Parameter 'anomalySampleRate' and 'anomalyTrainingDays' is required for this analyzer");
		}

		try {
			int anomalySampleRate = (Integer) payload.get("anomalySampleRate");
			int anomalyTrainingDays = (Integer) payload.get("anomalyTrainingDays");
			String targetContent = (String) payload.get("targetContent");
			anomalyRequest.put("sample_rate", anomalySampleRate);
			anomalyRequest.put("train_time_frame", anomalyTrainingDays);
			giRequest.put("target_registers", targetContent);
			analyzer.setAnomalySampleRate(anomalySampleRate);
			analyzer.setAnomalyTrainingDays(anomalyTrainingDays);
			analyzer.setTargetContent(targetContent);
			if (payload.get("anomalyUpperTime") != null) {
				long anomalyUpperTime = (Long) payload.get("anomalyUpperTime");
				anomalyRequest.put("top_date", DateUtil.dateLongToString(anomalyUpperTime));
				analyzer.setAnomalyUpperTime(anomalyUpperTime);
			}
		} catch (ClassCastException e) {
			throw new IllegalArgumentException("Illegal input value for this analyzer");
		}

		anomalyRequest.put("sample_rate", payload.get("anomalySampleRate"));
		anomalyRequest.put("train_time_frame", payload.get("anomalyTrainingDays"));
		if (payload.get("anomalyUpperTime") != null) {
			try {
				anomalyRequest.put("top_date", DateUtil.dateLongToString((Long) payload.get("anomalyUpperTime")));
			} catch (ClassCastException e) {
				throw new IllegalArgumentException("Illegal input value for this analyzer");
			}
		}
	}

	@Override
	public TargetAnalyzer createRequestFromFile(Map<String, Object> payload, String fileContent, String jid)
			throws BackEndServerException, SessionExpiredException {

		TargetAnalyzer analyzer = new TargetAnalyzer();
		populateAnalyzer(analyzer, payload);

		Map<String, Object> anomalyRequest = prepairModelRequest(analyzer, "anomaly@");
		Map<String, Object> giRequest = prepairModelRequest(analyzer, "gi@");

		analyzerSpecificPrepareRequest(anomalyRequest, giRequest, payload, analyzer);

		populateFileRequest(giRequest, fileContent);

		createSequence(analyzer, payload, anomalyRequest, giRequest, jid);

		return analyzer;
	};

	@Override
	public TargetAnalyzer createRequestFromDB(Map<String, Object> payload, DBInput input, String jid)
			throws BackEndServerException, SessionExpiredException {

		TargetAnalyzer analyzer = new TargetAnalyzer();
		populateAnalyzer(analyzer, payload);

		Map<String, Object> anomalyRequest = prepairModelRequest(analyzer, "anomaly@");
		Map<String, Object> giRequest = prepairModelRequest(analyzer, "gi@");

		analyzerSpecificPrepareRequest(anomalyRequest, giRequest, payload, analyzer);

		populateDBRequest(giRequest, input);

		createSequence(analyzer, payload, anomalyRequest, giRequest, jid);

		return analyzer;
	};

	@Override
	public String delete(int id, String jid) throws BackEndServerException, SessionExpiredException {
		TargetAnalyzer analyzer = retriveExist(id);
		requestOperation(analyzer.getAeId(), deleteAEURL, jid);
		analyzer.setArchived(true);
		analyzerDao.saveOrUpdate(analyzer);
		requestOperation(analyzer.getGiModelId(), deleteModelURL, jid);
		requestOperation(analyzer.getModelId(), deleteModelURL, jid);

		return "Analyzer removed";
	}

	@Override
	public String train(int id, Integer batch, String jid) throws BackEndServerException, SessionExpiredException {

		TargetAnalyzer analyzer = retriveExist(id);
		trainOperation(analyzer.getGiModelId(), batch, jid);
		return "Model submitted for training";
	}

	@Override
	public String stop(int id, String jid) throws BackEndServerException, SessionExpiredException {

		TargetAnalyzer analyzer = retriveExist(id);
		requestOperation(analyzer.getGiModelId(), stopURL, jid);
		return "Model train session abort request submitted";
	}

	@Override
	public String removeSchedule(int id, String jid) throws BackEndServerException, SessionExpiredException {
		TargetAnalyzer analyzer = retriveExist(id);
		String result = requestOperation(analyzer.getGiModelId(), removeScheduleURL, jid);
		return parseScheduleResponse(result);
	}

	@Override
	public String suspendSchedule(int id, String jid) throws BackEndServerException, SessionExpiredException {
		TargetAnalyzer analyzer = retriveExist(id);
		String result = requestOperation(analyzer.getGiModelId(), suspendScheduleURL, jid);
		return parseScheduleResponse(result);
	}

	@Override
	public String resumeSchedule(int id, String jid) throws BackEndServerException, SessionExpiredException {
		TargetAnalyzer analyzer = retriveExist(id);
		String result = requestOperation(analyzer.getGiModelId(), resumeScheduleURL, jid);
		return parseScheduleResponse(result);
	}

	@Override
	public String createSchedule(int id, String cron, String jid)
			throws BackEndServerException, SessionExpiredException {
		TargetAnalyzer analyzer = retriveExist(id);
		String result = scheduleOperation(analyzer.getGiModelId(), cron, jid);
		return parseScheduleCreateResponse(result);
	}

	@Override
	public String toggleActive(int id, String jid)
			throws BackEndServerException, SessionExpiredException, AnalyzerCorruptedException {
		TargetAnalyzer analyzer = retriveExist(id);
		return aeService.toggleActive(analyzer.getAeId(), analyzer.getId(), jid);
	}

	@Override
	public String toggleVisible(int id, String jid)
			throws BackEndServerException, SessionExpiredException, AnalyzerCorruptedException {
		TargetAnalyzer analyzer = retriveExist(id);
		return aeService.toggleVisible(analyzer.getAeId(), analyzer.getId(), jid);
	}
}