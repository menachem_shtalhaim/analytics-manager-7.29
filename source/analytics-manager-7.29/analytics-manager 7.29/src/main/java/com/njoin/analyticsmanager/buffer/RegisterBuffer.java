package com.njoin.analyticsmanager.buffer;

import java.util.Map;
import java.util.concurrent.Future;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.njoin.analyticsmanager.service.IRequestService;

/**
 * An implementation of IRegisterBuffer. The buffer input comes in batches and
 * clears with new input
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Service
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class RegisterBuffer extends AbstractOperationBuffer<JSONArray, String> implements IRegisterBuffer {

	@Value("${server.train.report.su.url}")
	private String getSUNameURL;
	@Autowired
	private IRequestService requestService;

	/**
	 * The buffer is cleared upon new input.
	 */
	@Override
	public void prepare(Map<Integer, JSONArray> records, String jid) {
		clear();
		for (Map.Entry<Integer, JSONArray> entry : records.entrySet()) {
			JSONArray jsonArray = entry.getValue();
			Future<String> future = threadPool
					.submit(new RegisterCallable(jid, jsonArray, getSUNameURL, requestService));
			results.put(entry.getKey(), future);
		}
	}
}