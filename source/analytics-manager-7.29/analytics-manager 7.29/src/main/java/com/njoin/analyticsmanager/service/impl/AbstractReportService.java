package com.njoin.analyticsmanager.service.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.njoin.analyticsmanager.buffer.IGroupBuffer;
import com.njoin.analyticsmanager.buffer.IGroupCountBuffer;
import com.njoin.analyticsmanager.buffer.IRegisterBuffer;
import com.njoin.analyticsmanager.entity.ModelReport;
import com.njoin.analyticsmanager.entity.Report;
import com.njoin.analyticsmanager.entity.analyzer.Analyzer;
import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.service.IAnalyzerReportService;
import com.njoin.analyticsmanager.service.IModelService;
import com.njoin.analyticsmanager.service.IRequestService;

/**
 * An Abstract implementation for the analyzer report service, extracts the
 * common operations for each type of analyzer. It provides helper protected
 * methods to facilitate the concrete implementation of the service
 * 
 * This class also maintains all the relevant request urls and relevant general
 * usage services that all concrete implementations will use
 * 
 * @author Henry Xing
 * @version 7.29
 *
 * @param <T>
 *            the type of the Analyzer
 */
@Transactional
public abstract class AbstractReportService<T extends Analyzer> implements IAnalyzerReportService<T> {

	@Value("${server.train.report.url}")
	protected String trainReportURL;

	@Autowired
	protected IRequestService requestService;

	@Autowired
	private IGroupCountBuffer groupCountBuffer;
	@Autowired
	private IRegisterBuffer registerBuffer;
	@Autowired
	private IGroupBuffer groupBuffer;
	@Autowired
	private IModelService modelService;

	/**
	 * A helper method to extract the basic fields of the model report from the
	 * json object and the provided semantic unit map
	 * 
	 * @param json
	 *            the json object of the training report from the back-end
	 *            response
	 * @param su
	 *            the semantic unit map for this model report preprepared
	 * @return the created model report entity from the parameters
	 */
	private ModelReport extractModelReport(JSONObject json, Map<String, Integer> su) {
		ModelReport modelReport = new ModelReport();
		modelReport.setEndTime(json.getLong("endTime"));
		modelReport.setDataStartTime(json.getLong("trainDataStartTime"));
		modelReport.setDataEndTime(json.getLong("trainDataEndTime"));
		modelReport.setError(json.getString("errorMessage"));
		modelReport.setSu(su);
		return modelReport;
	}

	/**
	 * A helper method to extract the basic fields of the training anlayzer
	 * report from the json object specified
	 * 
	 * @param json
	 *            the json object of the training report from the back-end
	 *            response
	 * @return the created training report entity from the parameters
	 */
	private Report extractReport(JSONObject json) {
		Report report = new Report();
		report.setStatus(json.getString("state"));
		report.setSchedule(json.getString("cronSchedule"));
		report.setId(json.getIntValue("id"));
		report.setStartTime(json.getLong("startTime"));
		Map<String, ModelReport> modelReports = new HashMap<>(2);
		report.setModelReports(modelReports);
		return report;
	}

	/**
	 * Extract all reports for the specified analyzer from the server response
	 * result String. The method only extracts the model report of the primary
	 * component of the analyzer into the training report, i.e., the model the
	 * primary information of training is based on.
	 * 
	 * This method also handles the semantic unit preparation if the mode
	 * prepareSU is turned on
	 * 
	 * @param analyzer
	 *            the analyzer the report is based on
	 * @param primaryId
	 *            the id of the model corresponding to the primary component of
	 *            the analyzer
	 * @param result
	 *            the response body String from the back-end server for training
	 *            report
	 * @param modelName
	 *            the name of the primary component of the analyzer
	 * @param hasTwo
	 *            whether the component maintains both registers and group
	 *            semantic units
	 * @param giId
	 *            the id of the group induction component of the analyzer for
	 *            preparation of group semantic units, can be null if the
	 *            component does not encompass group induction
	 * @param jid
	 * @param prepareSU
	 *            whether to prepare the semantic units in this method
	 * @return the extracted report that contains the training report and the
	 *         model report for the primary component of the specified analyzer
	 * @throws Throwable
	 */
	protected List<Report> extractAllReports(T analyzer, int primaryId, String result, String modelName, boolean hasTwo,
			Integer giId, String jid, boolean prepareSU) throws Throwable {
		List<Report> reports = new LinkedList<>();
		Map<Integer, JSONArray> registerInput = new HashMap<>();
		Map<Integer, Integer> groupInput = new HashMap<>();
		try {
			JSONArray jsonArray = JSON.parseObject(result).getJSONArray("records");
			for (Object object : jsonArray) {
				JSONObject json = (JSONObject) object;
				Report report = extractReport(json);
				Map<String, Integer> sumap = new HashMap<>(2);
				sumap.put(ModelReport.REGISTER_TYPE, json.getJSONArray("semanticUnits").size());
				if (hasTwo && prepareSU) {
					sumap.put(ModelReport.GROUP_TYPE, groupCountBuffer.getResponse(giId));
					groupInput.put(report.getId(), giId);
				}
				ModelReport modelReport = extractModelReport(json, sumap);
				modelReport.setReport(report);
				modelReport.setType(modelName);
				report.getModelReports().put(modelName, modelReport);
				report.setRegistersFound(json.getJSONArray("semanticUnits").size());
				report.setAnalyzerId(analyzer.getId());

				if (report.getStatus().equals("RUNNING")) {
					String modelStatus = modelService.getModelStatus(primaryId, jid);
					if (modelStatus.contains("[Idle]")) {
						report.setStatus("CRUSHED");
						modelReport.setError(modelReport.getError() + " [server crashed]");
					}
				}

				reports.add(report);
				JSONArray array = json.getJSONArray("semanticUnits");

				registerInput.put(report.getId(), array);
			}
		} catch (Exception e) {
			throw new BackEndServerException("Unable to read server response");
		}
		if (prepareSU) {
			registerBuffer.prepare(registerInput, jid);
			if (hasTwo) {
				groupBuffer.prepare(groupInput, jid);
			}
		}
		return reports;
	}

	/**
	 * This model extracts the training model report of the additional group
	 * induction component of the analyzer. The report is added to each matching
	 * report in the list of reports specified. This method should be called
	 * immediately after extractAllReports for analyzer with the additional
	 * component
	 * 
	 * @param result
	 *            the response body String from the back-end server for training
	 *            report
	 * @param reports
	 *            the extracted training reports with the model report of the
	 *            primary component
	 * @param modelName
	 *            the name of the group induction component of the analyzer
	 * @param giId
	 *            the id of the group induction component of the analyzer
	 * @param jid
	 * @param prepareSU
	 *            whether to prepare the semantic units in this method
	 * @throws Throwable
	 */
	protected void addGroupInductionModelToEachReport(String result, List<Report> reports, String modelName, int giId,
			String jid, boolean prepareSU) throws Throwable {
		Map<Integer, Integer> groupInput = new HashMap<>();
		try {
			JSONArray jsonArray = JSON.parseObject(result).getJSONArray("records");
			Iterator<Report> iterator = reports.listIterator();

			for (Object object : jsonArray) {
				JSONObject json = (JSONObject) object;
				if (iterator.hasNext()) {
					Report report = iterator.next();
					Map<String, Integer> sumap = new HashMap<>(2);
					if (prepareSU && !iterator.hasNext()) {
						sumap.put(ModelReport.GROUP_TYPE, groupCountBuffer.getResponse(giId));
					}
					ModelReport modelReport = extractModelReport(json, sumap);
					modelReport.setReport(report);
					modelReport.setType(modelName);
					if (report.getStatus().equals("CRUSHED")) {
						modelReport.setError(modelReport.getError() + " [server crashed]");
					}
					groupInput.put(report.getId(), giId);
					report.getModelReports().put(modelName, modelReport);
				}
			}
		} catch (Exception e) {
			throw new BackEndServerException("Unable to read server response");
		}
		if (prepareSU) {
			groupBuffer.prepare(groupInput, jid);
		}
	}

}
