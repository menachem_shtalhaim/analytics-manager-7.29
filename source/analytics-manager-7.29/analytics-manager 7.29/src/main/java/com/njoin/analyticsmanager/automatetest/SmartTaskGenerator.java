package com.njoin.analyticsmanager.automatetest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.njoin.analyticsmanager.dao.IDisplayDao;
import com.njoin.analyticsmanager.entity.DisplayInfo;
import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.DummyException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;
import com.njoin.analyticsmanager.service.ITrainingTaskService;

/**
 * A concrete implementation of a ISmartTaskGenerator. This generator allows the
 * user to specify a range and increment step for the following parameters of
 * analyzer creation:
 * <p>
 * <ul>
 * <li>sampleRate</li>
 * <li>trainingDays</li>
 * <li>maxNumeric</li>
 * <li>maxNonNumeric</li>
 * </ul>
 * </p>
 * 
 * Optionally, the user can specify the devices used for database query.
 * 
 * For analyzers that allows different sets of (sampleRate, upperTime,
 * trainingDays) values, this generator will set the same value for all of them.
 * This implementation might not be ideal and is subject to change
 * 
 * This generator refrains from testing all permutation of the values specified.
 * Instead, it generates the tasks in an incremental manner. For every new task,
 * one of the parameter is changed for the task to be more difficult/costly. The
 * frequency of each parameter being incremented is proportional to the total
 * increments available for that specific parameter. This stepwise generation
 * enables stress testing or other types of incremental experimentation.
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Service
@Transactional
public class SmartTaskGenerator implements ISmartTaskGenerator {

	@Autowired
	private ITrainingTaskService trainingTaskService;
	@Autowired
	private IDisplayDao displayDao;

	@Override
	public int generate(Map<String, Object> payload, String jid)
			throws BackEndServerException, SessionExpiredException {
		String type, devices, targetContent;
		int sampleRateMin, sampleRateInc, sampleRateMax, trainingDaysMin, trainingDaysInc, trainingDaysMax, numericMin,
				numericInc, numericMax, nonNumericMin, nonNumericInc, nonNumericMax, minNumeric, minNonNumeric;
		long upperTime;
		boolean temporary;
		try {
			type = (String) payload.get("type");
			temporary = (Boolean) payload.get("temporary");
			sampleRateMin = (Integer) payload.get("sampleRateMin");
			sampleRateInc = (Integer) payload.get("sampleRateInc");
			sampleRateMax = (Integer) payload.get("sampleRateMax");
			trainingDaysMin = (Integer) payload.get("trainingDaysMin");
			trainingDaysInc = (Integer) payload.get("trainingDaysInc");
			trainingDaysMax = (Integer) payload.get("trainingDaysMax");
			upperTime = (Long) payload.get("upperTime");

			numericMin = (Integer) payload.get("numericMin");
			numericInc = (Integer) payload.get("numericInc");
			numericMax = (Integer) payload.get("numericMax");
			nonNumericMin = (Integer) payload.get("nonNumericMin");
			nonNumericInc = (Integer) payload.get("nonNumericInc");
			nonNumericMax = (Integer) payload.get("nonNumericMax");
			minNumeric = (Integer) payload.get("minNumeric");
			minNonNumeric = (Integer) payload.get("minNonNumeric");
			devices = (String) payload.get("devices");
			targetContent = (String) payload.get("targetContent");

			if (sampleRateMin > sampleRateMax || trainingDaysMin > trainingDaysMax || numericMin > numericMax
					|| nonNumericMin > nonNumericMax) {
				// The min values cannot exceed the max values
				throw new DummyException();
			}

			if (sampleRateInc >= 0 || trainingDaysInc <= 0 || numericInc <= 0 || nonNumericInc <= 0) {
				// The increment must be in the direction for the task to
				// becoming more difficult
				throw new DummyException();
			}

			DisplayInfo dInfo = displayDao.getDisplayInfoByType(type);
			if (dInfo == null) {
				throw new DummyException();
			}

			if (!dInfo.isNumeric()) {
				// numeric value should not change if it is not allowed
				numericMax = numericMin;
			}
			if (!dInfo.isNonNumeric()) {
				// non numeric value should not change if it is not allowed
				nonNumericMax = nonNumericMin;
			}

		} catch (Exception e) {
			throw new IllegalArgumentException("Invalid input for smart task generation");
		}

		// The number of times each parameter can be incremented
		int sampleRateIncCount = (sampleRateMin - sampleRateMax) / sampleRateInc;
		int trainingDaysIncCount = (trainingDaysMax - trainingDaysMin) / trainingDaysInc;
		int numericIncCount = (numericMax - numericMin) / numericInc;
		int nonNumericIncCount = (nonNumericMax - nonNumericMin) / nonNumericInc;

		// prepare the first training task creation request
		Map<String, Object> request = new HashMap<>();
		request.put("type", type);
		request.put("temporary", temporary);
		request.put("targetContent", targetContent);
		request.put("devices", devices);
		request.put("minNumericValue", minNumeric);
		request.put("minNonNumericValue", minNonNumeric);
		request.put("upperTime", upperTime);
		request.put("sampleRate", sampleRateMax);
		request.put("trainingDays", trainingDaysMin);
		request.put("maxNumeric", numericMin);
		request.put("maxNonNumeric", nonNumericMin);
		request.put("anomalyUpperTime", request.get("upperTime"));
		request.put("anomalySampleRate", request.get("sampleRate"));
		request.put("anomalyTrainingDays", request.get("trainingDays"));

		trainingTaskService.createTrainingTask(request, jid);

		// keeps track of the number of task successfully created
		Counter counter = new Counter();
		counter.count++;

		// A list containing the current parameter combination
		List<Node> nodes = new ArrayList<>();
		nodes.add(
				new Node(sampleRateMax, sampleRateMin, sampleRateInc, sampleRateMax, sampleRateIncCount, "sampleRate"));
		nodes.add(new Node(trainingDaysMin, trainingDaysMax, trainingDaysInc, trainingDaysMin, trainingDaysIncCount,
				"trainingDays"));
		nodes.add(new Node(numericMin, numericMax, numericInc, numericMin, numericIncCount, "maxNumeric"));
		nodes.add(new Node(nonNumericMin, nonNumericMax, nonNumericInc, nonNumericMin, nonNumericIncCount,
				"maxNonNumeric"));

		// parameters with fewer steps to increment to the boundary should
		// appear first in the list
		Collections.sort(nodes, new Comparator<Node>() {

			@Override
			public int compare(Node o1, Node o2) {
				return o1.count - o2.count;
			}
		});

		// Every node is linked to the subsequent node with the least higher
		// increment count
		Node prev = null;
		for (Node node : nodes) {
			if (prev != null) {
				prev.nextLayer = node;
			}
			prev = node;
		}
		prev.nextLayer = null;

		Iterator<Node> iterator = nodes.iterator();
		while (iterator.hasNext()) {
			Node node = iterator.next();
			if (node.count > 0) {
				// trigger the move sequence for the first movable parameter
				// node
				node.move(node.count, request, trainingTaskService, jid, counter);
				break;
			}
		}

		return counter.count;
	}

	/**
	 * A Counter that registers how many training tasks have been created
	 * 
	 * @author Henry Xing
	 * @version 7.29
	 *
	 */
	private static class Counter {
		public int count;
	}

	/**
	 * A Node that tracks the value of a single parameter. Moving the node will
	 * trigger all the nodes behind this one to move corresponding to their
	 * allowance - the proportion of the increment count of that node to this
	 * node
	 * 
	 * @author Henry Xing
	 * @version 7.29
	 *
	 */
	private static class Node {

		public Node nextLayer;
		public String name;
		public int start;
		public int end;
		public int step;
		public int cur;
		public int count;

		/**
		 * Constructor
		 * 
		 * @param start
		 *            the starting value of the parameter
		 * @param end
		 *            the ending value of the parameter
		 * @param step
		 *            the step of increase for the value of the parameter
		 * @param cur
		 *            the current value of the parameter
		 * @param count
		 *            the number of increase allowed for the parameter
		 * @param name
		 *            the name of the parameter as exists in the creation
		 *            request
		 */
		public Node(int start, int end, int step, int cur, int count, String name) {
			this.start = start;
			this.end = end;
			this.step = step;
			this.cur = cur;
			this.count = count;
			this.name = name;
		}

		/**
		 * Performs the action of the node, in this case, it modifies the
		 * request according to the nodes parameter value and creates a new
		 * training task
		 * 
		 * @param request
		 *            the original request
		 * @param trainingTaskService
		 * @param jid
		 * @param counter
		 *            the counter for task creation, will be incremented after
		 *            the task is successfully created
		 * @throws BackEndServerException
		 * @throws SessionExpiredException
		 */
		public void action(Map<String, Object> request, ITrainingTaskService trainingTaskService, String jid,
				Counter counter) throws BackEndServerException, SessionExpiredException {
			request.put(name, cur);
			addAnomalyFields(request);
			trainingTaskService.createTrainingTask(request, jid);
			counter.count++;
		}

		/**
		 * Copies the (sampleRate, upperTime, trainingDays) parameters to the
		 * anomaly model
		 * 
		 * @param request
		 *            the original request
		 */
		private void addAnomalyFields(Map<String, Object> request) {
			request.put("anomalyUpperTime", request.get("upperTime"));
			request.put("anomalySampleRate", request.get("sampleRate"));
			request.put("anomalyTrainingDays", request.get("trainingDays"));
		}

		/**
		 * Move the node forward, will trigger the move operation for the next
		 * node. This method moves the node allowance times and moves the next
		 * node according to the ratio of the two node's total allowance of
		 * moving
		 * 
		 * @param allowance
		 *            the times this node can move
		 * @param request
		 *            the original request
		 * @param trainingTaskService
		 * @param jid
		 * @param counter
		 *            the counter for task creation, will be incremented every
		 *            time after the task is successfully created
		 * @throws BackEndServerException
		 * @throws SessionExpiredException
		 */
		public void move(int allowance, Map<String, Object> request, ITrainingTaskService trainingTaskService,
				String jid, Counter counter) throws BackEndServerException, SessionExpiredException {
			for (int i = 0; i < allowance; i++) {
				if (canProceed()) {
					proceed();
					action(request, trainingTaskService, jid, counter);
				}
				if (nextLayer != null) {
					nextLayer.move(nextLayer.count / count, request, trainingTaskService, jid, counter);
				}
			}
			// handles the leftover move allowances for the next node
			// relies on the fact that a = (a / b) * b + a % b
			if (nextLayer != null) {
				nextLayer.move(nextLayer.count % count, request, trainingTaskService, jid, counter);
			}
		}

		/**
		 * Proceed and increment the parameter value
		 */
		public void proceed() {
			cur += step;
		}

		/**
		 * whether the node can proceed
		 * 
		 * @return false if the next increment will birng the value out of
		 *         range, true otherwise
		 */
		public boolean canProceed() {
			if (cur + step >= start && cur + step <= end) {
				return true;
			}
			if (cur + step <= start && cur + step >= end) {
				return true;
			}
			return false;
		}
	}

}
