package com.njoin.analyticsmanager.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.njoin.analyticsmanager.service.ILoginService;

/**
 * Controller that processes login and logout requests and communicates with the
 * back-end server with the specified tokens
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@Controller
public class LoginController {

	@Autowired
	private ILoginService loginService;

	/**
	 * Processes the login post request. Attempts to login to the back-end
	 * server, either sets a failure or sets the JSESSIONID in the cookie
	 * 
	 * @param username
	 * @param password
	 * @param session
	 * @param res
	 * @return
	 * @throws IOException
	 */
	@PostMapping("analytics/login")
	public String login(@RequestParam("username") String username, @RequestParam("password") String password,
			HttpSession session, HttpServletResponse res) throws IOException {
		String jid = loginService.login(username, password);
		if (jid == null) {
			res.sendRedirect("/analytics/login/login.html?error");
			return null;
		} else {
			session.setAttribute("jid", jid);
			return "redirect:/analytics";
		}
	}

	/**
	 * Logs out the user and removes the cookie, redirects to the login page
	 * 
	 * @param session
	 * @return
	 */
	@GetMapping("analytics/logout")
	public String logout(HttpSession session) {
		session.removeAttribute("jid");
		return "redirect:/analytics/login";
	}

	/**
	 * Serves the login page
	 * 
	 * @return
	 */
	@GetMapping("analytics/login")
	public String loginPage() {
		return "/analytics/login/login.html";
	}
}