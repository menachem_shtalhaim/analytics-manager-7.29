package com.njoin.analyticsmanager.service;

import com.njoin.analyticsmanager.entity.Records;
import com.njoin.analyticsmanager.exception.AnalyzerCorruptedException;
import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;

/**
 * A Service that parses the analyzer data and shows the analyzer information as
 * a Records entity to the front end
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
public interface IShowService {

	/**
	 * Shows all the analyzer record. This method retrieves data from all the
	 * unarchived analyzers in the database, it also queries to the back-end
	 * server API to populate the transient fields of the analyzer
	 * 
	 * @param jid
	 * @return a records entity encompassing all the unarchived analyzer
	 *         information
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 * @throws AnalyzerCorruptedException
	 */
	public Records showRecords(String jid)
			throws BackEndServerException, SessionExpiredException, AnalyzerCorruptedException;

}
