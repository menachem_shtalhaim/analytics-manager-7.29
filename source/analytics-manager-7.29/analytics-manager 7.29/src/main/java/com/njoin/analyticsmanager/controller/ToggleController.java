package com.njoin.analyticsmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttribute;

import com.njoin.analyticsmanager.entity.Res;
import com.njoin.analyticsmanager.entity.analyzer.Analyzer;
import com.njoin.analyticsmanager.entity.analyzer.DupAnalyzer;
import com.njoin.analyticsmanager.entity.analyzer.KNNAnalyzer;
import com.njoin.analyticsmanager.entity.analyzer.MotifAnalyzer;
import com.njoin.analyticsmanager.entity.analyzer.TargetAnalyzer;
import com.njoin.analyticsmanager.exception.AnalyzerCorruptedException;
import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;
import com.njoin.analyticsmanager.service.IAnalyzerService;

/**
 * Rest Controller for toggling the status of the analyzers. For all the
 * methods, the client must specify an analyzer identified with its type and id
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
@RestController
public class ToggleController {

	@Autowired
	private IAnalyzerService<KNNAnalyzer> knnService;
	@Autowired
	private IAnalyzerService<MotifAnalyzer> motifService;
	@Autowired
	private IAnalyzerService<DupAnalyzer> dupService;
	@Autowired
	private IAnalyzerService<TargetAnalyzer> targetService;

	/**
	 * Toggle the status of the real time analysis monitor
	 * 
	 * @param type
	 * @param id
	 * @param jid
	 * @return
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 * @throws AnalyzerCorruptedException
	 */
	@GetMapping("/analytics/toggle/active")
	public Res toggleActive(@RequestParam("type") String type, @RequestParam("id") int id,
			@SessionAttribute("jid") String jid)
			throws BackEndServerException, SessionExpiredException, AnalyzerCorruptedException {
		if (type.equals(Analyzer.KNN_TYPE)) {
			return new Res(knnService.toggleActive(id, jid));
		} else if (type.equals(Analyzer.MOTIF_TYPE)) {
			return new Res(motifService.toggleActive(id, jid));
		} else if (type.equals(Analyzer.DUP_TYPE)) {
			return new Res(dupService.toggleActive(id, jid));
		} else if (type.equals(Analyzer.TARGET_TYPE)) {
			return new Res(targetService.toggleActive(id, jid));
		} else {
			throw new IllegalArgumentException("Analyzer type not recognized");
		}
	}

	/**
	 * Toggle the status of the real time analysis visibility
	 * 
	 * @param type
	 * @param id
	 * @param jid
	 * @return
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 * @throws AnalyzerCorruptedException
	 */
	@GetMapping("/analytics/toggle/visible")
	public Res toggleVisible(@RequestParam("type") String type, @RequestParam("id") int id,
			@SessionAttribute("jid") String jid)
			throws BackEndServerException, SessionExpiredException, AnalyzerCorruptedException {
		if (type.equals(Analyzer.KNN_TYPE)) {
			return new Res(knnService.toggleVisible(id, jid));
		} else if (type.equals(Analyzer.MOTIF_TYPE)) {
			return new Res(motifService.toggleVisible(id, jid));
		} else if (type.equals(Analyzer.DUP_TYPE)) {
			return new Res(dupService.toggleVisible(id, jid));
		} else if (type.equals(Analyzer.TARGET_TYPE)) {
			return new Res(targetService.toggleVisible(id, jid));
		} else {
			throw new IllegalArgumentException("Analyzer type not recognized");
		}
	}
}
