package com.njoin.analyticsmanager.service;

import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;

/**
 * A Service that manages Elastic Search Repositories, the methods directly
 * calls the back-end server API
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
public interface IESService {

	/**
	 * Show all the repositories 
	 * @param jid
	 * @return
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	public String showES(String jid) throws BackEndServerException, SessionExpiredException;

	/**
	 * Clear the specified repository
	 * @param name
	 * @param jid
	 * @return
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	public String clear(String name, String jid) throws BackEndServerException, SessionExpiredException;

	/**
	 * Clear and insert all to the specified repository
	 * @param name
	 * @param jid
	 * @return
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	public String clearInsert(String name, String jid) throws BackEndServerException, SessionExpiredException;

	/**
	 * insert and update all to the specified repository
	 * @param name
	 * @param jid
	 * @return
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	public String insertUpdate(String name, String jid) throws BackEndServerException, SessionExpiredException;

}
