package com.njoin.analyticsmanager.service;

import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.njoin.analyticsmanager.entity.AnalysisEngine;
import com.njoin.analyticsmanager.exception.AnalyzerCorruptedException;
import com.njoin.analyticsmanager.exception.BackEndServerException;
import com.njoin.analyticsmanager.exception.SessionExpiredException;

/**
 * A Service that manages Analysis Engines
 * 
 * @author Henry Xing
 * @version 7.29
 *
 */
public interface IAEService {

	/**
	 * Extract and populate the fields of an analysis engine entity from the
	 * server response json object
	 * 
	 * @param aeObject
	 *            the json object corresponding to the analysis engine
	 * @return the parsed analysis engine entity
	 */
	public AnalysisEngine extractAE(JSONObject aeObject);

	/**
	 * Create an analysis engine in the back-end server from the payload
	 * 
	 * @param payload
	 *            the request to create the analysis engine
	 * @param modelId
	 *            the model the analysis engine is monitoring on
	 * @param prefix
	 *            the prefix of the analysis engine name
	 * @param jid
	 * @return the id of the created analysis engine
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 */
	public int createAE(Map<String, Object> payload, int modelId, String prefix, String jid)
			throws BackEndServerException, SessionExpiredException;

	/**
	 * Toggle the real time analysis monitor of the analysis engine
	 * 
	 * @param id
	 *            the id of the engine
	 * @param displayId
	 *            the id of the analyzer
	 * @param jid
	 * @return the message of the operation
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 * @throws AnalyzerCorruptedException
	 */
	public String toggleActive(int id, int displayId, String jid)
			throws BackEndServerException, SessionExpiredException, AnalyzerCorruptedException;

	/**
	 * Toggle the real time analysis visibility of the analysis engine
	 * 
	 * @param id
	 *            the id of the engine
	 * @param displayId
	 *            the id of the analyzer
	 * @param jid
	 * @return the message of the operation
	 * @throws BackEndServerException
	 * @throws SessionExpiredException
	 * @throws AnalyzerCorruptedException
	 */
	public String toggleVisible(int id, int displayId, String jid)
			throws BackEndServerException, SessionExpiredException, AnalyzerCorruptedException;

}
