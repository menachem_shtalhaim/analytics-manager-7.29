'use strict';

angular.module('analyzer').
	factory('fileHandler', function() {
		var handler = {};

		var warn = function(msg) {
			bootbox.alert({
				title: "Warning",
				message: msg,
				backdrop: true
			});
		}

		var suc = function(msg) {
			bootbox.alert({
				title: "Success",
				message: msg,
				backdrop: true
			});
		}

		handler.upload = function(file, callback) {
			var r = new FileReader();
	        r.onloadend = function(e) {
	            var data = e.target.result;

	            if(data.length > 5000000){
	                warn(" file size <"+data.length+">  of file  "+ file.name + " is above limit of 5MB");
	                return;
	            }

	            callback(data);
	            suc("file "+ file.name + " loaded!!!");
	        }
	        r.readAsText(file);
		}

		handler.download = function(data, format, name) {
			var url = window.URL.createObjectURL(new Blob([data], {type: format}));
			var link = document.createElement('a');
    		if (typeof link.download === 'string') {
        		document.body.appendChild(link);
        		link.download = name;
        		link.href = url;
        		link.click();
        		document.body.removeChild(link);
    		} else {
        		location.replace(url);
    		}
		}

		return handler;
	});
