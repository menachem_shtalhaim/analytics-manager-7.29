'use strict';

angular.module('analyzer').
	factory('callbackHandler', function() {
		var handler = {};

		handler.getErrCallback = function() {
			var errCallback = function(err) {
				bootbox.alert({
					title: "Error!",
					message: err.data.message,
					backdrop: true
				});
			}
			return errCallback;
		}

		handler.getSucCallback = function(callback) {
			var sucCallback = function(res) {
				bootbox.alert({
					title: "Success!",
					message: res.data.message,
					backdrop: true
				});
				if (callback !== undefined) {
					callback();
				}
			}
			return sucCallback;
		}

		handler.getParseCallback = function(parse) {
			var sucCallback = function(res) {
				parse(res.data);
			}
			return sucCallback;
		}

		handler.getConfirmCallback = function() {
			return function (message, callback) {
					bootbox.confirm({
						title: "Message",
						message: message, 
						backdrop: true,
						callback: function(result) {
							if (result === true) {
								callback();
							}
						}
					});
				}
		}

		return handler;
	});
