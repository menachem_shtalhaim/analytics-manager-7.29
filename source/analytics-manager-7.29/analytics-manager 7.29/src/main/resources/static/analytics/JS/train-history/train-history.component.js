'use strict';

angular.
	module('trainHistory').
	component('trainHistory', {
		templateUrl: '/analytics/JS/train-history/train-history.template.html',
		controller: ['requestHandler', 'timeParser', 'dataParser','callbackHandler', '$uibModal',
			function ReportController(requestHandler, timeParser,dataParser, callbackHandler, $uibModal) {
				var $ctrl = this;

				$ctrl.enableAutoRefresh = false;
				$ctrl.showArchived = false;

				$ctrl.history = [];
				$ctrl.analyzers = {};

				$ctrl.sortType = "record.startTime";

				$ctrl.parseData = function(data){
					$ctrl.analyzers = data.analyzers
					$ctrl.history = []
					$.each(data.reports, function(idx, item){
						var record = {};
						record.recordId = item.id;
						record.status = item.status;
						record.registersFound = item.registersFound;
						if (record.registersFound === null) {
							record.registersFound = "not available";
						}
						record.analyzerId = item.analyzerId + ''
						record.startTime = timeParser.formatTime(item.startTime);
						record.analyzerDisplay = $ctrl.analyzers[record.analyzerId].displayType + " (id = " + $ctrl.analyzers[record.analyzerId].id + ")"

						record.span = Object.keys(item.modelReports).length;
						var showRecord = true;

						$.each(item.modelReports, function(name, modelReport) {
							if (modelReport.dataStartTime !== null) {
								modelReport.dataStartTime = timeParser.formatTime(modelReport.dataStartTime);
							}
							if (modelReport.dataEndTime !== null) {
								modelReport.dataEndTime = timeParser.formatTime(modelReport.dataEndTime);
							}

							if (modelReport.endTime === null) {
								if (record.status === "CRUSHED") {
									modelReport.duration = "not available: crushed";
								} else {
									modelReport.duration = "in progress...";
								}
							} else {
								modelReport.duration = timeParser.getDiff(item.startTime, modelReport.endTime);
							}
							if (modelReport.error === null) {
								modelReport.error = "no error";
							}

							modelReport.name = name;
							modelReport.showRecord = showRecord;
							showRecord = false;
							modelReport.record = record;

							$ctrl.history.push(modelReport);
						});
					});
				}

				var parseCallback = callbackHandler.getParseCallback($ctrl.parseData);
				var errCallback = callbackHandler.getErrCallback();

				$ctrl.refresh = function() {
					requestHandler.showHistory(parseCallback, errCallback);
				}

				$ctrl.detail = function(item) {
					var modal = $uibModal.open({
						animation: true,
						backdrop: true,
						ariaLabelledBy: 'modal-title',
						ariaDescribedBy: 'modal-body',
						component: 'detailModal',
						size: 'lg',
						resolve: {
							item: function() {
								return dataParser.extractItem(item);
							}
						}
					});
					modal.result.then($ctrl.refresh);
				}

				$ctrl.canShow = function(value, index, array) {
					if ($ctrl.showArchived) {
						return true;
					}
					return !$ctrl.analyzers[value.record.analyzerId].archived;
				}

				$ctrl.selectSort = function(field) {
					$ctrl.sortType = field;
				}

				$ctrl.autoRefresh = function() {
					$ctrl.refresh();
					if ($ctrl.enableAutoRefresh) {
						setTimeout($ctrl.autoRefresh, 2000);
					}
				}

				$ctrl.startAutoRefresh = function(){
					if ($ctrl.enableAutoRefresh) {
						$ctrl.autoRefresh();
					}
				}

				$ctrl.refresh();

				$ctrl.$onDestroy = function(){
					$ctrl.enableAutoRefresh = false;
				}
			}
		]
	});