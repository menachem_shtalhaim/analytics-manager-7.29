'use strict';

angular.
	module('manageMqtt').
	component('manageMqtt', {
		templateUrl: '/analytics/JS/manage-mqtt/manage-mqtt.template.html',
		controller: ['requestHandler', 'callbackHandler',
			function ManageMqttController(requestHandler, callbackHandler) {
				var $ctrl = this;

				$ctrl.data = [];

				$ctrl.request = {name: null, address: null, identifier: null}

				$ctrl.enableAutoRefresh = false;

				var confirm = callbackHandler.getConfirmCallback();
				
				$ctrl.parseData = function(data){
					$ctrl.data = data.devices;
				}

				var parseCallback = callbackHandler.getParseCallback($ctrl.parseData);
				var errCallback = callbackHandler.getErrCallback();

				$ctrl.refresh = function() {
					requestHandler.showMqtt(parseCallback, errCallback);
				}

				var sucCallback = callbackHandler.getSucCallback($ctrl.refresh);

				$ctrl.autoRefresh = function() {
					$ctrl.refresh();
					if ($ctrl.enableAutoRefresh) {
						setTimeout($ctrl.autoRefresh, 2000);
					}
				}

				$ctrl.startAutoRefresh = function(){
					if ($ctrl.enableAutoRefresh) {
						$ctrl.autoRefresh();
					}
				}

				$ctrl.submit = function() {
					requestHandler.createMqtt($ctrl.request, sucCallback, errCallback);
				}

				$ctrl.refresh();

				$ctrl.$onDestroy = function(){
					$ctrl.enableAutoRefresh = false;
				}
			}
		]
	});