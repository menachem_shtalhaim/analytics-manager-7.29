'use strict';

angular.
	module('trainReport').
	component('trainReport', {
		templateUrl: '/analytics/JS/train-report/train-report.template.html',
		controller: ['requestHandler', 'timeParser', 'fileHandler', 'callbackHandler', '$location',
			function ReportController(requestHandler, timeParser, fileHandler, callbackHandler, $location) {
				var $ctrl = this;

				$ctrl.params = $location.search();
				$ctrl.enableAutoRefresh = false;

				$ctrl.data = [];

				$ctrl.parseData = function(data){
					$ctrl.data = [];
					$.each(data, function(idx, item){
						var record = {};
						record.recordId = item.id;
						record.status = item.status;
						record.schedule = item.schedule;
						record.startTime = timeParser.formatTime(item.startTime);

						if (record.schedule === null) {
							record.schedule = "unscheduled";
						}

						record.span = Object.keys(item.modelReports).length;
						
						var showRecord = true;

						$.each(item.modelReports, function(name, modelReport) {
							if (modelReport.dataStartTime !== null) {
								modelReport.dataStartTime = timeParser.formatTime(modelReport.dataStartTime);
							}
							if (modelReport.dataEndTime !== null) {
								modelReport.dataEndTime = timeParser.formatTime(modelReport.dataEndTime);
							}

							if (modelReport.endTime === null) {
								if (record.status === "CRUSHED") {
									modelReport.duration = "not available: crushed";
								} else {
									modelReport.duration = "in progress...";
								}
							} else {
								modelReport.duration = timeParser.getDiff(item.startTime, modelReport.endTime);
							}
							if (modelReport.error === null) {
								modelReport.error = "no error";
							}
							modelReport.name = name;
							modelReport.showRecord = showRecord;
							showRecord = false;
							modelReport.record = record;

							$.each(modelReport.su, function(suType, suCount) {
								modelReport.su[suType] = {};
								modelReport.su[suType].count = suCount;
								if (suType === "group") {
									modelReport.su[suType].faClass = "fa-search";
									modelReport.su[suType].action = "Inspect";
								} else if (suType === "register") {
									modelReport.su[suType].action = "Download csv";
									modelReport.su[suType].faClass = "fa-download";
								}
							})

							$ctrl.data.push(modelReport);
						});
					});
				}

				var parseCallback = callbackHandler.getParseCallback($ctrl.parseData);
				var errCallback = callbackHandler.getErrCallback();

				$ctrl.refresh = function() {
					requestHandler.showReport($ctrl.params.i, $ctrl.params.t, parseCallback, errCallback);
				}

				$ctrl.showSU = function(recordId, suType) {
					window.open("/analytics/su?recordId=" + recordId + "&suType=" + suType);
				}

				$ctrl.autoRefresh = function() {
					$ctrl.refresh();
					if ($ctrl.enableAutoRefresh) {
						setTimeout($ctrl.autoRefresh, 2000);
					}
				}

				$ctrl.startAutoRefresh = function(){
					if ($ctrl.enableAutoRefresh) {
						$ctrl.autoRefresh();
					}
				}

				$ctrl.refresh();

				$ctrl.$onDestroy = function(){
					$ctrl.enableAutoRefresh = false;
				}
			}
		]
	});