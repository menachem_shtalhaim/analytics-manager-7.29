'use strict';

angular.
	module('showAnalyzers').
	component('scheduleModal', {
		templateUrl: '/analytics/JS/show-analyzers/show-analyzers.schedule-modal.template.html',
		bindings: {
		    resolve: '<',
		    close: '&',
		    dismiss: '&'
		},
		controller: ['requestHandler', 'dataParser', 'callbackHandler',
			function ScheduleController(requestHandler, dataParser, callbackHandler) {
				var $ctrl = this;

				var errCallback = callbackHandler.getErrCallback();

				$ctrl.setItem = function(item) {
					if (item.schedule.length == 0) {
						$ctrl.set = false;
						item.schedule = "not set";
					} else {
						$ctrl.set = true;
					}
					$ctrl.item = item;
				}

				$ctrl.setItem($ctrl.resolve.item);


				$ctrl.cancel = function () {
			    	$ctrl.close();
			    };

			    $ctrl.cron = "";

			    $ctrl.disabled = {
			    	create: false,
			    	remove: false,
			    	suspend: false,
			    	resume: false
			    }

			    $ctrl.enableAll = function() {
			    	$ctrl.disabled.create = false;
			    	$ctrl.disabled.remove = false;
			    	$ctrl.disabled.suspend = false;
			    	$ctrl.disabled.resume = false;
			    }

			    $ctrl.parseData = function(data) {
			    	var item = dataParser.updateItem(data.records, $ctrl.item);
			    	$ctrl.setItem(item);
			    	$ctrl.enableAll();
			    }

			    var parseCallback = callbackHandler.getParseCallback($ctrl.parseData);

			    $ctrl.refresh = function() {
			    	requestHandler.showAnalyzer(parseCallback, errCallback);
			    }

			    var sucCallback = callbackHandler.getSucCallback($ctrl.refresh);

			    $ctrl.create = function() {
			    	$ctrl.disabled.create = true;
			    	requestHandler.createSchedule($ctrl.item, $ctrl.cron, sucCallback, errCallback);
			    }

			    $ctrl.remove = function() {
			    	$ctrl.disabled.remove = true;
			    	requestHandler.manageSchedule($ctrl.item, 'remove', sucCallback, errCallback);
			    }

			    $ctrl.suspend = function() {
			    	$ctrl.disabled.suspend = true;
			    	requestHandler.manageSchedule($ctrl.item, 'suspend', sucCallback, errCallback);
			    }

			    $ctrl.resume = function() {
			    	$ctrl.disabled.resume = true;
			    	requestHandler.manageSchedule($ctrl.item, 'resume', sucCallback, errCallback);
			    }
			}
		]
	});