'use strict';

angular.
	module('manageSubscription').
	component('changeSubscriptionModal', {
		templateUrl: '/analytics/JS/manage-subscription/manage-subscription.change-modal.template.html',
		bindings: {
		    resolve: '<',
		    close: '&',
		    dismiss: '&'
		},
		controller: ['requestHandler', 'callbackHandler',
			function ChangeSubscriptionController(requestHandler, callbackHandler) {
				var $ctrl = this;

				$ctrl.cancel = function () {
			    	$ctrl.close();
			    };

				var errCallback = callbackHandler.getErrCallback();
				var sucCallback = callbackHandler.getSucCallback($ctrl.cancel);

				$ctrl.subscriber = $.extend({},$ctrl.resolve.subscriber);
				$ctrl.mode = $ctrl.resolve.mode;

			    $ctrl.reset = function(){
			    	$ctrl.subscriber = $.extend({},$ctrl.resolve.subscriber);
			    }

			    $ctrl.submit = function() {
			    	requestHandler.saveSubscription($ctrl.subscriber, sucCallback, errCallback);
			    }
			}
		]
	});