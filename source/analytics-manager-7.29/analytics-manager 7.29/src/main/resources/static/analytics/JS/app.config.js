'use strict';
angular.
  module('analyticsConsole').
  config(['$locationProvider' ,'$routeProvider',
    function config($locationProvider, $routeProvider) {
      $locationProvider.hashPrefix('!');

      $routeProvider.
        when('/show-analyzers', {
          template: '<show-analyzers></show-analyzers>'
        }).
        when('/create-analyzer', {
          template: '<create-analyzer></create-analyzer>'
        }).
        when('/train-report', {
          template: '<train-report></train-report>'
        }).
        when('/create-simulation', {
          template: '<create-simulation></create-simulation>'
        }).
        when('/show-simulation', {
          template: '<show-simulation></show-simulation>'
        }).
        when('/feed-value', {
          template: '<feed-value></feed-value>'
        }).
        when('/detect-state', {
          template: '<detect-state></detect-state>'
        }).
        when('/import-state', {
          template: '<import-state></import-state>'
        }).
        when('/manage-es', {
          template: '<manage-es></manage-es>'
        }).
        when('/manage-mqtt', {
          template: '<manage-mqtt></manage-mqtt>'
        }).
        when('/manage-subscription', {
          template: '<manage-subscription></manage-subscription>'
        }).
        when('/train-history', {
          template: '<train-history></train-history>'
        }).
        when('/train-task', {
          template: '<train-task></train-task>'
        }).
        otherwise('/show-analyzers');
    }
  ]);
