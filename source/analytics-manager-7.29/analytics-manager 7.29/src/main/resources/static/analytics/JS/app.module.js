'use strict';
angular.module('analyticsConsole',
	['ngRoute','ngSanitize','angularValidator','moment-picker','ui.toggle','ui.bootstrap',
	'analyzer','showAnalyzers','createAnalyzer', 'trainReport', 'createSimulation', 'showSimulation', 'feedValue', 'manageState','manageEs','manageMqtt','manageSubscription','trainHistory','trainTask']);