'use strict';

angular.
	module('feedValue').
	component('feedValue', {
		templateUrl: '/analytics/JS/feed-value/feed-value.template.html',
		controller: ['requestHandler', 'callbackHandler',
			function FeedValueController(requestHandler, callbackHandler) {
				var $ctrl = this;

				$ctrl.registerId = null;
				$ctrl.value = null;

				var sucCallback = callbackHandler.getSucCallback();
				var errCallback = callbackHandler.getErrCallback();

				$ctrl.submit = function() {
					requestHandler.feedValue($ctrl.registerId, $ctrl.value, sucCallback, errCallback);
				}
			}
		]
	});