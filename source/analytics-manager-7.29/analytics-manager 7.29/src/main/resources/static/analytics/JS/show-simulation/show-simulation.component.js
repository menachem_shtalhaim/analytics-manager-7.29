'use strict';

angular.
	module('showSimulation').
	component('showSimulation', {
		templateUrl: '/analytics/JS/show-simulation/show-simulation.template.html',
		controller: ['requestHandler', 'callbackHandler', 'timeParser',
			function ShowSimulationController(requestHandler, callbackHandler, timeParser) {

				var $ctrl = this;
				$ctrl.data = {};
				$ctrl.motif = false;
				$ctrl.enableAutoRefresh = false;

				var confirm = callbackHandler.getConfirmCallback();
				
				$ctrl.parseData = function(data){
					$ctrl.data = data;
					if (data.cacheSizeTotal > 0) {
						$ctrl.motif = true;
					} else {
						$ctrl.motif = false;
					}
					var p = data.queueSize / data.queueSizeMax;
					if (p < 0.7) {
						$ctrl.data.type = 'info';
					} else if (p < 0.9) {
						$ctrl.data.type = 'warning';
					} else {
						$ctrl.data.type = 'danger';
					}
					$.each($ctrl.data.running, function(idx, item) {
						item.start = timeParser.formatTime(item.start);
						item.end = timeParser.formatTime(item.end);
					});
					$.each($ctrl.data.waiting, function(idx, item) {
						item.start = timeParser.formatTime(item.start);
						item.end = timeParser.formatTime(item.end);
					});
				}

				var parseCallback = callbackHandler.getParseCallback($ctrl.parseData);
				var errCallback = callbackHandler.getErrCallback();

				$ctrl.refresh = function() {
					requestHandler.showSimulation(parseCallback, errCallback);
				}

				var sucCallback = callbackHandler.getSucCallback($ctrl.refresh);

				$ctrl.removeRunning = function(){
					confirm("Are you sure you want to terminate the running simulation task?", function(){
						requestHandler.removeRunning(sucCallback, errCallback);
					});
				}

				$ctrl.removeWaiting = function(){
					confirm("Are you sure you want to remove all waiting simulation tasks?", function(){
						requestHandler.removeWaiting(sucCallback, errCallback);
					});
				}

				$ctrl.removeStatistics = function(){
					confirm("Are you sure you want to remove all analyzer statistics?", function(){
						requestHandler.removeStatistics(sucCallback, errCallback);
					});
				}

				$ctrl.autoRefresh = function() {
					$ctrl.refresh();
					if ($ctrl.enableAutoRefresh) {
						setTimeout($ctrl.autoRefresh, 2000);
					}
				}

				$ctrl.startAutoRefresh = function(){
					if ($ctrl.enableAutoRefresh) {
						$ctrl.autoRefresh();
					}
				}

				$ctrl.refresh();

				$ctrl.$onDestroy = function(){
					$ctrl.enableAutoRefresh = false;
				}
			}
		]
	});