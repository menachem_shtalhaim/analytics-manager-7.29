'use strict';

angular.module('analyzer').
	factory('requestHandler',['$http', function($http) {
		var handler = {};

		var getRequest = function(url, data, sucCallback, errCallback) {
			$http({
				url: url, 
				method: "GET",
				params: data,
			}).then(sucCallback, errCallback);
		}

		var postRequest = function(url, data, sucCallback, errCallback) {
			$http.post(url, data).then(sucCallback, errCallback);
		}

		// For Create Analyzer View

		handler.createAnalyzer = function(analyzer, sucCallback, errCallback) {
			postRequest('/analytics/create', analyzer, sucCallback, errCallback);
		}

		handler.showTypes = function(sucCallback, errCallback) {
			getRequest('/analytics/display', null, sucCallback, errCallback);
		}

		// For Show Analyzers View

		handler.showAnalyzer = function(sucCallback, errCallback) {
			getRequest('/analytics/show', null, sucCallback, errCallback);
		}

		handler.deleteAnalyzer = function(item, sucCallback, errCallback) {
			getRequest('/analytics/delete', {id: item.id, type: item.type}, sucCallback, errCallback);
		}

		handler.trainAnalyzer = function(item, batch, sucCallback, errCallback) {
			getRequest('/analytics/train', {id: item.id, type: item.type, batch: batch}, sucCallback, errCallback);
		}

		handler.stopAnalyzer = function(item, sucCallback, errCallback) {
			getRequest('/analytics/stop', {id: item.id, type: item.type}, sucCallback, errCallback);
		}

		// For scheduling View

		handler.createSchedule = function(item, cron, sucCallback, errCallback) {
			getRequest('/analytics/schedule/create', {id: item.id, type: item.type, cron: cron}, sucCallback, errCallback);
		}

		handler.manageSchedule = function(item, action, sucCallback, errCallback) {
			getRequest('/analytics/schedule/' + action, {id: item.id, type: item.type}, sucCallback, errCallback);
		}

		// For toggles

		handler.toggle = function(item, action, sucCallback, errCallback) {
			getRequest('/analytics/toggle/' + action, {id: item.id, type: item.type}, sucCallback, errCallback);
		}

		// For Show Report View

		handler.showReport = function(id, type, sucCallback, errCallback) {
			getRequest('/analytics/report', {id: id, type: type}, sucCallback, errCallback);
		}

		// For Simulation

		handler.createSimulation = function(simulation, sucCallback, errCallback) {
			postRequest('/analytics/simulation/create', simulation, sucCallback, errCallback);
		}

		handler.showSimulation = function(sucCallback, errCallback) {
			getRequest('/analytics/simulation/show', null, sucCallback, errCallback);
		}

		handler.removeRunning = function(sucCallback, errCallback) {
			getRequest('/analytics/simulation/remove_running', null, sucCallback, errCallback);
		}	

		handler.removeWaiting = function(sucCallback, errCallback) {
			getRequest('/analytics/simulation/remove_waiting', null, sucCallback, errCallback);
		}	

		handler.removeStatistics = function(sucCallback, errCallback) {
			getRequest('/analytics/simulation/remove_statistics', null, sucCallback, errCallback);
		}

		// For feed value

		handler.feedValue = function(registerId, value, sucCallback, errCallback) {
			getRequest('/analytics/feed', {registerId: registerId, value: value}, sucCallback, errCallback);
		}

		// For State Register

		handler.showStateRequest = function(sucCallback, errCallback) {
			getRequest('/analytics/state/requests', null, sucCallback, errCallback);
		}

		handler.detectStateRequest = function(request, sucCallback, errCallback) {
			getRequest('/analytics/state/detect', request, sucCallback, errCallback);
		}

		handler.deleteStateRequest = function(id, sucCallback, errCallback) {
			getRequest('/analytics/state/remove', {requestId:id}, sucCallback, errCallback);
		}

		handler.importState = function(filePath, isNew, sucCallback, errCallback) {
			getRequest('/analytics/state/import', {filePath: filePath, isNew: isNew}, sucCallback, errCallback);
		}

		// For ES

		handler.showEs = function(sucCallback, errCallback) {
			getRequest('/analytics/es/show', null, sucCallback, errCallback);
		}

		handler.clearEs = function(name, sucCallback, errCallback) {
			getRequest('/analytics/es/clear', {name: name}, sucCallback, errCallback);
		}

		handler.clearInsertEs = function(name, sucCallback, errCallback) {
			getRequest('/analytics/es/clear_insert', {name: name}, sucCallback, errCallback);
		}

		handler.insertUpdateEs = function(name, sucCallback, errCallback) {
			getRequest('/analytics/es/insert_update', {name: name}, sucCallback, errCallback);
		}

		// For MQTT

		handler.showMqtt = function(sucCallback, errCallback) {
			getRequest('/analytics/mqtt/show', null, sucCallback, errCallback);
		}

		handler.createMqtt = function(request, sucCallback, errCallback) {
			getRequest('/analytics/mqtt/create', request, sucCallback, errCallback);
		}

		// For Subsciption

		handler.showSubscription = function(sucCallback, errCallback) {
			getRequest('/analytics/subscriber/show', null, sucCallback, errCallback);
		}

		handler.saveSubscription = function(subscriber, sucCallback, errCallback) {
			postRequest('/analytics/subscriber/save', subscriber, sucCallback,errCallback);
		}

		handler.deleteSubscription = function(id, sucCallback, errCallback) {
			getRequest('/analytics/subscriber/delete', {id:id}, sucCallback,errCallback);
		}

		// For Training History
		handler.showHistory = function(sucCallback, errCallback) {
			getRequest('/analytics/history', null, sucCallback,errCallback);
		}

		handler.showTask = function(sucCallback, errCallback) {
			getRequest('/analytics/task/show', null, sucCallback, errCallback);
		}

		handler.createTask = function(request, sucCallback, errCallback) {
			postRequest('/analytics/task/create', request, sucCallback, errCallback);
		}

		handler.createAutoTask = function(request, sucCallback, errCallback) {
			postRequest('/analytics/task/smart', request, sucCallback, errCallback);
		}

		handler.deleteTask = function(id, sucCallback, errCallback) {
			getRequest('/analytics/task/delete', {id:id}, sucCallback,errCallback);
		}


		return handler;
	}]);
