'use strict';

angular.
	module('trainTask').
	component('trainTask', {
		templateUrl: '/analytics/JS/train-task/train-task.template.html',
		controller: ['requestHandler', 'fileHandler','callbackHandler','createConfigurater', '$uibModal',
			function TrainTaskController(requestHandler, fileHandler, callbackHandler,createConfigurater, $uibModal) {
				var $ctrl = this;

				$ctrl.data = [];
				$ctrl.types = [];
				$ctrl.auto = true;

				$ctrl.request = {
					type:null,
					sampleRate: 30,
					trainingDays: 30,
					upperTime: null,
					anomalySampleRate: 30,
					anomalyUpperTime: null,
					anomalyTrainingDays: 30,
					maxNumeric: 1,
					maxNonNumeric: 0,
					minNumericValue: 0,
					minNonNumericValue: 0,
					targetContent: null,
					devices: null,
					temporary: true
				}

				$ctrl.autoRequest = {
					type:null,
					upperTime: null,
					minNumeric: 0,
					minNonNumeric: 0,
					targetContent: null,
					devices: null,
					temporary: true,
					sampleRateMin: 30,
					sampleRateMax: 60,
					sampleRateInc: -15,
					trainingDaysMin: 20,
					trainingDaysMax: 60,
					trainingDaysInc: 20,
					numericMin: 500,
					numericMax: 2000,
					numericInc: 500,
					nonNumericMin: 1000,
					nonNumericMax: 1000,
					nonNumericInc: 1
				}

				$ctrl.enableAutoRefresh = false;

				var confirm = callbackHandler.getConfirmCallback();
				
				$ctrl.parseData = function(data){
					$ctrl.data = data;
					$.each($ctrl.data, function(idx, item) {
						if (item.status === 1) {
							item.status = "Running"
						} else if (item.status === 0) {
							item.status = "Waiting"
						}
					})
				}

				$ctrl.dataType = new Map();

				$ctrl.parseType = function(data) {
					$ctrl.types = [];
					$.each(data, function(idx, item) {
						item.support = [];
						if (item.numeric === true) {
							item.support.push("numeric");
						}
						if (item.nonNumeric === true) {
							item.support.push("non numeric");
						}
						$ctrl.dataType.set(item.type, {numeric: item.numeric, nonNumeric: item.nonNumeric});
						$ctrl.types.push(item);
					});
				}

				$ctrl.reconfig = function() {
					$ctrl.config = createConfigurater.configurate($ctrl.request.type);
					$ctrl.autoConfig = createConfigurater.configurate($ctrl.autoRequest.type);
				}

				var parseCallback = callbackHandler.getParseCallback($ctrl.parseData);
				var parseTypeCallback = callbackHandler.getParseCallback($ctrl.parseType);
				var errCallback = callbackHandler.getErrCallback();

				$ctrl.refresh = function() {
					requestHandler.showTask(parseCallback, errCallback);
				}

				$ctrl.showTypes = function() {
					requestHandler.showTypes(parseTypeCallback, errCallback);
				}

				var sucCallback = callbackHandler.getSucCallback($ctrl.refresh);
				var createCallback = callbackHandler.getSucCallback(function(){
					$ctrl.request.upperTime = undefined;
					$ctrl.request.anomalyUpperTime = undefined;
					$ctrl.reconfig();
					$ctrl.refresh();
				});
				var autoCreateCallback = callbackHandler.getSucCallback(function(){
					$ctrl.autoRequest.upperTime = undefined;
					$ctrl.reconfig();
					$ctrl.refresh();
				});

				$ctrl.autoRefresh = function() {
					$ctrl.refresh();
					if ($ctrl.enableAutoRefresh) {
						setTimeout($ctrl.autoRefresh, 2000);
					}
				}

				$ctrl.startAutoRefresh = function(){
					if ($ctrl.enableAutoRefresh) {
						$ctrl.autoRefresh();
					}
				}

				$ctrl.uploadTarget = function(element) {
					var file = element.files[0];
			        var callback = function(data) {
			        	$ctrl.request.targetContent = data;
			        }
			        fileHandler.upload(file, callback);
				}

				$ctrl.uploadAutoTarget = function(element) {
					var file = element.files[0];
			        var callback = function(data) {
			        	$ctrl.autoRequest.targetContent = data;
			        }
			        fileHandler.upload(file, callback);
				}

				$ctrl.detail = function(item) {
					var modal = $uibModal.open({
						animation: true,
						backdrop: true,
						ariaLabelledBy: 'modal-title',
						ariaDescribedBy: 'modal-body',
						component: 'taskDetailModal',
						size: 'lg',
						resolve: {
							item: function() {
								return item;
							}
						}
					});
					modal.result.then($ctrl.refresh);
				}

				$ctrl.submit = function() {
					$ctrl.request.upperTime = Date.parse($ctrl.request.upperTime);
					if ($ctrl.config.twoTF === true) {
						$ctrl.request.anomalyUpperTime = Date.parse($ctrl.request.anomalyUpperTime);
					}
					requestHandler.createTask($ctrl.request, createCallback, errCallback);
				}

				$ctrl.autoSubmit = function() {
					$ctrl.autoRequest.upperTime = Date.parse($ctrl.autoRequest.upperTime);
					requestHandler.createAutoTask($ctrl.autoRequest, autoCreateCallback, errCallback);
				}

				$ctrl.delete = function(item) {
					confirm("Are you sure you want to delete this task?", function(){
						requestHandler.deleteTask(item.id, sucCallback, errCallback);
					})
				}

				$ctrl.refresh();
				$ctrl.showTypes();
				$ctrl.reconfig();

				$ctrl.$onDestroy = function(){
					$ctrl.enableAutoRefresh = false;
				}
			}
		]
	});