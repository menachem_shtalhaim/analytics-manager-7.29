'use strict';

angular.
	module('showAnalyzers').
	component('detailModal', {
		templateUrl: '/analytics/JS/show-analyzers/show-analyzers.detail-modal.template.html',
		bindings: {
		    resolve: '<',
		    close: '&',
		    dismiss: '&'
		},
		controller: ['timeParser',
			function DetailController(timeParser) {
				var $ctrl = this;

				$ctrl.setItem = function(item) {
					item.upperTime = timeParser.formatTime(item.upperTime);
					item.anomalyUpperTime = timeParser.formatTime(item.anomalyUpperTime);
					if (item.schedule === null) {
						item.scheduleSet = false;
						item.schedule = "not available";
					} else if (item.schedule.length == 0) {
						item.scheduleSet = false;
						item.schedule = "not set";
					} else {
						item.scheduleSet = true;
					}
					if (item.dbInput !== null) {
						if (item.dbInput.devices === null) {
							item.dbInput.devices = "all devices";
						}
					}
					if (item.analysisEngine !== null) {
						if (item.analysisEngine.emailAddress === null) {
							item.subscribed = "No";
						} else {
							item.subscribed = "Yes";
						}
						if (item.analysisEngine.active) {
							item.analysisEngine.active = "active";
						} else {
							item.analysisEngine.active = "inactive";
						}
						if (item.analysisEngine.visible) {
							item.analysisEngine.visible = "visible";
						} else {
							item.analysisEngine.visible = "invisible";
						}
					} else {
						item.subscribed = "not available";
					}
					if (item.upperTime === null) {
						item.upperTime = "Current Time";
					}
					if (item.anomalyUpperTime === null) {
						item.anomalyUpperTime = "Current Time";
					}
					$ctrl.item = item;
				}

				$ctrl.setItem($ctrl.resolve.item);


				$ctrl.cancel = function () {
			    	$ctrl.close();
			    };
			}
		]
	});