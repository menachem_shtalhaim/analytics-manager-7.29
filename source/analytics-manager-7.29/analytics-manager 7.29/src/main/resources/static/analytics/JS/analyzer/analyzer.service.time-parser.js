'use strict';

angular.module('analyzer').
	factory('timeParser', function() {
		var parser = {};

		parser.formatTime = function(mili) {
			if (mili === null || mili === undefined) {
				return null
			}

			var date = new Date(mili);
			var year = date.getFullYear();
			var month = date.getMonth() + 1;
			var day = date.getDate();
			var hour = date.getHours();
			var minute = date.getMinutes();
			var second = date.getSeconds();

			month = month > 9 ? "" + month : "0" + month;
			day = day > 9 ? "" + day : "0" + day;
			hour = hour > 9 ? "" + hour : "0" + hour;
			minute = minute > 9 ? "" + minute : "0" + minute;
			second = second > 9 ? "" + second : "0" + second;

			return year + "/" + month + "/" + day + " " + hour + ":" + minute + ":" + second;
		}

		parser.getDiff = function(start, end) {
			var d = end - start;
			var mili = d % 1000;
			d = Math.floor(d / 1000);
			var second = d % 60;
			d = Math.floor(d / 60);
			var minute = d % 60;
			d = Math.floor(d / 60);
			var hour = d;

			if (mili < 10) {
				mili = "000" + mili;
			} else if (mili < 100) {
				mili = "00" + mili;
			} else if (mili < 1000) {
				mili = "0" + mili;
			} else {
				mili = "" + mili;
			}

			hour = hour > 9 ? "" + hour : "0" + hour;
			minute = minute > 9 ? "" + minute : "0" + minute;
			second = second > 9 ? "" + second : "0" + second;

			return hour + ":" + minute + ":" + second + ":" + mili;
		}

		return parser;
	});
