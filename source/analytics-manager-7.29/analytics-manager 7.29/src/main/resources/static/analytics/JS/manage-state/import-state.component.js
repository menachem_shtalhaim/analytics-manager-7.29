'use strict';

angular.
	module('manageState').
	component('importState', {
		templateUrl: '/analytics/JS/manage-state/import-state.template.html',
		controller: ['requestHandler', 'callbackHandler',
			function ImportStateController(requestHandler, callbackHandler) {
				var $ctrl = this;

				$ctrl.filePath = null;
				$ctrl.isNew = true;

				var sucCallback = callbackHandler.getSucCallback();
				var errCallback = callbackHandler.getErrCallback();

				$ctrl.submit = function() {
					requestHandler.importState($ctrl.filePath, $ctrl.isNew, sucCallback, errCallback);
				}
			}
		]
	});