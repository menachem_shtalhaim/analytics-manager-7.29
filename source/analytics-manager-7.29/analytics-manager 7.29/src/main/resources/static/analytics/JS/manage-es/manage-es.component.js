'use strict';

angular.
	module('manageEs').
	component('manageEs', {
		templateUrl: '/analytics/JS/manage-es/manage-es.template.html',
		controller: ['requestHandler', 'callbackHandler',
			function ManageEsController(requestHandler, callbackHandler) {
				var $ctrl = this;

				$ctrl.data = [];

				$ctrl.enableAutoRefresh = false;

				var confirm = callbackHandler.getConfirmCallback();
				
				$ctrl.parseData = function(data){
					$ctrl.data = data;
				}

				var parseCallback = callbackHandler.getParseCallback($ctrl.parseData);
				var errCallback = callbackHandler.getErrCallback();

				$ctrl.refresh = function() {
					requestHandler.showEs(parseCallback, errCallback);
				}

				var sucCallback = callbackHandler.getSucCallback($ctrl.refresh);

				$ctrl.autoRefresh = function() {
					$ctrl.refresh();
					if ($ctrl.enableAutoRefresh) {
						setTimeout($ctrl.autoRefresh, 2000);
					}
				}

				$ctrl.startAutoRefresh = function(){
					if ($ctrl.enableAutoRefresh) {
						$ctrl.autoRefresh();
					}
				}

				$ctrl.clear = function(item) {
					confirm("Are you sure you want to clear this repository?", function() {
						requestHandler.clearEs(item.name, sucCallback, errCallback);
					});
				}

				$ctrl.clearInsert = function(item) {
					confirm("Are you sure you want to clear and insert all to this repository?", function() {
						requestHandler.clearInsertEs(item.name, sucCallback, errCallback);
					});
				}

				$ctrl.insertUpdate = function(item) {
					confirm("Are you sure you want to insert or update all to this repository?", function() {
						requestHandler.insertUpdateEs(item.name, sucCallback, errCallback);
					});
				}

				$ctrl.refresh();

				$ctrl.$onDestroy = function(){
					$ctrl.enableAutoRefresh = false;
				}
			}
		]
	});