'use strict';

angular.
	module('manageSubscription').
	component('manageSubscription', {
		templateUrl: '/analytics/JS/manage-subscription/manage-subscription.template.html',
		controller: ['requestHandler', 'callbackHandler', '$uibModal',
			function ManageSubscriptionController(requestHandler, callbackHandler, $uibModal) {
				var $ctrl = this;

				$ctrl.data = [];

				var confirm = callbackHandler.getConfirmCallback();
				
				$ctrl.parseData = function(data){
					$ctrl.data = data;
					$.each($ctrl.data, function(id, item) {
						if (item.level == 0) {
							item.displayLevel = "No Notification";
						} else if (item.level == 1) {
							item.displayLevel = "Error Only";
						} else {
							item.displayLevel = "All Notifications";
						}
					})
				}

				var parseCallback = callbackHandler.getParseCallback($ctrl.parseData);
				var errCallback = callbackHandler.getErrCallback();

				$ctrl.refresh = function() {
					requestHandler.showSubscription(parseCallback, errCallback);
				}

				var sucCallback = callbackHandler.getSucCallback($ctrl.refresh);

				$ctrl.change = function(mode, subscriber) {
					var modal = $uibModal.open({
						animation: true,
						backdrop: true,
						ariaLabelledBy: 'modal-title',
						ariaDescribedBy: 'modal-body',
						component: 'changeSubscriptionModal',
						size: 'lg',
						resolve: {
							subscriber: function() {
								return subscriber;
							},
							mode: function() {
								return mode;
							}
						}
					});
					modal.result.then(function(){
						$ctrl.refresh();
					});
				}

				$ctrl.add = function() {
					$ctrl.change("Add", {
						id: null,
						name: null,
						address: null,
						subjectTemplate: "[$level$] Analyzer id=$id$ is $status$!",
						bodyTemplate: "The status of analyzer id=$id$ is changed\n\nStatus: $status$\n\nError Message:\n$error$",
						level: 1
					});
				}

				$ctrl.update = function(subscriber) {
					$ctrl.change("Update", subscriber);
				}

				$ctrl.delete = function(subscriber) {
					confirm("Are you sure you want to delete this subscriber?", function(){
						requestHandler.deleteSubscription(subscriber.id, sucCallback, errCallback);
					});
				}

				$ctrl.refresh();
			}
		]
	});