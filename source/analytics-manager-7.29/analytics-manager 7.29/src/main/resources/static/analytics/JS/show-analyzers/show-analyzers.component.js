'use strict';

angular.
	module('showAnalyzers').
	component('showAnalyzers', {
		templateUrl: '/analytics/JS/show-analyzers/show-analyzers.template.html',
		controller: ['requestHandler', '$uibModal', 'dataParser', 'callbackHandler',
			function ShowController(requestHandler, $uibModal, dataParser, callbackHandler) {

				var $ctrl = this;
				$ctrl.data = [];
				$ctrl.enableAutoRefresh = false;

				var confirm = callbackHandler.getConfirmCallback();
				
				$ctrl.parseData = function(data){
					$ctrl.data = dataParser.parseData(data.records);
				}

				var parseCallback = callbackHandler.getParseCallback($ctrl.parseData);
				var errCallback = callbackHandler.getErrCallback();

				$ctrl.refresh = function() {
					requestHandler.showAnalyzer(parseCallback, errCallback);
				}

				var sucCallback = callbackHandler.getSucCallback($ctrl.refresh);

				$ctrl.delete = function(item) {
					confirm("Are you sure you want to delete this analyzer? (id=" + item.id + ")",function() {
						requestHandler.deleteAnalyzer(item, sucCallback, errCallback);
					});
					
				}

				$ctrl.train = function(item) {
					confirm("Are you sure you want to train this analyzer? (id=" + item.id + ")",function() {
						bootbox.prompt({
						    title: "Please enter a batch size",
						    inputType: 'number',
						    value: 3000,
						    callback: function(result) {
						    	requestHandler.trainAnalyzer(item, result, sucCallback, errCallback);
						    }
						});
					});					
				}

				$ctrl.stop = function(item) {
					confirm("Are you sure you want to stop this analyzer? (id=" + item.id + ")",function() {
						requestHandler.stopAnalyzer(item, sucCallback, errCallback);
					});
				}

				$ctrl.toggleActive = function(item) {
					requestHandler.toggle(item, 'active', sucCallback, errCallback);
				}

				$ctrl.toggleVisible = function(item) {
					requestHandler.toggle(item, 'visible', sucCallback, errCallback);
				}

				$ctrl.schedule = function(item) {
					var modal = $uibModal.open({
						animation: true,
						backdrop: true,
						ariaLabelledBy: 'modal-title',
						ariaDescribedBy: 'modal-body',
						component: 'scheduleModal',
						size: 'lg',
						resolve: {
							item: function() {
								return item;
							}
						}
					});
					modal.result.then($ctrl.refresh);
				}

				$ctrl.detail = function(item) {
					var modal = $uibModal.open({
						animation: true,
						backdrop: true,
						ariaLabelledBy: 'modal-title',
						ariaDescribedBy: 'modal-body',
						component: 'detailModal',
						size: 'lg',
						resolve: {
							item: function() {
								return item;
							}
						}
					});
					modal.result.then($ctrl.refresh);
				}

				$ctrl.autoRefresh = function() {
					$ctrl.refresh();
					if ($ctrl.enableAutoRefresh) {
						setTimeout($ctrl.autoRefresh, 2000);
					}
				}

				$ctrl.startAutoRefresh = function(){
					if ($ctrl.enableAutoRefresh) {
						$ctrl.autoRefresh();
					}
				}

				$ctrl.refresh();

				$ctrl.$onDestroy = function(){
					$ctrl.enableAutoRefresh = false;
				}
			}
		]
	});