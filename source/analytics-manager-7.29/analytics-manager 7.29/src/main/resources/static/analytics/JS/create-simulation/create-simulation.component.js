'use strict';

angular.
	module('createSimulation').
	component('createSimulation', {
		templateUrl: '/analytics/JS/create-simulation/create-simulation.template.html',
		controller: ['requestHandler', 'callbackHandler',
			function CreateSimulationController(requestHandler, callbackHandler) {
				var $ctrl = this;

				$ctrl.simulation = {}

				$ctrl.refresh = function() {
					$ctrl.simulation.from = undefined;
					$ctrl.simulation.to = undefined;
					$ctrl.simulation.frequency = 1000;
				}

				$ctrl.refresh();

				var sucCallback = callbackHandler.getSucCallback($ctrl.refresh);
				var errCallback = callbackHandler.getErrCallback();

				$ctrl.submit = function() {
					if (Date.parse($ctrl.simulation.to) <= Date.parse($ctrl.simulation.from)) {
						errCallback({data:{message:"Date range is invalid"}});
					} else {
						$ctrl.simulation.from = Date.parse($ctrl.simulation.from);
						$ctrl.simulation.to = Date.parse($ctrl.simulation.to);
						requestHandler.createSimulation($ctrl.simulation, sucCallback, errCallback);
					}
				}

				$ctrl.validateTime = function() {
					if (Date.parse($ctrl.simulation.from) < Date.parse($ctrl.simulation.to)) {
						return true;
					} else {
						return false;
					}
				}
			}
		]
	});