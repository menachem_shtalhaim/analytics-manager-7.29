'use strict';

angular.module('analyzer').
	factory('createConfigurater', function() {
		var configurater = {};

		configurater.configurate = function(type) {
			var config = {
				twoTF: false,
				target: false
			}
			if (type === null) {
				return config;
			}
			if (type === "dup") {
				config.twoTF = true;
			} else if (type === "target") {
				config.twoTF = true;
				config.target = true;
			}
			return config;
		}

		return configurater;
	});
