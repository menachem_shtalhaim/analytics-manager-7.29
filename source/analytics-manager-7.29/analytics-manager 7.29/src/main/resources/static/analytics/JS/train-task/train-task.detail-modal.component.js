'use strict';

angular.
	module('trainTask').
	component('taskDetailModal', {
		templateUrl: '/analytics/JS/train-task/train-task.detail-modal.template.html',
		bindings: {
		    resolve: '<',
		    close: '&',
		    dismiss: '&'
		},
		controller: ['timeParser',
			function TaskDetailController(timeParser) {
				var $ctrl = this;

				$ctrl.setItem = function(item) {
					item.analyzerCreateData.upperTime = timeParser.formatTime(item.analyzerCreateData.upperTime);
					item.analyzerCreateData.anomalyUpperTime = timeParser.formatTime(item.analyzerCreateData.anomalyUpperTime);

					if (item.analyzerCreateData.upperTime === null) {
						item.analyzerCreateData.upperTime = "Current Time";
					}
					if (item.analyzerCreateData.anomalyUpperTime === null) {
						item.analyzerCreateData.anomalyUpperTime = "Current Time";
					}
					$ctrl.item = item;
				}

				$ctrl.setItem($ctrl.resolve.item);


				$ctrl.cancel = function () {
			    	$ctrl.close();
			    };
			}
		]
	});