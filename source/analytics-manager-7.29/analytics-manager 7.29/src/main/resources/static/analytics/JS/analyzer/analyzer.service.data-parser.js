'use strict';

angular.module('analyzer').
	factory('dataParser', function() {
		var parser = {};

		parser.extractItem = function(v) {
			var item = {
				id : v.id,
				type : v.type,
				displayType : v.displayType,
				name : v.name,
				desc : v.description,
				status : v.status,
				schedule : v.schedule,
				scheduleState : v.scheduleState,
				dbInput : v.dbInput,
				analysisEngine : v.analysisEngine,
				sampleRate : v.sampleRate,
				upperTime : v.upperTime,
				trainingDays : v.trainingDays,
				anomalySampleRate : v.anomalySampleRate,
				anomalyUpperTime : v.anomalyUpperTime,
				anomalyTrainingDays : v.anomalyTrainingDays
			};
			if (item.analysisEngine !== null) {
				item.active = v.analysisEngine.active;
				item.visible = v.analysisEngine.visible;
			}
			if (item.dbInput === null || item.dbInput === undefined) {
				item.source = "file";
			} else {
				item.source = "database";
			}
			if (v.anomalySampleRate === null || v.anomalySampleRate === undefined) {
				item.twoTF = false;
			} else {
				item.twoTF = true;
			}
			return item;
		}

		parser.parseData = function(data) {
			var result = [];
			$.each(data, function(name, arr) {
				$.each(arr, function(idx, v) {
					var item = parser.extractItem(v);
					result.push(item);
				});
			});
			return result;
		}

		parser.updateItem = function(data, item) {
			var result;
			$.each(data, function(name, arr) {
	    		if (name == item.type) {
	    			$.each(arr, function(idx, v) {
	    				if (v.id == item.id) {
	    					result = parser.extractItem(v);
	    				}
					});
	    		}
			});
			return result;
		}

		return parser;
	});
