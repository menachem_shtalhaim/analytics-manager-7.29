'use strict';

angular.
	module('manageState').
	component('detectState', {
		templateUrl: '/analytics/JS/manage-state/detect-state.template.html',
		controller: ['requestHandler', 'callbackHandler',
			function DetectStateController(requestHandler, callbackHandler) {
				var $ctrl = this;
				$ctrl.running = 0;
				$ctrl.data = [];
				$ctrl.enableAutoRefresh = false;

				$ctrl.request = {
					start: null,
					end: null,
					sampleRate: 30,
					max: 500
				};

				var confirm = callbackHandler.getConfirmCallback();
				
				$ctrl.parseData = function(data){
					$ctrl.data = [];
					$ctrl.running = 0;
					$.each(data, function(i, v) {
						var item = {id:i, value:v};
						if (v === false) {
							$ctrl.running += 1;
						}
						$ctrl.data.push(item);
					});
				}

				var parseCallback = callbackHandler.getParseCallback($ctrl.parseData);
				var errCallback = callbackHandler.getErrCallback();

				$ctrl.refresh = function() {
					requestHandler.showStateRequest(parseCallback, errCallback);
				}

				$ctrl.refreshForm = function() {
					$ctrl.request.start = null;
					$ctrl.request.end = null;
					$ctrl.refresh();
				}

				var sucCallback = callbackHandler.getSucCallback($ctrl.refresh);
				var formCallback = callbackHandler.getSucCallback($ctrl.refreshForm);

				$ctrl.autoRefresh = function() {
					$ctrl.refresh();
					if ($ctrl.enableAutoRefresh) {
						setTimeout($ctrl.autoRefresh, 2000);
					}
				}

				$ctrl.startAutoRefresh = function(){
					if ($ctrl.enableAutoRefresh) {
						$ctrl.autoRefresh();
					}
				}

				$ctrl.submit = function() {
					$ctrl.request.start = Date.parse($ctrl.request.start);
					$ctrl.request.end = Date.parse($ctrl.request.end);
					requestHandler.detectStateRequest($ctrl.request, formCallback, errCallback);
				}

				$ctrl.delete = function(item) {
					confirm("Are you sure you want to delete this request?", function(){
						requestHandler.deleteStateRequest(item.id, sucCallback, errCallback);
					});
				}

				$ctrl.download = function(item) {
					window.open("/analytics/state/download?requestId=" + item.id);
					$ctrl.refresh();
				}

				$ctrl.refresh();

				$ctrl.$onDestroy = function(){
					$ctrl.enableAutoRefresh = false;
				}
			}
		]
	});