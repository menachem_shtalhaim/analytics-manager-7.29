'use strict';

angular.
	module('createAnalyzer').
	component('createAnalyzer', {
		templateUrl: '/analytics/JS/create-analyzer/create-analyzer.template.html',
		controller: ['requestHandler', 'fileHandler', 'callbackHandler', 'createConfigurater',
			function CreateController(requestHandler, fileHandler, callbackHandler, createConfigurater) {
				var $ctrl = this;
				$ctrl.analyzer = {
					type: null,
					name: null,
					description: null,
					sampleRate: 30,
					trainingDays: 30,
					upperTime: null,
					dataSource: null,
					fileContent: null,
					maxNumeric: 1,
					maxNonNumeric: 0,
					minNumericValue: 0,
					minNonNumericValue: 0,
					devices: null,
					active: true,
					visible: true,
					emailAddress: null,
					emailSubject: null,
					emailBody: null,
					suppress: 0,
					anomalySampleRate: 30,
					anomalyUpperTime: null,
					anomalyTrainingDays: 30,
					targetContent: null
				}

				$ctrl.enableNot = false;

				$ctrl.data = [];

				$ctrl.dataType = new Map();

				$ctrl.parseData = function(data) {
					$ctrl.data = [];
					$.each(data, function(idx, item) {
						item.support = [];
						if (item.numeric === true) {
							item.support.push("numeric");
						}
						if (item.nonNumeric === true) {
							item.support.push("non numeric");
						}
						$ctrl.dataType.set(item.type, {numeric: item.numeric, nonNumeric: item.nonNumeric});
						$ctrl.data.push(item);
					});
				}

				$ctrl.reconfig = function() {
					$ctrl.config = createConfigurater.configurate($ctrl.analyzer.type);
				}

				var parseCallback = callbackHandler.getParseCallback($ctrl.parseData);
				var errCallback = callbackHandler.getErrCallback();

				$ctrl.refresh = function() {
					$ctrl.analyzer.upperTime = undefined;
					$ctrl.analyzer.anomalyUpperTime = undefined;
					$ctrl.reconfig();
				}

				var sucCallback = callbackHandler.getSucCallback(function(){
					$ctrl.refresh();
					window.location = "/analytics#!/show-analyzers"
				});

				$ctrl.showTypes = function() {
					requestHandler.showTypes(parseCallback, errCallback);
				}

				$ctrl.showTypes();
				$ctrl.refresh();

				$ctrl.uploadInput = function(element) {
					var file = element.files[0];
			        var callback = function(data) {
			        	$ctrl.analyzer.fileContent = data;
			        }
			        fileHandler.upload(file, callback);
				}

				$ctrl.uploadTarget = function(element) {
					var file = element.files[0];
			        var callback = function(data) {
			        	$ctrl.analyzer.targetContent = data;
			        }
			        fileHandler.upload(file, callback);
				}

				$ctrl.submit = function() {
					$ctrl.analyzer.upperTime = Date.parse($ctrl.analyzer.upperTime);
					if ($ctrl.config.twoTF === true) {
						$ctrl.analyzer.anomalyUpperTime = Date.parse($ctrl.analyzer.anomalyUpperTime);
					}
					if ($ctrl.enableNot === false) {
						$ctrl.analyzer.emailAddress = null;
						$ctrl.analyzer.emailSubject = null;
						$ctrl.analyzer.emailBody = null;
					}
					requestHandler.createAnalyzer($ctrl.analyzer, sucCallback, errCallback);
				}
			}
		]
	});