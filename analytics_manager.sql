# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.7.13)
# Database: analytics_manager
# Generation Time: 2017-07-26 06:23:50 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table analyzer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `analyzer`;

CREATE TABLE `analyzer` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `description` varchar(500) NOT NULL DEFAULT '',
  `display_type` varchar(30) NOT NULL DEFAULT '',
  `sample_rate` int(11) unsigned NOT NULL,
  `upper_time` bigint(11) unsigned DEFAULT NULL,
  `training_days` int(11) unsigned NOT NULL,
  `archived` tinyint(1) NOT NULL,
  `type` varchar(11) NOT NULL DEFAULT '',
  `analyzer_type` char(3) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `analyzer` WRITE;
/*!40000 ALTER TABLE `analyzer` DISABLE KEYS */;

INSERT INTO `analyzer` (`id`, `name`, `description`, `display_type`, `sample_rate`, `upper_time`, `training_days`, `archived`, `type`, `analyzer_type`)
VALUES
	(58,'shape','shape','Shape Analysis',30,1493510400000,30,0,'motif','M'),
	(59,'Training Task 0','automatically generated analyzer for training task','Value Distribution',30,1491004800000,7,1,'knn','K'),
	(60,'Training Task 0','automatically generated analyzer for training task','Targeted Prediction',30,1493510400000,7,1,'target','T'),
	(61,'Training Task 1','automatically generated analyzer for training task','Targeted Prediction',30,NULL,7,1,'target','T'),
	(62,'Training Task 2','automatically generated analyzer for training task','Targeted Prediction',30,NULL,7,1,'target','T'),
	(63,'Training Task 3','automatically generated analyzer for training task','Duplicate Signals',60,1493510400000,40,1,'dup','D'),
	(64,'Training Task 0','automatically generated analyzer for training task','Duplicate Signals',45,1493510400000,40,0,'dup','D');

/*!40000 ALTER TABLE `analyzer` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table create_data
# ------------------------------------------------------------

DROP TABLE IF EXISTS `create_data`;

CREATE TABLE `create_data` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `task_id` int(11) unsigned NOT NULL,
  `sample_rate` int(11) unsigned NOT NULL,
  `training_days` int(11) unsigned NOT NULL,
  `upper_time` bigint(11) unsigned DEFAULT NULL,
  `anomaly_sample_rate` int(11) unsigned DEFAULT NULL,
  `anomaly_training_days` int(11) unsigned DEFAULT NULL,
  `anomaly_upper_time` bigint(11) unsigned DEFAULT NULL,
  `max_numeric` int(11) unsigned NOT NULL,
  `max_non_numeric` int(11) unsigned NOT NULL,
  `min_numeric` int(11) unsigned NOT NULL,
  `min_non_numeric` int(11) unsigned NOT NULL,
  `devices` varchar(100) DEFAULT NULL,
  `target` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `create_data` WRITE;
/*!40000 ALTER TABLE `create_data` DISABLE KEYS */;

INSERT INTO `create_data` (`id`, `task_id`, `sample_rate`, `training_days`, `upper_time`, `anomaly_sample_rate`, `anomaly_training_days`, `anomaly_upper_time`, `max_numeric`, `max_non_numeric`, `min_numeric`, `min_non_numeric`, `devices`, `target`)
VALUES
	(33,35,45,40,1493510400000,45,40,1493510400000,1000,1000,50,0,NULL,NULL);

/*!40000 ALTER TABLE `create_data` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table db_input
# ------------------------------------------------------------

DROP TABLE IF EXISTS `db_input`;

CREATE TABLE `db_input` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `max_numeric` int(11) unsigned NOT NULL,
  `max_non_numeric` int(11) unsigned NOT NULL,
  `min_numeric` int(11) unsigned NOT NULL,
  `min_non_numeric` int(11) unsigned NOT NULL,
  `devices` varchar(100) DEFAULT NULL,
  `analyzer_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `db_input` WRITE;
/*!40000 ALTER TABLE `db_input` DISABLE KEYS */;

INSERT INTO `db_input` (`id`, `max_numeric`, `max_non_numeric`, `min_numeric`, `min_non_numeric`, `devices`, `analyzer_id`)
VALUES
	(33,300,0,30,0,NULL,58),
	(34,20,0,50,0,NULL,59),
	(35,20,0,50,0,NULL,60),
	(36,20,0,50,0,NULL,61),
	(37,20,0,50,0,NULL,62),
	(38,1000,1000,50,0,NULL,63),
	(39,1000,1000,50,0,NULL,64);

/*!40000 ALTER TABLE `db_input` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table display_info
# ------------------------------------------------------------

DROP TABLE IF EXISTS `display_info`;

CREATE TABLE `display_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(11) NOT NULL DEFAULT '',
  `display_type` varchar(30) NOT NULL DEFAULT '',
  `numeric` tinyint(1) unsigned NOT NULL,
  `non_numeric` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `display_info` WRITE;
/*!40000 ALTER TABLE `display_info` DISABLE KEYS */;

INSERT INTO `display_info` (`id`, `type`, `display_type`, `numeric`, `non_numeric`)
VALUES
	(1,'knn','Value Distribution',1,0),
	(2,'motif','Shape Analysis',1,1),
	(3,'dup','Duplicate Signals',1,0),
	(4,'target','Targeted Prediction',1,1);

/*!40000 ALTER TABLE `display_info` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dup_analyzer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dup_analyzer`;

CREATE TABLE `dup_analyzer` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `model_id` int(11) unsigned NOT NULL,
  `gi_model_id` int(11) unsigned NOT NULL,
  `ae_id` int(11) unsigned NOT NULL,
  `sample_rate_anomaly` int(11) unsigned NOT NULL,
  `training_days_anomaly` int(11) unsigned NOT NULL,
  `upper_time_anomaly` bigint(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `dup_analyzer` WRITE;
/*!40000 ALTER TABLE `dup_analyzer` DISABLE KEYS */;

INSERT INTO `dup_analyzer` (`id`, `model_id`, `gi_model_id`, `ae_id`, `sample_rate_anomaly`, `training_days_anomaly`, `upper_time_anomaly`)
VALUES
	(63,41,42,170,60,40,1493510400000),
	(64,43,44,171,45,40,1493510400000);

/*!40000 ALTER TABLE `dup_analyzer` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table knn_analyzer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `knn_analyzer`;

CREATE TABLE `knn_analyzer` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `model_id` int(11) unsigned NOT NULL,
  `ae_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `knn_analyzer` WRITE;
/*!40000 ALTER TABLE `knn_analyzer` DISABLE KEYS */;

INSERT INTO `knn_analyzer` (`id`, `model_id`, `ae_id`)
VALUES
	(59,34,166);

/*!40000 ALTER TABLE `knn_analyzer` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table motif_analyzer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `motif_analyzer`;

CREATE TABLE `motif_analyzer` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `dummy_model_id` int(11) unsigned NOT NULL,
  `gi_model_id` int(11) unsigned NOT NULL,
  `single_model_id` int(11) unsigned NOT NULL,
  `pair_model_id` int(11) unsigned NOT NULL,
  `single_ae_id` int(11) unsigned NOT NULL,
  `pair_ae_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `motif_analyzer` WRITE;
/*!40000 ALTER TABLE `motif_analyzer` DISABLE KEYS */;

INSERT INTO `motif_analyzer` (`id`, `dummy_model_id`, `gi_model_id`, `single_model_id`, `pair_model_id`, `single_ae_id`, `pair_ae_id`)
VALUES
	(58,30,33,31,32,164,165);

/*!40000 ALTER TABLE `motif_analyzer` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table subscriber
# ------------------------------------------------------------

DROP TABLE IF EXISTS `subscriber`;

CREATE TABLE `subscriber` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `address` varchar(100) NOT NULL DEFAULT '',
  `subject` varchar(100) NOT NULL DEFAULT '',
  `body` varchar(500) NOT NULL DEFAULT '',
  `level` tinyint(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `subscriber` WRITE;
/*!40000 ALTER TABLE `subscriber` DISABLE KEYS */;

INSERT INTO `subscriber` (`id`, `name`, `address`, `subject`, `body`, `level`)
VALUES
	(1,'Henry','henry@n-join.com','[$level$] Analyzer id=$id$ is $status$!','The status of analyzer id=$id$ is changed\n\nStatus: $status$\n\nError Message: $error$',2);

/*!40000 ALTER TABLE `subscriber` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table target_analyzer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `target_analyzer`;

CREATE TABLE `target_analyzer` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `model_id` int(11) unsigned NOT NULL,
  `gi_model_id` int(11) unsigned NOT NULL,
  `ae_id` int(11) unsigned NOT NULL,
  `sample_rate_anomaly` int(11) unsigned NOT NULL,
  `training_days_anomaly` int(11) unsigned NOT NULL,
  `upper_time_anomaly` bigint(11) unsigned DEFAULT NULL,
  `target_content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `target_analyzer` WRITE;
/*!40000 ALTER TABLE `target_analyzer` DISABLE KEYS */;

INSERT INTO `target_analyzer` (`id`, `model_id`, `gi_model_id`, `ae_id`, `sample_rate_anomaly`, `training_days_anomaly`, `upper_time_anomaly`, `target_content`)
VALUES
	(60,35,36,167,30,70,1493510400000,'20649419'),
	(61,37,38,168,30,70,NULL,'20649419'),
	(62,39,40,169,30,70,NULL,'20649419');

/*!40000 ALTER TABLE `target_analyzer` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table training_record
# ------------------------------------------------------------

DROP TABLE IF EXISTS `training_record`;

CREATE TABLE `training_record` (
  `id` int(11) unsigned NOT NULL,
  `status` varchar(11) NOT NULL DEFAULT '',
  `analyzer_id` int(11) NOT NULL,
  `manage_status` smallint(3) unsigned NOT NULL,
  `start_time` bigint(11) unsigned DEFAULT NULL,
  `found` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `training_record` WRITE;
/*!40000 ALTER TABLE `training_record` DISABLE KEYS */;

INSERT INTO `training_record` (`id`, `status`, `analyzer_id`, `manage_status`, `start_time`, `found`)
VALUES
	(21,'CRUSHED',59,1,1501006794356,NULL),
	(23,'COMPLETED',60,0,1501009636500,0),
	(27,'COMPLETED',62,2,1501010036591,0),
	(29,'RUNNING',64,1,1501048928521,0);

/*!40000 ALTER TABLE `training_record` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table training_record_model
# ------------------------------------------------------------

DROP TABLE IF EXISTS `training_record_model`;

CREATE TABLE `training_record_model` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `end_time` bigint(11) unsigned DEFAULT NULL,
  `data_start_time` bigint(11) unsigned DEFAULT NULL,
  `data_end_time` bigint(20) unsigned DEFAULT NULL,
  `error` text,
  `type` varchar(30) NOT NULL DEFAULT '',
  `report_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `training_record_model` WRITE;
/*!40000 ALTER TABLE `training_record_model` DISABLE KEYS */;

INSERT INTO `training_record_model` (`id`, `end_time`, `data_start_time`, `data_end_time`, `error`, `type`, `report_id`)
VALUES
	(341,NULL,1490389200000,1490994000000,'null [server crashed]','Analysis Model',21),
	(342,1501009636529,1487451600000,1493499600000,NULL,'Anomaly Model',23),
	(343,1501009637351,1492894800000,1493499600000,NULL,'Group Induction',23),
	(344,1501010036620,1494962036606,1501010036606,NULL,'Anomaly Model',27),
	(345,NULL,1500405222253,1501010022253,NULL,'Group Induction',27),
	(366,NULL,NULL,NULL,NULL,'Analysis Model',29);

/*!40000 ALTER TABLE `training_record_model` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table training_task
# ------------------------------------------------------------

DROP TABLE IF EXISTS `training_task`;

CREATE TABLE `training_task` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `analyzer_type` varchar(11) NOT NULL DEFAULT '',
  `analyzer_id` int(11) unsigned DEFAULT NULL,
  `report_id` int(11) unsigned DEFAULT NULL,
  `status` smallint(3) unsigned NOT NULL,
  `temporary` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `training_task` WRITE;
/*!40000 ALTER TABLE `training_task` DISABLE KEYS */;

INSERT INTO `training_task` (`id`, `analyzer_type`, `analyzer_id`, `report_id`, `status`, `temporary`)
VALUES
	(35,'dup',64,29,1,1);

/*!40000 ALTER TABLE `training_task` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
